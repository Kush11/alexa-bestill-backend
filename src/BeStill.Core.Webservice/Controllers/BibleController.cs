﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BibleController : ControllerBase
    {
        private readonly IRecommendedBibleService _recommendedBibleService;
        private readonly ILogger<BibleController> _logger;

        public BibleController(IRecommendedBibleService recommendedBibleService, ILogger<BibleController> logger)
        {
            _recommendedBibleService = recommendedBibleService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> InsertBible([FromBody] RecommendedBible bible)
        {
            if (bible == null)
            {
                return BadRequest("bible request is null");
            }
            await _recommendedBibleService.Insert(bible.Title, bible.Abbreviation, bible.SubTitle, bible.Description, bible.Link, bible.Status,
                bible.CreatedBy, bible.CreatedOn, bible.ModifiedBy, bible.ModifiedOn);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> GetAllBibles()
        {
            try
            {
                var retVal = await _recommendedBibleService.GetAllBibles();
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);
            }
        }

    }

}
