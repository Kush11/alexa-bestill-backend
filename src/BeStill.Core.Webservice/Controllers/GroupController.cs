﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Model;
using BeStill.Abstraction.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : BaseApiController
    {
        private readonly IGroupService _groupService;
        private readonly ILogger<GroupController> _logger;

        public GroupController(IGroupService groupService, ILogger<GroupController> logger)
        {
            _groupService = groupService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var UserId = HttpContext.User.Identity.Name;
                var response = await _groupService.GetAllGroups(UserId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return CreateApiException(ex);
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var response = await _groupService.GetGroup(id);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return CreateApiException(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] Group_InputDTO request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var UserId = HttpContext.User.Identity.Name;
                await _groupService.Insert(request.Name, request.Description, request.Organization, request.Location, UserId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _groupService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {

                return CreateApiException(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody]Group_UpdateDTO request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var UserId = HttpContext.User.Identity.Name;
                await _groupService.Update(id, request.Name, request.Description, request.Organization, request.Location, UserId);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
    
        [HttpGet]
        [Route("ChangeMemberType/{id}")]
        public async Task<IActionResult> ChangeMemberType(int id)
        {
            try
            {
                var userId = HttpContext.User.Identity.Name;
                await _groupService.ChangeMemberType(id, userId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        [HttpDelete]
        [Route("RemoveMemberFromGroup/{id}")]
        public async Task<IActionResult> RemoveMemberFromGroup(int id)
        {
            try
            {
                var userId = HttpContext.User.Identity.Name;
                await _groupService.RemoveMemberFromGroup(id, userId);
                return Ok();
            }
            catch (Exception ex)
            {

                return CreateApiException(ex);
            }
            
        }

        [HttpGet]
        [Route("InviteStatus")]
        public async Task<IActionResult> GetInvitationStatus([FromBody] GroupInvite_DTO response)
        {
            try
            {
                var res = await _groupService.GetInviteStatus(response.UserId, response.GroupId);
                return Ok(res);
            }
            catch (Exception ex)
            {

                return CreateApiException(ex);
            }
        }

        [HttpGet]
        [Route("UpdateInviteStatus")]
        public async Task<IActionResult> UpdateInviteStatus([FromBody] GroupInvite_DTO response)
        {
            try
            {
                await _groupService.UpdateInviteStatus(response.UserId, response.GroupId, response.Status);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

    }
}
