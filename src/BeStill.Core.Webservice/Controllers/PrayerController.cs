﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.Model;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PrayerController : BaseApiController
    {
        private readonly IPrayerService _prayerService;
        private readonly ILogger<PrayerController> _logger;

        public PrayerController(IPrayerService prayerService, ILogger<PrayerController> logger)
        {
            _prayerService = prayerService;
            _logger = logger;
        }
        // GET: api/Prayer
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var userId = HttpContext.User.Identity.Name;
                var result = await _prayerService.GetPrayers(userId);
                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // GET: api/Prayer/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var result = await _prayerService.GetPrayer(Guid.Parse(id));
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // GET: api/Prayer/5
        [HttpGet]
        [Route("PrayerAnswer/{id}")]
        public async Task<IActionResult> PrayerAnswer(string id)
        {
            try
            {
                await _prayerService.PrayerAnswer(Guid.Parse(id));
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // GET: api/Prayer/5
        [HttpGet]
        [Route("ArchivePrayer/{id}")]
        public async Task<IActionResult> ArchivePrayer(string id)
        {
            try
            {
                var currentDate = DateTime.UtcNow;
                await _prayerService.ArchivePrayer(Guid.Parse(id));
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // POST: api/Prayer
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PrayerModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var userId = HttpContext.User.Identity.Name;
                await _prayerService.Insert(userId, "", model.Title, userId);
                return Ok();


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // PUT: api/Prayer/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _prayerService.Delete(Guid.Parse(id));
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            try
            {
                var userId = HttpContext.User.Identity.Name;
                await _prayerService.DeleteAll(userId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
    }
}
