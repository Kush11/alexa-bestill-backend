﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Model;
using BeStill.Abstraction.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupPrayerController : BaseApiController
    {
        private readonly IGroupPrayerService _groupPrayerService;
        private readonly ILogger<GroupPrayerController> _logger;

        public GroupPrayerController(IGroupPrayerService groupPrayerService, ILogger<GroupPrayerController> logger)
        {
            _groupPrayerService = groupPrayerService;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var UserId = HttpContext.User.Identity.Name;
                var response = await _groupPrayerService.GetGroupPrayers(id, UserId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return CreateApiException(ex);
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> Post(int id, [FromBody] GroupPrayerModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var UserId = HttpContext.User.Identity.Name;
                await _groupPrayerService.AddPrayerToGroup(id, request.GroupId, UserId);
                return Ok();
            }
            catch (Exception ex)
            {

               return CreateApiException(ex);
            }
        }


        [HttpPost]
        [Route("HidePrayer/{id}")]
        public async Task<IActionResult> HidePrayer(int id)
        {
            try
            {
                var UserId = HttpContext.User.Identity.Name;
                await _groupPrayerService.HidePrayer(UserId, id, UserId);
                return Ok();

            }
            catch (Exception ex)
            {

                return CreateApiException(ex);
            }
        }
        
        [HttpGet]
        [Route("FlagPrayer/{id}")]
        public async Task<IActionResult> FlagPrayer(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                await _groupPrayerService.FlagPrayer(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }


        [HttpGet]
        [Route("ChangeGroupPrayerStatus/{id}")]
        public async Task<IActionResult> ChangeGroupPrayerStatus(int id, [FromBody] GroupPrayerModel request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));


                await _groupPrayerService.ChangeGroupPrayerStatus(id, request.GroupId);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
    }
}
