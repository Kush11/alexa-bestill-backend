﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeStill.Core.Webservice.Controllers
{
    [Route("[controller]")]
    public class CallbackController : Controller
    {
        private readonly IGroupService _groupService;
        private readonly IEmailService _emailService;

        public CallbackController(IGroupService groupService, IEmailService emailService)
        {
            _groupService = groupService;
            _emailService = emailService;
        }
        // GET: CallbackController
        [HttpGet]
        [Route("accept/{id}")]
        public async Task<ActionResult> Accept(int id)
        {
            string User = HttpContext.User.Identity.Name;
            string newMember = "You are now a member of the group ";
            await _groupService.AddMemberToGroup(User, false, false, id, "SYSTEM");
            await _groupService.UpdateInviteStatus(User, id, "Accept");
            var group = await _groupService.GetGroup(id);
            newMember += group.Name;
            return View((object)newMember);
        }


        [HttpPost]
        [Route("sendInvite")]
        public async Task<ActionResult> sendInvite([FromBody] GroupInvite_DTO response)
        {
            var group = await _groupService.GetGroup(response.GroupId);
            string Subject = "Group Invitation";
            string Body = "You are invited to " + group.Name + "Check your Application to accept or reject.";
            await _groupService.InsertInviteStatus(response.UserId, response.GroupId, "Pending", "SYSTEM");
            await _emailService.SendAsync(response.UserId, Subject, Body, "BeStill", response.UserId);
            return Ok();
        }
    }
}
