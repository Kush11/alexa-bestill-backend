﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    public class BaseApiController : ControllerBase
    {

        [NonAction]
        protected IActionResult CreateApiException(Exception ex)
        {
            throw ex;
        }

    }
}