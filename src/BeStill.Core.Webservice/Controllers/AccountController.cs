﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BeStill.Core.Webservice.Settings;
using Microsoft.Extensions.Options;
using BeStill.Abstraction.Model;
using System.Security.Claims;
using System.Text;
using BeStill.Abstraction.Service;
using BeStill.Abstraction.Settings;
using BeStill.Core.Webservice.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace BeStill.Core.Webservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseApiController
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IUserDeviceService _userDeviceService;
        private readonly JwtSecurityTokenSettings _jwt;
        private readonly ILogger<AccountController> _logger;

        public AccountController(
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration,
            IEmailService emailService,
            IUserDeviceService userDeviceService,
        IOptions<JwtSecurityTokenSettings> jwt,
            ILogger<AccountController> logger
            )
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._configuration = configuration;
            this._emailService = emailService;
            this._userDeviceService = userDeviceService;
            this._jwt = jwt.Value;
            this._logger = logger;
        }

        /// <summary>
        /// Confirms a user email address
        /// </summary>
        /// <param name="model">ConfirmEmailViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IdentityResult), 200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("confirmEmail")]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmailModel model)
        {
            try
            {
                if (model.Email == null || model.Code == null)
                {
                    return BadRequest(new string[] { "Error retrieving information!" });
                }

                var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
                if (user == null)
                    return BadRequest(new string[] { "Could not find user!" });

                var result = await _userManager.ConfirmEmailAsync(user, model.Code).ConfigureAwait(false);
                if (result.Succeeded)
                    return Ok(result);

                return BadRequest(result.Errors.Select(x => x.Description));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        /// <summary>
        /// Register an account
        /// </summary>
        /// <param name="model">RegisterViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IdentityResult), 200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

            var user = new User { UserName = model.Email, Email = model.Email, FullName = model.FullName, Birthday = model.Birthday };
            var result = await _userManager.CreateAsync(user, model.Password).ConfigureAwait(false);

            if (result.Succeeded)
            {
                var _user = await _userManager.FindByEmailAsync(user.Email);
                if(model.DeviceInfo != null)
                await _userDeviceService.Insert(_user.Id, model.DeviceInfo.DeviceId, model.DeviceInfo.DeviceModel, model.DeviceInfo.DeviceName, "Active", "System");
                
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                //var callbackUrl = $"{_client.Url}{_client.EmailConfirmationPath}?uid={user.Id}&code={System.Net.WebUtility.UrlEncode(code)}";

                //await _emailService.SendEmailConfirmationAsync(model.Email, code).ConfigureAwait(false);

                return Ok();
            }

            return BadRequest(result.Errors.Select(x => x.Description));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        /// <summary>
        /// Register an account
        /// </summary>
        /// <param name="model">RegisterViewModel</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("updateemail/{email}")]
        public async Task<IActionResult> UpdateEmail(string email)
        {
            try
            {
                var existUser = await _userManager.FindByEmailAsync(email);
                if (existUser != null)
                    throw new Exception("The email entered belong to another user on our system");

                var userId = HttpContext.User.Identity.Name;
                var user = await _userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    user.Email = email;
                }

                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return Ok();
                }

                return BadRequest(result.Errors.Select(x => x.Description));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        [Authorize]
        [HttpDelete]
        [Route("removeuser")]
        public async Task<IActionResult> DeleteUser()
        {
            try
            {
                var userId = HttpContext.User.Identity.Name;
                var user = await _userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    await _userManager.DeleteAsync(user);
                    // TODO: delete userdevice record
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        /// <summary>
        /// Log into account
        /// </summary>
        /// <param name="model">LoginViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(TokenModel), 200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("token")]
        public async Task<IActionResult> CreateToken([FromBody]LoginModel model)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
                if (user == null)
                    return BadRequest(new string[] { "Invalid credentials." });

                var tokenModel = new TokenModel()
                {
                    HasVerifiedEmail = false
                };

                // Only allow login if email is confirmed
                if (!user.EmailConfirmed)
                {
                    return Ok(tokenModel);
                }

                // Used as user lock
                if (user.LockoutEnabled)
                    return BadRequest(new string[] { "This account has been locked." });

                if (await _userManager.CheckPasswordAsync(user, model.Password).ConfigureAwait(false))
                {
                    tokenModel.HasVerifiedEmail = true;

                    if (user.TwoFactorEnabled)
                    {
                        tokenModel.TFAEnabled = true;
                        return Ok(tokenModel);
                    }
                    else
                    {
                        JwtSecurityToken jwtSecurityToken = await CreateJwtToken(user).ConfigureAwait(false);
                        tokenModel.TFAEnabled = false;
                        tokenModel.Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

                        return Ok(tokenModel);
                    }
                }

                return BadRequest(new string[] { "Invalid login attempt." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        

        /// <summary>
        /// Forgot email sends an email with a link containing reset token
        /// </summary>
        /// <param name="model">ForgotPasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("forgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody]EmailModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user).ConfigureAwait(false)))
                    return BadRequest(new string[] { "Please verify your email address." });

                var code = await _userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);
                //var callbackUrl = $"{_client.Url}{_client.ResetPasswordPath}?uid={user.Id}&code={System.Net.WebUtility.UrlEncode(code)}";

                //await _emailService.SendPasswordResetAsync(model.Email, code).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        /// <summary>
        /// Reset account password with reset token
        

        /// <summary>
        /// Resend email verification email with token link
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("resendVerificationEmail")]
        public async Task<IActionResult> resendVerificationEmail([FromBody]EmailModel model)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
                if (user == null)
                    return BadRequest(new string[] { "Could not find user!" });

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                //var callbackUrl = $"{_client.Url}{_client.EmailConfirmationPath}?uid={user.Id}&code={System.Net.WebUtility.UrlEncode(code)}";
                //await _emailService.SendEmailConfirmationAsync(user.Email, code).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
        [HttpPost]
        [ProducesResponseType(typeof(IdentityResult), 200)]
        [ProducesResponseType(typeof(IEnumerable<string>), 400)]
        [Route("resetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.Values.Select(x => x.Errors.FirstOrDefault().ErrorMessage));

                var user = await _userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    return BadRequest(new string[] { "Invalid credentials." });
                }
                var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password).ConfigureAwait(false);
                if (result.Succeeded)
                {
                    return Ok(result);
                }
                return BadRequest(result.Errors.Select(x => x.Description));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
        private async Task<JwtSecurityToken> CreateJwtToken(User user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user).ConfigureAwait(false);
            var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);

            var roleClaims = new List<Claim>();

            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim("roles", roles[i]));
            }

            string ipAddress = IpHelper.GetIpAddress();

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Id),
                new Claim(ClaimTypes.Name, user.Id),
                new Claim("uid", user.Id),
                new Claim("ip", ipAddress)
            }
            .Union(userClaims)
            .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwt.Issuer,
                audience: _jwt.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddHours(3),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }
    }
}