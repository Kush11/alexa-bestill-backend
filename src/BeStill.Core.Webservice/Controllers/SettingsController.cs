﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Core.Webservice.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private readonly IMobileSettingService _mobileSettingService;
        private readonly IMobileSharingSettingsService _mobileSharingSettingsService;
        private readonly IMobileSettingPushNotificationService _mobileSettingPushNotification;
        private readonly IAlexaMobileSettingService _alexaMobileSettingService;
        private readonly IGroupMobileSettingService _groupMobileSettingService;
        private readonly ILogger<SettingsController> _logger;
        private readonly IPrayerMobileSettingsService _prayerMobileSettigsService;



        public SettingsController(IMobileSettingService mobileSettingService, ILogger<SettingsController> logger,
            IMobileSettingPushNotificationService mobileSettingPushNotification, IAlexaMobileSettingService alexaMobileSettingService,
            IMobileSharingSettingsService mobileSharingSettingsService, IGroupMobileSettingService groupMobileSettingService,
            IPrayerMobileSettingsService prayerMobileSettingsService)
        {
            _mobileSettingService = mobileSettingService;
            _mobileSharingSettingsService = mobileSharingSettingsService;
            _logger = logger;
            _mobileSettingPushNotification = mobileSettingPushNotification;
            _alexaMobileSettingService = alexaMobileSettingService;
            _groupMobileSettingService = groupMobileSettingService;
            _prayerMobileSettigsService = prayerMobileSettingsService;

        }

        [HttpPost]
        public async Task<IActionResult> InsertSetting([FromBody] MobileSettings settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _mobileSettingService.Insert(settings.Appearance, settings.DefaultSortBy, settings.DefaultSnoozeDuration, settings.ArchiveAutoDelete,
                settings.IncludeAnsweredPrayerAutoDelete, settings.Status, settings.CreatedBy, settings.CreatedOn, settings.ModifiedBy, settings.ModifiedOn);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> GetSetting(int UserId)
        {
            try
            {
                var retVal = await _mobileSettingService.GetMobileSettings(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);
            }
        }

        [HttpPost("pushNotification")]
        public async Task<IActionResult> InsertPushNotificationSetting([FromBody] MobileSettingPushNotification settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _mobileSettingPushNotification.Insert(settings.AllowPushNotification, settings.AllowTextNotification, settings.EmailUpdateNotification,
                settings.EmailUpdateFrequency, settings.NotifyMeSomeOnePostOnGroup, settings.NotifyMeSomeOneSharePrayerWithMe,
                settings.CreatedBy, settings.CreatedOn, settings.ModifiedBy, settings.ModifiedOn);
            return Ok();
        }

        [HttpGet("pushNotification")]
        public async Task<IActionResult> GetPushSetting(int UserId)
        {
            try
            {
                var retVal = await _mobileSettingPushNotification.GetPushSettings(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);
            }
        }

        [HttpPost("alexaSettings")]
        public async Task<IActionResult> InsertAlexaSetting([FromBody] MobileSettingsAlexa settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _alexaMobileSettingService.Insert(settings.AllowPrayerTimeNotification, settings.SyncAlexa);
            return Ok();
        }

        [HttpGet("alexaSettings")]
        public async Task<IActionResult> GetAlexaSetting(int UserId)
        {
            try
            {
                var retVal = await _alexaMobileSettingService.GetAlexaSetting(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);

            }


        }

        [HttpPost("sharingSettings")]
        public async Task<IActionResult> InsertSharingSetting([FromBody] MobileSettingsSharing settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _mobileSharingSettingsService.Insert(settings.EnableSharingViaText, settings.EnableSharingViaEmail, settings.ChurchId,
                settings.Email, settings.Phone, settings.Status, settings.CreatedBy, settings.CreatedOn, settings.ModifiedBy, settings.ModifiedOn);
            return Ok();
        }

        [HttpGet("sharingSettings")]
        public async Task<IActionResult> GetSharingSetting(int UserId)
        {
            try
            {
                var retVal = await _mobileSharingSettingsService.GetMobileSharingSetting(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);

            }


        }

        [HttpPost("groupSettings")]
        public async Task<IActionResult> InsertGroupSetting([FromBody] GroupMobileSettings settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _groupMobileSettingService.Insert(settings.DeviceId, settings.Status, settings.CreatedBy, settings.CreatedOn, settings.ModifiedBy, settings.ModifiedOn);
            return Ok();
        }

        [HttpGet("groupSettings")]
        public async Task<IActionResult> GetGroupSetting(int UserId)
        {
            try
            {
                var retVal = await _groupMobileSettingService.GetSettingByUserId(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);

            }
        }


        [HttpPost("prayerSettings")]
        public async Task<IActionResult> InsertPrayerSetting([FromBody] PrayerMobileSettings settings)
        {
            if (settings == null)
            {
                return BadRequest("settings request is null");
            }
            await _prayerMobileSettigsService.Insert(settings.Frequency, settings.Date, settings.Time, settings.CreatedBy, settings.CreatedOn, settings.ModifiedBy, settings.ModifiedOn);
            return Ok();
        }

        [HttpGet("prayerSettings")]
        public async Task<IActionResult> GetPrayerSetting(int UserId)
        {
            try
            {
                var retVal = await _prayerMobileSettigsService.GetPrayerSetting(UserId);
                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw (ex);

            }

        }
    }
}


