﻿using BeStill.Abstraction.Service;
using BeStill.Business.Service;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Core.Webservice
{
    public class ServiceRegistration
    {
        public static void Setup(IServiceCollection services, bool useMockForDatabase, bool useMockForIntegrations)
        {
            //data layer services
            if (useMockForDatabase)
            {
                //services.AddSingleton<ICustomerDataService, MockCustomerDataService>();
            }
            else
            {
                services.AddTransient<IDeviceDataService, DeviceDataService>();
                services.AddTransient<IUserDeviceDataService, UserDeviceDataService>();
                services.AddTransient<IIntentHistoryLogDataService, IntentHistoryLogDataService>();
                services.AddTransient<IPrayerDataService, PrayerDataService>();
                services.AddTransient<IMobileSettingDataService, MobileSettingDataService>();
                services.AddTransient<IReminderDataService, ReminderDataService>();
                services.AddTransient<IIntentHistoryLogTempDataService, IntentHistoryLogTempDataService>();
                services.AddTransient<IUserPreferenceDataService, UserPreferenceDataService>();
                services.AddTransient<IGroupDataService, GroupDataService>();
                services.AddTransient<IMobileSettingPushNotificationDataService, MobileSettingPushNotificationDataService>();
                services.AddTransient<IAlexaMobileSettingDataService, AlexaMobileSettingDataService>();
                services.AddTransient<IGroupPrayerDataService, GroupPrayerDataService>();
                services.AddTransient<IMobileSharingSettingsDataService, MobileSharingSettingsDataService>();
                services.AddTransient<IGroupMobileSettingsDataService, GroupMobileSettingsDataService>();
                services.AddTransient<IPrayerMobileSettingsDataService, PrayerMobileSettingsDataService>();
                services.AddTransient<IRecommendedBibleDataService, RecommendedBibleDataService>();




            }

            services.AddTransient<IDeviceService, DeviceService>();
            services.AddTransient<IUserDeviceService, UserDeviceService>();
            services.AddTransient<IPrayerService, PrayerService>();
            services.AddTransient<IReminderService, ReminderService>();
            services.AddTransient<IIntentHistoryLogService, IntentHistoryLogService>();
            services.AddTransient<IIntentHistoryLogTempService, IntentHistoryLogTempService>();
            services.AddTransient<IUserPreferenceService, UserPreferenceService>();
            services.AddTransient<IMobileSettingService, MobileSettingService>();
            services.AddTransient<IMobileSettingPushNotificationService, MobileSettingPushNotificationService>();
            services.AddTransient<IAlexaMobileSettingService, AlexaMobileSettingService>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<IGroupPrayerService, GroupPrayerService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IMobileSharingSettingsService, MobileSharingSettingsService>();
            services.AddTransient<IGroupMobileSettingService, GroupMobileSettingsService>();
            services.AddTransient<IPrayerMobileSettingsService, PrayerMobileSettingsService>();
            services.AddTransient<IRecommendedBibleService, RecommendedBibleService>();


        }
    }
}
