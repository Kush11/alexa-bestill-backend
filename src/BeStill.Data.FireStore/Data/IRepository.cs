﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Data
{
    public interface IRepository<T> where T : class
    {
        Task<List<T>> GetAll();
        Task<T> Get(string id);
        Task<Guid> Add(T entity);
        Task Update(T entity, string id);
        Task<T> Delete(string id);
    }
}
