﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Data
{
    public abstract class CoreRepository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : class
        where TContext : DbContext
    {
        private readonly TContext context;
        public CoreRepository(TContext context)
        {
            this.context = context;
        }
        public async Task<Guid> Add(TEntity entity)
        {  
            Guid Id = Guid.NewGuid();
            DocumentReference docRef = context.database.Collection(entity.GetType().Name).Document(Id.ToString());
            await docRef.SetAsync(entity);
            return Id;
        }

        public async Task<TEntity> Delete(string id)
        {
            DocumentReference docRef = context.database.Collection(typeof(TEntity).Name).Document(id);
            await docRef.DeleteAsync();

            return null;
        }

        public async Task<TEntity> Get(string id)
        {
            DocumentReference docRef = context.database.Collection(typeof(TEntity).Name).Document(id);
            var snap = await docRef.GetSnapshotAsync();
            if (snap.Exists)
                return snap.ConvertTo<TEntity>();

            return null;
        }

        public async Task<List<TEntity>> GetAll()
        {
            List<TEntity> result = null;
            Query docRef = context.database.Collection(typeof(TEntity).Name);
            var snaps = await docRef.GetSnapshotAsync();

            if(snaps.Count > 0)
            {
                result = new List<TEntity>();
            }
            foreach(var snap in snaps)
            {
                if(snap.Exists)
                result.Add(snap.ConvertTo<TEntity>());
            }

            return result;
        }

        public async Task Update(TEntity entity, string id)
        {
            DocumentReference docRef = context.database.Collection(typeof(TEntity).Name).Document(id);
            var data = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToDictionary(prop => prop.Name, prop => prop.GetValue(entity, null));
            var snap = await docRef.GetSnapshotAsync();
            if (snap.Exists)
            {
                await docRef.UpdateAsync(data);
            }
        }

    }
}
