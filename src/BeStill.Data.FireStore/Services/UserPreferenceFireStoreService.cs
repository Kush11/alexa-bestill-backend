﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class UserPreferenceFireStoreService : CoreRepository<Model.UserPreference, DbContext>,IUserPreferenceDataService
    {
        DbContext _context;
        UserFireStoreService _userService;
        public UserPreferenceFireStoreService(UserFireStoreService userService, DbContext context) : base(context)
        {
            _context = context;
            _userService = userService;
        }

        public async Task<UserPreference> GetUserPreferenceById(string UserId)
        {

            Query qRef = _context.database.Collection(typeof(Model.UserPreference).Name);
            qRef = qRef.WhereEqualTo("Userid", UserId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                var docRefs = snaps[0];
                if (docRefs.Exists)
                {
                    var udevice = docRefs.ConvertTo<Model.UserPreference>();
                    return Map(udevice);
                }
            }
            return null;
        }

        public async Task InsertOrUpdate(string UserId, int PrayerTime, string BackgroundMusic, string CreatedBy)
        {

            Query qRef = _context.database.Collection(typeof(Model.UserPreference).Name);
            qRef = qRef.WhereEqualTo("Userid", UserId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                var docRefs = snaps[0];
                if (docRefs.Exists)
                {
                    DocumentReference docRef = _context.database.Collection(typeof(Model.UserPreference).Name).Document(docRefs.Reference.Id);
                    var data = new Dictionary<string, object>();
                    data.Add("PrayerTime", PrayerTime);
                    data.Add("BackgroundMusic", BackgroundMusic);
                    var snap = await docRef.GetSnapshotAsync();
                    if (snap.Exists)
                    {
                        await docRef.UpdateAsync(data);
                    }
                }
            }
            else
            {
                var userPref = new Model.UserPreference
                {
                    Userid = UserId,
                    PrayerTime = PrayerTime,
                    BackgroundMusic = BackgroundMusic,
                    CreatedBy = CreatedBy,
                    CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    ModifiedBy = CreatedBy,
                    ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())
                };
                await this.Add(userPref);
            }
            
        }

        private Abstraction.Models.UserPreference Map(Model.UserPreference obj)
        {
            return new Abstraction.Models.UserPreference
            {
                UserId = obj.Userid,
                CreatedBy = obj.CreatedBy,
                PrayerTime = obj.PrayerTime,
                BackgroundMusic = obj.BackgroundMusic
            };
        }
    }
}
