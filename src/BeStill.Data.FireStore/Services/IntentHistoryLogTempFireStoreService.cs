﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class IntentHistoryLogTempFireStoreService : CoreRepository<Model.IntentHistoryLogTemp, DbContext>, IIntentHistoryLogTempDataService
    {
        DbContext _context;
        public IntentHistoryLogTempFireStoreService(DbContext context) : base(context)
        {
            _context = context;
        }

        public Task BulkInsert(List<IntentHistoryLogTemp> intentHistories)
        {
            throw new NotImplementedException();
        }
    }
}
