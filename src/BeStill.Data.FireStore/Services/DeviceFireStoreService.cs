﻿using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using BeStill.Data.FireStore.Model;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class DeviceFireStoreService : CoreRepository<Device, DbContext>, IDeviceDataService
    {
        public DeviceFireStoreService(DbContext context) : base(context)
        {
        }

        public async Task<Guid> Insert(string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
            var device = new Device
            {
                DeviceId = DeviceId,
                Model = Model,
                Name = Name,
                Status = Status,
                CreatedBy = CreatedBy,
                CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                ModifiedBy = CreatedBy,
                ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())
            };

            return await this.Add(device);

        }
    }
}
