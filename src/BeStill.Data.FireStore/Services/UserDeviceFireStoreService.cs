﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using BeStill.Data.FireStore.Model;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User = BeStill.Data.FireStore.Model.User;

namespace BeStill.Data.FireStore.Services
{
    public class UserDeviceFireStoreService : CoreRepository<Model.UserDevice, DbContext>, IUserDeviceDataService
    {
        DeviceFireStoreService _deviceService;
        UserFireStoreService _userService;
        DbContext _context;
        public UserDeviceFireStoreService(DbContext context, DeviceFireStoreService deviceService, UserFireStoreService userService) : base(context)
        {
            _deviceService = deviceService;
            _userService = userService;
            _context = context;
        }

        public async Task<Abstraction.Models.UserDevice> GetUserDeviceById(string UserId)
        {
            var Id = await _userService.GetUserIdByKey(UserId);
            Query qRef = _context.database.Collection(typeof(Model.UserDevice).Name);
            qRef = qRef.WhereEqualTo("UserId", Id);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if(snaps.Count > 0)
            {
                var docRefs = snaps[0];
                if (docRefs.Exists)
                {
                    var udevice = docRefs.ConvertTo<Model.UserDevice>();
                    return Map(udevice);
                }
            }
            return null;
        }

        public async Task Insert(string UserId, string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                var deviceId = await _deviceService.Insert(DeviceId, Model, Name, Status, CreatedBy);
                var user = new User
                {
                    KeyReference = UserId,
                    DOB = Timestamp.FromDateTime(DateTime.MinValue.ToUniversalTime()),
                    CreatedBy = CreatedBy,
                    CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    ModifiedBy = CreatedBy,
                    ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())
                };
                var userId = await _userService.Add(user);

                var userDevice = new Model.UserDevice
                {
                    UserId = userId.ToString(),
                    DeviceId = deviceId.ToString(),
                    Status = Status,
                    CreatedBy = CreatedBy,
                    CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    ModifiedBy = CreatedBy,
                    ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())
                };

                await this.Add(userDevice);
            });
        }


        private Abstraction.Models.UserDevice Map(Model.UserDevice obj)
        {
            var result = new Abstraction.Models.UserDevice
            {
                UserId = obj.UserId,
                DeviceId = obj.DeviceId,
                Status = obj.Status,
                CreatedBy = obj.CreatedBy,
                CreatedOn = obj.CreatedOn.ToDateTime(),
                ModifiedBy = obj.ModifiedBy,
                ModifiedOn = obj.ModifiedOn.ToDateTime()
            };
            return result;
        }
    }
}
