﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class ReminderFireStoreService : CoreRepository<Model.Reminder, DbContext>, IReminderDataService
    {
        DbContext _context;
        public ReminderFireStoreService(DbContext context) : base(context)
        {
            _context = context;
        }

        public async Task Delete(Guid ReminderId)
        {
            await this.Delete(ReminderId.ToString());
        }

        public async Task<List<Reminder>> GetReminders(string UserId)
        {
            List<Abstraction.Models.Reminder> result = null;
            Query qRef = _context.database.Collection(typeof(Model.Reminder).Name);
            qRef = qRef.WhereEqualTo("UserId", UserId)
                .WhereEqualTo("Status", "Active");
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                result = new List<Abstraction.Models.Reminder>();
            }
            int index = 1;
            foreach (DocumentSnapshot snap in snaps)
            {
                if (snap.Exists)
                {
                    var reminder = Map(snap.ConvertTo<Model.Reminder>());
                    reminder.ReminderId = Guid.Parse(snap.Reference.Id);
                    reminder.Sort = index;
                    result.Add(reminder);
                    index++;
                }
            }
            
            return result;
        }

        public async Task Insert(string UserId, int Frequency, string Token, DateTime StartDate, DateTime EndDate, string Title, string CreatedBy, string prayerId)
        {
            var reminder = new Model.Reminder
            {
                UserId = UserId,
                PrayerId = prayerId,
                Frequency = Frequency,
                Token = Token,
                StartDate = Timestamp.FromDateTime(StartDate.ToUniversalTime()),
                EndDate = Timestamp.FromDateTime(EndDate.ToUniversalTime()),
                Title = Title,
                CreatedBy = CreatedBy,
                CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                ModifiedBy = CreatedBy,
                ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                Status = "Active"
            };

            await this.Add(reminder);
        }

        private Reminder Map(Model.Reminder obj)
        {
            return new Reminder
            {
                UserId = obj.UserId,
                PrayerId = obj.PrayerId,
                Frequency = obj.Frequency,
                StartDate = obj.StartDate.ToDateTime(),
                EndDate = obj.EndDate.ToDateTime(),
                Title = obj.Title,
                Token = obj.Token,
                Status = obj.Status,
                CreatedBy = obj.CreatedBy

            };
        }
    }
}
