﻿using BeStill.Data.FireStore.Data;
using BeStill.Data.FireStore.Model;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class UserPrayerFireStoreService : CoreRepository<UserPrayer, DbContext>
    {
        DbContext _context;
        public UserPrayerFireStoreService(DbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<UserPrayer> GetUserPrayerByPrayerId(string PrayerId)
        {
            Query qRef = _context.database.Collection(typeof(Model.UserPrayer).Name);
            qRef = qRef.WhereEqualTo("PrayerId", PrayerId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            var doc = snaps[0];
            if (doc.Exists)
                return doc.ConvertTo<UserPrayer>();

            return null;
        }

        public async Task<List<UserPrayer>> GetUserPrayersByUserId(string UserId)
        {
            List<UserPrayer> result = null;
            CollectionReference qRef = _context.database.Collection(typeof(Model.UserPrayer).Name);
            Query _qRef = qRef.WhereEqualTo("Userid", UserId);
            QuerySnapshot snaps = await _qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                result = new List<UserPrayer>();
            }
            foreach (var snap in snaps)
            {
                if (snap.Exists)
                    result.Add(snap.ConvertTo<UserPrayer>());
            }

            return result;
        }

        public async Task DeleteByUserId(string UserId)
        {
            Query qRef = _context.database.Collection(typeof(Model.UserPrayer).Name);
            qRef = qRef.WhereEqualTo("Userid", UserId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();

            foreach (var snap in snaps)
            {
                if (snap.Exists)
                    await this.Delete(snap.Reference.Id);
            }
        }
    }
}
