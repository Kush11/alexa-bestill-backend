﻿using BeStill.Data.FireStore.Data;
using BeStill.Data.FireStore.Model;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class UserFireStoreService : CoreRepository<User, DbContext>
    {
        DbContext _context;
        public UserFireStoreService(DbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<string> GetUserIdByKey(string UserId)
        {
            Query qRef = _context.database.Collection(typeof(Model.User).Name);
            qRef = qRef.WhereEqualTo("KeyReference", UserId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                var docRefs = snaps[0];
                if (docRefs.Exists)
                {
                    return docRefs.Reference.Id;
                }
            }
            return null;
        }
    }
}
