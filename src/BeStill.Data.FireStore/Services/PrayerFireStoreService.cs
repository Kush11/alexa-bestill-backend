﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore.Data;
using BeStill.Data.FireStore.Model;
using BeStill.Data.FireStore.Services;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.FireStore.Services
{
    public class PrayerFireStoreService : CoreRepository<Model.Prayer, DbContext>, IPrayerDataService
    {
        DbContext _context;
        UserPrayerFireStoreService _userPrayerService;
        public PrayerFireStoreService(UserPrayerFireStoreService userPrayerService, DbContext context) : base(context)
        {
            _context = context;
            _userPrayerService = userPrayerService;
        }

        public async Task ArchivePrayer(Guid PrayerId)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                DocumentReference docRef = _context.database.Collection(typeof(Model.Prayer).Name).Document(PrayerId.ToString());
                var data = new Dictionary<string, object>();
                data.Add("Status", "inactive");
                var snap = await docRef.GetSnapshotAsync();
                if (snap.Exists)
                {
                    await docRef.UpdateAsync(data);
                }
            });
        }
        public async Task SnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                DocumentReference docRef = _context.database.Collection(typeof(Model.Prayer).Name).Document(PrayerId.ToString());
                var data = new Dictionary<string, object>();
                data.Add("SnoozeExpiryDate", Timestamp.FromDateTime(SnoozeExpiryDate));
                //data.Add("SnoozeExpiryDate", SnoozeExpiryDate);
                var snap = await docRef.GetSnapshotAsync();
                if (snap.Exists)
                {
                    await docRef.UpdateAsync(data);
                }
            });
        }

        public async Task UnsnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                DocumentReference docRef = _context.database.Collection(typeof(Model.Prayer).Name).Document(PrayerId.ToString());
                var data = new Dictionary<string, object>();
                data.Add("SnoozeExpiryDate", Timestamp.FromDateTime(SnoozeExpiryDate));
                //data.Add("SnoozeExpiryDate", SnoozeExpiryDate);
                var snap = await docRef.GetSnapshotAsync();
                if (snap.Exists)
                {
                    await docRef.UpdateAsync(data);
                }
            });
        }

        public async Task DeleteAll(string userId)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                var userPrayer = await _userPrayerService.GetUserPrayersByUserId(userId);
                if (userPrayer != null)
                {
                    foreach (var uprayer in userPrayer)
                    {
                        await this.Delete(uprayer.PrayerId);
                    }
                    await _userPrayerService.DeleteByUserId(userId);
                }
            });
        }

        public async Task<DateTime> GetLastPrayedDate(string UserId)
        {
            Query qRef = _context.database.Collection(typeof(Model.User).Name);
            qRef = qRef.WhereEqualTo("KeyReference", UserId);
            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            if (snaps.Count > 0)
            {
                var doc = snaps[0];
                User usr = new User();
                if (doc.Exists)
                    usr = doc.ConvertTo<User>();

                return usr.LastPrayed.ToDateTime();
            }
            return DateTime.MinValue;

        }

        public async Task<Abstraction.Models.Prayer> GetPrayer(Guid PrayerId)
        {
            Abstraction.Models.Prayer prayerResult = null;
            var prayer = await this.Get(PrayerId.ToString());
            var userPrayer = await _userPrayerService.GetUserPrayerByPrayerId(PrayerId.ToString());
            if (userPrayer != null && prayer != null)
            {
                prayerResult = Map(prayer, userPrayer);
                prayerResult.PrayerId = PrayerId.ToString();
            }


            return prayerResult;
        }

        public async Task<List<Abstraction.Models.Prayer>> GetPrayers(string UserId)
        {
            List<Abstraction.Models.Prayer> result = null;
            var currentDate = DateTime.Now.ToUniversalTime();
            Query rRef = _context.database.Collection(typeof(Model.Reminder).Name);
            rRef = rRef.WhereEqualTo("UserId", UserId)
                .WhereEqualTo("Status", "Active");
            QuerySnapshot rsnaps = await rRef.GetSnapshotAsync();
            var prayerIds = rsnaps.Select(c => { var rem = c.ConvertTo<Model.Reminder>(); return rem.PrayerId; }).ToList();

            Query qRef = _context.database.Collection(typeof(Model.Prayer).Name);
            qRef = qRef.WhereEqualTo("UserId", UserId)
                .WhereEqualTo("Status", "Active")
                .WhereEqualTo("IsAnswer", null);

            QuerySnapshot snaps = await qRef.GetSnapshotAsync();
            //var doc = snaps;
            if (snaps.Count > 0)
            {
                result = new List<Abstraction.Models.Prayer>();
            }
            var index = 1;

            foreach (var snap in snaps)
            {
                var doc = snap;
                if (doc.Exists)
                {
                    var _prayer = doc.ConvertTo<Model.Prayer>();
                    var prayerNew = Map(_prayer);
                    prayerNew.PrayerId = doc.Reference.Id;
                    prayerNew.HasReminder = prayerIds.Contains(doc.Reference.Id);
                    //prayerNew.SnoozeExpiryDate = currentDate;
                    prayerNew.Sort = index;
                    result.Add(prayerNew);
                    index++;
                }
            }
            return result;
        }

        public async Task Insert(string UserId, string Type, string Title, string CreatedBy)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                var prayer = new Model.Prayer
                {
                    Type = Type,
                    Title = Title,
                    UserId = UserId,
                    //Description = Description,
                    Status = "Active",
                    CreatedBy = CreatedBy,
                    CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    ModifiedBy = CreatedBy,
                    ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    SnoozeExpiryDate = DateTime.Now.ToUniversalTime(),
                };
                var prayerId = await this.Add(prayer);

                var userPrayer = new Model.UserPrayer
                {
                    PrayerId = prayerId.ToString(),
                    Userid = UserId,
                    Status = "Active",
                    CreatedBy = CreatedBy,
                    CreatedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()),
                    ModifiedBy = CreatedBy,
                    ModifiedOn = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())
                };

                await _userPrayerService.Add(userPrayer);

            });
        }

        public async Task MarkAsAnswer(Guid prayerId)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                DocumentReference docRef = _context.database.Collection(typeof(Model.Prayer).Name).Document(prayerId.ToString());
                var data = new Dictionary<string, object>();
                data.Add("Status", "inactive");
                data.Add("IsAnswer", "y");
                var snap = await docRef.GetSnapshotAsync();
                if (snap.Exists)
                {
                    await docRef.UpdateAsync(data);
                }
            });
        }

        public async Task UpdateLastPrayedDate(string UserId)
        {
            await _context.database.RunTransactionAsync(async transaction =>
            {
                DocumentReference docRef = _context.database.Collection(typeof(Model.User).Name).Document(UserId);
                var data = new Dictionary<string, object>();
                data.Add("LastPrayed", Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()));
                var snap = await docRef.GetSnapshotAsync();
                if (snap.Exists)
                {
                    await docRef.UpdateAsync(data);
                }
            });
        }

        async Task IPrayerDataService.Delete(Guid prayerId)
        {
            await this.Delete(prayerId.ToString());
        }

        private Abstraction.Models.Prayer Map(Model.Prayer obj, UserPrayer uobj)
        {
            return new Abstraction.Models.Prayer
            {
                Status = obj.Status,
                CreatedBy = obj.CreatedBy,
                Type = obj.Type,
                Title = obj.Title,
                //Description = obj.Description,
                IsAnswer = obj.IsAnswer,
                IsFavourite = Convert.ToBoolean(uobj.IsFavourite),
                Sequence = Convert.ToInt32(uobj.Sequence),
                SnoozeExpiryDate = obj.SnoozeExpiryDate,

            };
        }
        private Abstraction.Models.Prayer Map(Model.Prayer obj)
        {
            return new Abstraction.Models.Prayer
            {
                Status = obj.Status,
                CreatedBy = obj.CreatedBy,
                Type = obj.Type,
                Title = obj.Title,
                //Description = obj.Description,
                IsAnswer = obj.IsAnswer,
                SnoozeExpiryDate = obj.SnoozeExpiryDate,

            };
        }
    }
}
