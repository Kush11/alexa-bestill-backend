﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore.Model
{
    [FirestoreData]
    public class UserPrayer
    {
        [FirestoreProperty]
        public string PrayerId { get; set; }
        [FirestoreProperty]
        public string Userid { get; set; }
        [FirestoreProperty]
        public int Sequence { get; set; }
        [FirestoreProperty]
        public bool IsFavourite { get; set; }
        [FirestoreProperty]
        public string Status { get; set; }
        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public Timestamp CreatedOn { get; set; }
        [FirestoreProperty]
        public string ModifiedBy { get; set; }
        [FirestoreProperty]
        public Timestamp ModifiedOn { get; set; }
    }
}
