﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore.Model
{
    [FirestoreData]
    public class Device
    {
        [FirestoreProperty]
        public string DeviceId { get; set; }
        [FirestoreProperty]
        public string Model { get; set; }
        [FirestoreProperty]
        public string Name { get; set; }
        [FirestoreProperty]
        public string Status { get; set; }
        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public Timestamp CreatedOn { get; set; }
        [FirestoreProperty]
        public string ModifiedBy { get; set; }
        [FirestoreProperty]
        public Timestamp ModifiedOn { get; set; }
    }
}
