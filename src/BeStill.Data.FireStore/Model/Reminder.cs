﻿using Google.Cloud.Firestore;
using Google.Protobuf.WellKnownTypes;
using System;
using System.Collections.Generic;
using System.Text;
using Timestamp = Google.Cloud.Firestore.Timestamp;

namespace BeStill.Data.FireStore.Model
{
    [FirestoreData]
    public class Reminder
    {
        [FirestoreProperty]
        public string UserId { get; set; }
        [FirestoreProperty]
        public string PrayerId { get; set; }
        [FirestoreProperty]
        public int Frequency { get; set; }
        [FirestoreProperty]
        public Timestamp StartDate { get; set; }
        [FirestoreProperty]
        public Timestamp EndDate { get; set; }
        [FirestoreProperty]
        public string Title { get; set; }
        [FirestoreProperty]
        public string Token { get; set; }
        [FirestoreProperty]
        public string Status { get; set; }
        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public Timestamp CreatedOn { get; set; }
        [FirestoreProperty]
        public string ModifiedBy { get; set; }
        [FirestoreProperty]
        public Timestamp ModifiedOn { get; set; }
    }
}
