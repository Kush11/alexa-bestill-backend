﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore.Model
{
    [FirestoreData]
    public class Prayer
    {
        [FirestoreProperty]
        public int GroupId { get; set; }
        [FirestoreProperty]
        public string UserId { get; set; }

        [FirestoreProperty]
        public string Type { get; set; }
        [FirestoreProperty]
        public string Title { get; set; }
        //[FirestoreProperty]
        //public string Description { get; set; }
        [FirestoreProperty]
        public string IsAnswer { get; set; }
        [FirestoreProperty]
        public string Status { get; set; }
        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public Timestamp CreatedOn { get; set; }
        [FirestoreProperty]
        public string ModifiedBy { get; set; }
        [FirestoreProperty]
        public Timestamp ModifiedOn { get; set; }
        [FirestoreProperty]
        public DateTime SnoozeExpiryDate { get; set; }

    }
}
