﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore.Model
{
	[FirestoreData]
    public class IntentHistoryLog
    {
		[FirestoreProperty]
		public string NextToken { get; set; }
		[FirestoreProperty]
		public string SkillId { get; set; }
		[FirestoreProperty]
		public string DialogAct { get; set; }
		[FirestoreProperty]
		public string IntentName { get; set; }
		[FirestoreProperty]
		public string IntentConfidence { get; set; }
		[FirestoreProperty]
		public string SlotName { get; set; }
		[FirestoreProperty]
		public string InteractionType { get; set; }
		[FirestoreProperty]
		public string PublicationStatus { get; set; }
		[FirestoreProperty]
		public string UtteranceText { get; set; }
		[FirestoreProperty]
		public string Locale { get; set; }
		[FirestoreProperty]
		public string Stage { get; set; }
		[FirestoreProperty]
		public Timestamp Date { get; set; }
	}
}
