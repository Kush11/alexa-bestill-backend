﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore.Model
{
    [FirestoreData]
    public class User
    {
        [FirestoreProperty]
        public string FirstName { get; set; }
        [FirestoreProperty]
        public string Lastname { get; set; }
        [FirestoreProperty]
        public string Phone { get; set; }
        [FirestoreProperty]
        public string Email { get; set; }
        [FirestoreProperty]
        public Timestamp DOB { get; set; }
        [FirestoreProperty]
        public string KeyReference { get; set; }
        [FirestoreProperty]
        public int ChurchId { get; set; }
        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public Timestamp CreatedOn { get; set; }
        [FirestoreProperty]
        public string ModifiedBy { get; set; }
        [FirestoreProperty]
        public Timestamp ModifiedOn { get; set; }

        [FirestoreProperty]
        public Timestamp LastPrayed { get; set; }
    }
}
