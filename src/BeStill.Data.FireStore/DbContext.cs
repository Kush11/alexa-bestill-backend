﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.FireStore
{
    public class DbContext
    {
        public FirestoreDb database;
        public DbContext()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"bestill-app-firebase-adminsdk.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

            database = FirestoreDb.Create("bestill-app");
        }
    }
}
