﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Helpers
{
    public enum Frequency
    {
        Once = 1,
        Daily = 7,
        Weekly = 14,
        Monthly = 30
    }
}
