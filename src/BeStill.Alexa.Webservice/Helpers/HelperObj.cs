﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Helpers
{
    public abstract class HelperObj
    {
        public static string BaseUrl { get; set; }
        public static string BgUrl { get; set; }
        public static string LogoUrl { get; set; }
    }
}
