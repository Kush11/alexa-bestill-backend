﻿using Alexa.NET.APL;
using Alexa.NET.APL.Components;
using Alexa.NET.APL.JsonConverter;
using Alexa.NET.Response.APL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Helpers
{
    public class CustomGridSequence : ActionableComponent
    {
        public override string Type => nameof(GridSequence);

        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore),
         JsonConverter(typeof(GenericSingleOrListConverter<object>))]
        public APLValue<IList<object>> Data { get; set; }

        [JsonProperty("firstItem", NullValueHandling = NullValueHandling.Ignore)]
        public APLValue<List<APLComponent>> FirstItem { get; set; }

        [JsonProperty("lastItem", NullValueHandling = NullValueHandling.Ignore)]
        public APLValue<List<APLComponent>> LastItem { get; set; }

        [JsonProperty("items", NullValueHandling = NullValueHandling.Ignore),
         JsonConverter(typeof(APLComponentListConverter))]
        public APLValue<IList<APLComponent>> Items { get; set; }

        [JsonProperty("childHeights", NullValueHandling = NullValueHandling.Ignore),
            JsonConverter(typeof(GenericSingleOrListConverter<APLDimensionValue>))]
        public APLValue<IList<APLDimensionValue>> ChildHeights { get; set; }

        [JsonProperty("childWidths", NullValueHandling = NullValueHandling.Ignore),
         JsonConverter(typeof(GenericSingleOrListConverter<APLDimensionValue>))]
        public APLValue<IList<APLDimensionValue>> ChildWidths { get; set; }

        [JsonProperty("childWidth", NullValueHandling = NullValueHandling.Ignore)]
        public APLValue<string> ChildWidth { get; set; }
        [JsonProperty("childHeight", NullValueHandling = NullValueHandling.Ignore)]
        public APLValue<string> ChildHeight { get; set; }

        [JsonProperty("numbered", NullValueHandling = NullValueHandling.Ignore)]
        public APLValue<int?> Numbered { get; set; }

        [JsonProperty("onScroll", NullValueHandling = NullValueHandling.Ignore),
         JsonConverter(typeof(APLCommandListConverter))]
        public APLValue<IList<APLCommand>> OnScroll { get; set; }

        [JsonProperty("scrollDirection", NullValueHandling = NullValueHandling.Ignore),
            JsonConverter(typeof(APLValueEnumConverter<ScrollDirection>))]
        public APLValue<ScrollDirection?> ScrollDirection { get; set; }
    }
}
