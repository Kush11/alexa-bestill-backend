﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alexa.NET.Conversations;
using Alexa.NET.Request;
using AutoMapper;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Handlers;
using BeStill.Alexa.Webservice.Helpers;
using BeStill.Business.Service.ObjectMapper;
using BeStill.Data.Service.Common;
using Json.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BeStill.Alexa.Webservice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
           
            BaseObj.ConnectionString = Configuration.GetConnectionString("ConnectionString");
            var isFireStoreDatabase = Configuration.GetValue<bool>("IsFireStoreDatabase");
            //HelperObj.BgUrl = Configuration.GetValue<string>("BgUrl");
            //HelperObj.LogoUrl = Configuration.GetValue<string>("LogoUrl");
         
            ServiceRegistration.Setup(services, isFireStoreDatabase, true);
            services.AddControllers().AddNewtonsoftJson();



            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            var config = new MapperConfiguration(c => c.AddProfile(new ApplicationProfile()));
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

       
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

       

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            DialogApiInvokedRequest.AddToRequestConverter();
            DialogDelegateRequestDirective.AddSupport();
        }
    }
}
