﻿using Alexa.NET;
using Alexa.NET.APL;
using Alexa.NET.APL.Components;
using Alexa.NET.APL.DataSources;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.APL;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Helpers;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class ViewPrayerIntentHandler : HelperObj, IViewPrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;

        int totalCount = 0;
        int currentPage = 1;


        public ViewPrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string msg = "";
            string screenMsg = "";
            string action = "";

            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
            }
            if (session.Attributes.ContainsKey("currentPage"))
            {
                currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
            }
            if (currentPage == 1)
            {
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    session.Attributes["currentPage"] = 1;
                }
                else
                {
                    session.Attributes.Add("currentPage", 1);
                }
            }
         
            var actual = GetPage(currentPage, 5, uid);
            var screenActual = ComposeList(uid);
            var reminderActual = GetPage(currentPage, 5, uid);




            List<string> prayers = new List<string>();
            List<string> screenPrayers = new List<string>();
            var reminderPrayer = actual.Where(d => d.HasReminder == true).ToList();
            reminderPrayer.ForEach(c => c.Title = string.Concat(c.Title, " ", "🎗️"));
            screenPrayers = actual.Select(c => string.Concat(c.Sort, ". ", c.Title)).ToList();
            prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
            var prayText = screenActual.Count > 1 ? "prayers" : "prayer";




            if (actual.Count > 0)
            {

                var proccessed = ((currentPage - 1) * 5) + 5;
                if (proccessed < totalCount)
                {
                    if (currentPage == 1)
                    {
                        msg = $"You have {screenActual.Count } {prayText}: ";
                        msg += $"{string.Join(". ", prayers)}. Would you like to continue?";


                    }
                    else
                    {
                        msg += $"{string.Join(". ", prayers)}. Would you like to continue?";
                    }
                    screenMsg = $"You have {screenActual.Count} {prayText}: " + "<br/>";
                    screenMsg += $"{string.Join("<br />", screenPrayers)}.";
                    session.Attributes["lastIntent"] = "ViewPrayerIntent";
                    session.Attributes["currentPage"] = currentPage + 1;
                }
                else
                {
                    if (currentPage == 1)
                    {
                        msg = $"You have {screenActual.Count} prayers. ";
                    }
                    //session.Attributes["currentPage"] = 1;
                    msg += $"{string.Join(". ", prayers)}. You can archive a prayer, snooze a prayer, unsnooze a prayer, mark a prayer as answered or view more options. What would you like to do?";
                    screenMsg = $"You have {screenActual.Count} prayers: " + "<br/>";
                    screenMsg += $"{string.Join("<br />", screenPrayers)}.";
                    session.Attributes.Remove("currentPage");

                }



                action = "Actions You Can Take:" + "<br />" +
                    "•	Alexa, Add a Prayer." + "<br />" +
                    "•	Alexa, Mark Number # as Answered" + "<br />" +
                    "•	Alexa, Archive Number #" + "<br />" +
                    "•	Alexa, Snooze Number #" + "<br />" +
                    "•	Alexa, Set Reminder for Number #" + "<br />" +
                    "•	Alexa, Delete Reminder for Number #" + "<br />" +
                    "•	Alexa, Delete Prayer List";

                //if (session.Attributes == null)
                //{
                //    session.Attributes = new Dictionary<string, object>();
                //}
                //session.Attributes.Add("LastIntent", input.Intent.Name);
            }
            else
            {
                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();

                }
                if (session.Attributes != null)
                    if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                //session.Attributes["lastIntent"] = "AddPrayerIntent";
                msg = "You have no prayer in your prayer list. " + '\n' + "What will you like to do? Add a prayer to your list or view more options.";


            }

            Reprompt er = new Reprompt(msg);
            //var resp = ResponseBuilder.Empty();
            var resp = ResponseBuilder.Ask(msg, er, session);

            if (!isAudio)
            {
                if (actual.Count > 0)
                {
                    //resp = ResponseBuilder.Ask("", new Reprompt(""), session);
                    resp = ResponseBuilder.Ask(msg, er, session);
                    resp.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "Your Prayer List", "").Result);
                }
                else
                {
                    resp = ResponseBuilder.Ask(msg, er, session);
                    resp.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Your Prayer List").Result);
                }
            }
            else
            {
                resp = ResponseBuilder.Ask(msg, er, session);
            }
            return Task.FromResult(resp);
        }

        private List<Prayer_DTO> ComposeList(string uid)
        {
            var expiry = DateTime.Now.ToUniversalTime();
            var prayers = _prayerservice.GetPrayers(uid).Result;
            var newPrayers = prayers.Where(c => c.SnoozeExpiryDate <= expiry);
            return newPrayers.ToList();
        }


        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}
