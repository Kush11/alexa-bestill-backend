﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class UnsnoozePrayerIntentHandler : IUnsnoozePrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        int currentPage = 1;

        public UnsnoozePrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;

        }

        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string msg = "";
                string screenMsg = "";
                string action = "";
                string continuePrayer = "say next snoozed prayers to continue reading your list.";
                //var actual = ComposeList(uid);
                action = "Tips on Managing Your Prayers:" + "<br />" +
                                    "•	<b>Snooze a Prayer</b>: To pray for someone less frequently than every day, ask Alexa to snooze a prayer request until a later date. Say, “Alexa, snooze #”" + "<br />" +
                                    "•	<b>Answered Prayers</b>: Keep track of answered prayers by asking Alexa to mark a prayer as answered. Say, “Alexa, update prayer # as answered.” When a prayer is answered, Be Still will move it to your archive." + "<br />" +
                                    "•	<b>Set a Reminder</b>: To remind yourself to pray for a specific request, Say, “Alexa, set reminder for #.”";
                //List<string> prayers = null;
                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();
                }
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
                }
                if (currentPage == 1)
                {
                    if (session.Attributes.ContainsKey("currentPage"))
                    {
                        session.Attributes["currentPage"] = 1;
                    }
                    else
                    {
                        session.Attributes.Add("currentPage", 1);
                    }
                }
                var actual = GetPage(currentPage, 5, uid);
                var actualPrayers = ComposeList(uid);

                if (actual.Count > 0)
                {
                    var prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                    string itm = input.Intent.Slots["id"].Value;
                    var expiry = DateTime.Now.ToUniversalTime();
                    var processed = ((currentPage - 1) * 5) + 5;


                    //string name = input.Intent.Slots["item"].Value;
                    if (string.IsNullOrEmpty(itm))
                    {
                        if (session.Attributes == null)
                        {
                            session.Attributes = new Dictionary<string, object>();
                        }
                        //session.Attributes.Add("LastIntent", input.Intent.Name);
                        if(processed < totalCount)
                        {
                            if(currentPage == 1)
                            {
                                msg = $"Here are your prayers. {string.Join(". ", prayers)}. Which prayer would you like to unsnooze? You can say the number of the prayer or " + continuePrayer;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer do you want to unsnooze? You can say the number of the prayer or " + continuePrayer;
                            }
                            screenMsg = $"Choose a Prayer to Unsnooze: " + "<br/>";
                            screenMsg += $"{string.Join("<br />", prayers)}." + "<br/><br/>" + continuePrayer;
                            session.Attributes["lastIntent"] = "UnsnoozePrayerIntent";
                            session.Attributes["currentPage"] = currentPage + 1;
                        } else
                        {
                            msg = $"{string.Join(". ", prayers)}. Which prayer would you like to unsnooze? You can say the number of the prayer";

                            session.Attributes.Remove("currentPage");
                            screenMsg = $"Choose a Prayer to Unsnooze: " + "<br/>";
                            screenMsg += $"{string.Join("<br />", prayers)}.";
                        }
                        
                        Reprompt er = new Reprompt(msg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        if (!isAudio)
                        {
                            resp.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "UnSnooze Prayer", "", "45vw").Result);
                        }
                        return Task.FromResult(resp);
                    }
                    var prayer = actual.FirstOrDefault(c => c.Sort == int.Parse(itm));
                    if (prayer == null)
                    {
                        msg = $"Prayer number {itm} can't be found." +
                            $" You could view your prayer list, add a new prayer request" +
                            $" or view more options. What would you like to do?";
                    }
                    else
                    {
                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");

                            }
                        }

                   

                        if (input.Intent.ConfirmationStatus == "NONE")
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"You want me to unsnooze {prayer.Title}  correct?");
                            var res = ResponseBuilder.DialogConfirmIntent(speech, input.Intent);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.ConfirmationStatus == "DENIED")
                        {
                            var deniedMsg = "Would you still like to unsnooze a prayer or would you like to view more options?";
                            Reprompt rpt = new Reprompt(deniedMsg);
                            var res = ResponseBuilder.Ask(deniedMsg, rpt, session);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.ConfirmationStatus == "CONFIRMED")
                        {
                            /// add the service
                            _prayerservice.UnsnoozePrayer(prayer.PrayerId, expiry);
                            msg = $"Done. What would you like to do next?";
                        }



                    }
                }
                else
                {
                    msg = "You have no snoozed prayers. You could snooze a prayer, " + " add a new prayer request, " +
                        " view your prayer list" +
                        " or view more options. What would you like to do?";
                }
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Unsnooze Prayer", "", "45vw").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                var msg = "I didn't get that. can you please rephrase your request";
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                return Task.FromResult(response);
            }

        }

        private List<Prayer_DTO> ComposeList(string uid)
        {
            var expiry = DateTime.Now.ToUniversalTime();
            var prayers = _prayerservice.GetPrayers(uid).Result;
            var newPrayers = prayers.Where(c => c.SnoozeExpiryDate > expiry);
            return newPrayers.ToList();
        }
        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}

