﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class PrayerIntentHandler : IPrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        public PrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string msg = "";
            var actual = ComposeList(uid);
            if (actual.Count > 0)
            {
                var prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                
                msg = $"{string.Join("\n", prayers)}";
            }
            else
            {
                if (session.Attributes != null)
                    if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }

                msg = "You have no prayer list";
            }

            Reprompt er = new Reprompt(msg);
            var resp = ResponseBuilder.Empty();
            if (!isAudio)
            {
                if (actual.Count > 0)
                {
                    resp = ResponseBuilder.Ask("", new Reprompt(""), session);
                    resp.Response.Directives.Add(_builder.BuildScrollingSequenceWithVoiceDirective(msg, "Prayer").Result);
                }
                else
                {
                    resp = ResponseBuilder.Ask(msg, er, session);
                    resp.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Prayer").Result);
                }
            }
            else
            {
                resp = ResponseBuilder.Ask(msg, er, session);
            }
            return Task.FromResult(resp);
        }

        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }
    }
}
