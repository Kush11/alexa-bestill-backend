﻿using Alexa.NET;
using Alexa.NET.APL;
using Alexa.NET.APL.Components;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.APL;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class MenuIntentHandler : IMenuIntentHandler
    {
        IDirectiveBuilder _builder;
        public MenuIntentHandler(IDirectiveBuilder builder)
        {
            _builder = builder;
        }

        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;

            var msg = "";
            var screenMsg = "";
            if (session.Attributes != null)
            {
                if (session.Attributes["lastIntent"].ToString() != "NextMenuIntent")
                {
                    msg = "The most frequently used actions you may take are: " +
                       "Add a Prayer to your prayer list, review your list, play today’s devotional, or start prayer time. " +
                       "Would you like to hear other actions you may take?";
                    screenMsg = "You may say the following actions" + "<br />" +
                                "•	Add a Prayer to Your Prayer List," + "<br />" +
                                "•	Review Your List," + "<br />" +
                                "•	Play Today’s Devotional," + "<br />" +
                                "•	Start Prayer Time," + "<br />" +
                                "•	Mark a Prayer as Answered," + "<br />" +
                                "•	Archive a Prayer," + "<br />" +
                                "•	Snooze a Prayer," + "<br />" +
                                "•	Add or Remove Prayer Reminders," + "<br />" +
                                "•	Play Yesterday’s Daily Devotional," + "<br />" +
                                "•	Clear Prayer List." + "<br />" +
                                " What would you like to do?";


                    session.Attributes["lastIntent"] = "NextMenuIntent";

                }
                else
                {
                    msg = "You may also mark a prayer as answered, archive a prayer, snooze a prayer to temporarily hide it until a future date," +
                        "clear your prayer list, add or remove prayer reminders, play yesterday’s devotional. " +
                        "What would you like to do?";

                    //var mainLayout = new Layout(
                    //    new AlexaTextList
                    //    {
                    //        ListItems = new APLValue<List<AlexaTextListItem>>()
                    //        {
                    //            Value = new List<AlexaTextListItem>
                    //            {
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Mark a Prayer as Answered"

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Archive a Prayer"

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Snooze a Prayer"

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Add or Remove Prayer Reminders"

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Play Yesterday’s Daily Devotional"

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "• Clear Prayer List."

                    //                },
                    //                new AlexaTextListItem
                    //                {
                    //                    PrimaryText = "What would you like to do?"

                    //                },
                    //            }
                    //        }
                    //    });
                    session.Attributes["lastIntent"] = "MenuIntent";

                }
            }



            Reprompt rp = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rp, session);
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildDefaultDirective(screenMsg, "Be Still").Result);
            }
            return Task.FromResult(response);

        }
    }
}
