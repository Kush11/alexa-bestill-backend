﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.Ssml;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
//using Google.Type;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class SnoozePrayerIntentHandler : ISnoozePrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        int currentPage = 1;
        public SnoozePrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string msg = "";
                string screenMsg = "";
                string action = "";
                var durationStatement = "";
                string continuePrayer = "say next unsnoozed prayers to continue reading your list.";

                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();
                }
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
                }
                if (currentPage == 1)
                {
                    if (session.Attributes.ContainsKey("currentPage"))
                    {
                        session.Attributes["currentPage"] = 1;
                    }
                    else
                    {
                        session.Attributes.Add("currentPage", 1);
                    }
                }
                var actual = GetPage(currentPage, 5, uid);
                var actualPrayers = ComposeList(uid);
                action = "Tips on Managing Your Prayers:" + "<br />" +
                                    "•	<b>Answered Prayers</b>: Keep track of answered prayers by asking Alexa to mark a prayer as answered. Say, “Alexa, update prayer # as answered.” When a prayer is answered, Be Still will move it to your archive." + "<br />" +
                                    "•	<b>Archive a Prayer</b>: To remove a specific prayer from your list, ask Alexa to archive it. Say, “Alexa, archive prayer #.”" + "<br />" +
                                    "•	<b>Set a Reminder</b>: To remind yourself to pray for a specific request, Say, “Alexa, set reminder for #.”";
                if (actual.Count > 0)
                {
                    var prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                    string itm = input.Intent.Slots["id"].Value;
                    string snoozeDuration = input.Intent.Slots["snoozeDuration"].Value;
                    //string snoozeDate = input.Intent.Slots["snoozeDate"].Value;

                    //var numberOfSnoozeDays = 0;
                    var expiry = DateTime.Now.ToUniversalTime();
                    var processed = ((currentPage - 1) * 5) + 5;


                    //string name = input.Intent.Slots["item"].Value;
                    if (string.IsNullOrEmpty(itm))
                    {
                        if (session.Attributes == null)
                        {
                            session.Attributes = new Dictionary<string, object>();
                        }
                        session.Attributes.Add("LastIntent", input.Intent.Name);
                        if (processed < totalCount)
                        {
                            if (currentPage == 1)
                            {
                                msg = $"Here are your prayers. {string.Join(". ", prayers)}. Which prayer would you like to snooze? You can say the number of the prayer or " + continuePrayer;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer do you want to snooze? You can say the number of the prayer or " + continuePrayer;
                            }
                            screenMsg = $"Choose a Prayer to snooze: " + "<br/>";
                            screenMsg += $"{string.Join("<br />", prayers)}." + "<br/><br/>" + continuePrayer;
                            session.Attributes["LastIntent"] = "SnoozePrayerIntent";
                            session.Attributes["currentPage"] = currentPage + 1;
                        }
                        else
                        {

                            msg = $"{string.Join(". ", prayers)}. Which prayer would you like to snooze? You can say the number of the prayer";
                            session.Attributes.Remove("currentPage");
                            screenMsg = $"Choose a Prayer to Snooze: " + "<br/>";
                            screenMsg += $"{string.Join("<br />", prayers)}.";
                        }


                        Reprompt er = new Reprompt(msg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        if (!isAudio)
                        {
                            resp.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "Snooze Prayer", "", "45vw").Result);
                        }
                        return Task.FromResult(resp);
                    }
                    var prayer = actual.FirstOrDefault(c => c.Sort == int.Parse(itm));
                    if (prayer == null)
                    {
                        msg = $"Prayer number {itm} can't be found." +
                            $" You could view your prayer list, add a new prayer request" +
                            $" or view more options. What would you like to do?";
                    }
                    else
                    {
                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");

                            }
                        }

                        if (string.IsNullOrEmpty(snoozeDuration))
                        {
                            msg = $"How long should this snooze last?";
                            Reprompt er = new Reprompt(msg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "snoozeDuration", session, input.Intent);
                            return Task.FromResult(resp);

                        }

                        else
                        {

                            if (snoozeDuration.Contains("W"))
                            {
                                msg = $"Sorry, be still does not support duration in weeks. Please specify the duration in Years, months, days, hours, minutes or seconds.";
                                Reprompt er = new Reprompt(msg);
                                PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                                var resp = ResponseBuilder.DialogElicitSlot(speech, "snoozeDuration", session, input.Intent);
                                return Task.FromResult(resp);
                            }
                            var duration = System.Xml.XmlConvert.ToTimeSpan(snoozeDuration);
                            var day = duration.Days;
                            var hour = duration.Hours;
                            var minute = duration.Minutes;
                            var second = duration.Seconds;
                            if (day != 0)
                            {
                                expiry = DateTime.Now.AddDays(day).ToUniversalTime();
                                durationStatement = day.ToString();
                                durationStatement += day == 1 ? " day" : " days";

                            }
                            if (hour != 0)
                            {
                                expiry = DateTime.Now.AddHours(hour).ToUniversalTime();
                                durationStatement = hour.ToString();
                                durationStatement += hour == 1 ? " hour" : " hours";


                            }
                            if (minute != 0)
                            {
                                expiry = DateTime.Now.AddMinutes(minute).ToUniversalTime();
                                durationStatement = minute.ToString();
                                durationStatement += minute == 1 ? " minute" : " minutes";


                            }
                            if (second != 0)
                            {
                                expiry = DateTime.Now.AddSeconds(second).ToUniversalTime();
                                durationStatement = second.ToString();
                                durationStatement += second == 1 ? " second" : " seconds";

                            }

                            //else if (!string.IsNullOrEmpty(snoozeDate))
                            //{
                            //    var newExpiry = (snoozeDate.Contains('/')) ? snoozeDate : VerifyDate(snoozeDate).ToShortDateString();
                            //    expiry = DateTime.Parse(newExpiry).ToUniversalTime();
                            //}
                        }

                        //var expiryDate = expiry.ToString("dd/MM/yyyy");
                        var expiryDate = expiry.ToShortDateString();
                        var expiryTime = expiry.ToShortTimeString();
                        if (input.Intent.ConfirmationStatus == "NONE")
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"You want me to snooze {prayer.Title} until {expiryDate} at {expiryTime}. Is that correct?");
                            var res = ResponseBuilder.DialogConfirmIntent(speech, input.Intent);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.ConfirmationStatus == "DENIED")
                        {
                            var deniedMsg = "Would you still like to snooze a prayer or would you like more options?";
                            Reprompt rpt = new Reprompt(deniedMsg);
                            var res = ResponseBuilder.Ask(deniedMsg, rpt, session);
                            return Task.FromResult(res);
                            //input.Intent.ConfirmationStatus = "NONE";
                            //msg = $"Ok, how long should this snooze last?";
                            //Reprompt er = new Reprompt(msg);
                            //PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            //var resp = ResponseBuilder.DialogElicitSlot(speech, "snoozeDuration", session, input.Intent);
                            //return Task.FromResult(resp);
                        }
                        if (input.Intent.ConfirmationStatus == "CONFIRMED")
                        {
                            /// add the service
                            _prayerservice.SnoozePrayer(prayer.PrayerId, expiry);
                            msg = $"Done. Your prayer will be unsnoozed on {expiryDate} at {expiryTime}. What would you like to do next?";
                            session.Attributes.Remove("LastIntent");
                        }



                    }
                }
                else
                {
                    msg = "You have no prayers. You could add a new prayer request," +
                        " view your prayer list" +
                        " or view more options. What would you like to do?";
                }
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Snooze Prayer", "", "45vw").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                var msg = "I didn't get that. Please rephrase your request";
                Reprompt er = new Reprompt(msg);
                PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                var resp = ResponseBuilder.DialogElicitSlot(speech, "snoozeDuration", session, input.Intent);
                return Task.FromResult(resp);
            }

        }

        private List<Prayer_DTO> ComposeList(string uid)
        {
            var expiry = DateTime.Now.ToUniversalTime();
            var prayers = _prayerservice.GetPrayers(uid).Result;
            var newPrayers = prayers.Where(c => c.SnoozeExpiryDate <= expiry);
            return newPrayers.ToList();
        }
        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }

        private DateTime VerifyDate(string date)
        {
            var dates = date.Split('-');
            string day = "", month = "", year = "";
            if (dates.Length < 3)
            {
                if (dates.Length < 2)
                {
                    year = dates[0];
                    month = DateTime.Now.Month.ToString();
                    day = DateTime.Now.Day.ToString();
                }
                else
                {
                    year = dates[0];
                    if (dates[1].Contains('W'))
                    {
                        var weeknumber = dates[1].Replace("W", "");
                        var week = FirstDateOfWeekISO8601(int.Parse(year), int.Parse(weeknumber));
                        month = week.Month.ToString();
                        day = week.Day.ToString();
                    }
                    else
                    {
                        month = dates[1];
                        day = DateTime.Now.Day.ToString();
                    }
                }
            }
            else
            {
                year = dates[0]; month = dates[1]; day = dates[2];
            }

            return new DateTime(int.Parse(year), int.Parse(month), int.Parse(day));
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

    }
}
