﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.Ssml;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class ArchivePrayerIntentHandler : IArchivePrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        int currentPage = 1;

        public ArchivePrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string msg = "";
                string screenMsg = "";
                string action = "";
                string continuePrayer = "Say continue prayer list to continue reading your prayers.";
                //var actual = ComposeList(uid);
                action = "Tips on Managing Your Prayers:" + "<br />" +
                                    "•	<b>Snooze a Prayer</b>: To pray for someone less frequently than every day, ask Alexa to snooze a prayer request until a later date. Say, “Alexa, snooze #”" + "<br />" +
                                    "•	<b>Answered Prayers</b>: Keep track of answered prayers by asking Alexa to mark a prayer as answered. Say, “Alexa, update prayer # as answered.” When a prayer is answered, Be Still will move it to your archive." + "<br />" +
                                    "•	<b>Set a Reminder</b>: To remind yourself to pray for a specific request, Say, “Alexa, set reminder for #.”";
                List<string> prayers = null;
                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();
                }
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
                }
                if (currentPage == 1)
                {
                    if (session.Attributes.ContainsKey("currentPage"))
                    {
                        session.Attributes["currentPage"] = 1;
                    }
                    else
                    {
                        session.Attributes.Add("currentPage", 1);
                    }
                }
                var actual = GetPage(currentPage, 5, uid);
                var actualPrayers = ComposeList(uid);


                if (actual.Count > 0)
                {
                    string title = input.Intent.Slots["title"].Value;
                    var processed = ((currentPage - 1) * 5) + 5;

                    if (!string.IsNullOrEmpty(title))
                    {
                        if (!title.Contains("number"))
                        {
                            prayers = actual.Where(c => c.Title.ToLower().Contains(title.ToLower())).Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                        }
                    }
                    else
                    {
                        prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                    }
                    string itm = input.Intent.Slots["id"].Value;
                    if (string.IsNullOrEmpty(itm) || !string.IsNullOrEmpty(title))
                    {
                        if (session.Attributes == null)
                        {
                            session.Attributes = new Dictionary<string, object>();
                        }
                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");
                            }
                        }
                        session.Attributes.Add("LastIntent", input.Intent.Name);
                        if (prayers.Count == 0)
                        {
                            prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();

                            if (processed < totalCount)
                            {
                                if (currentPage == 1)
                                {
                                    msg = $"{title} can't be found. ";
                                    msg += $"Which prayer would you like to archive? You can say the number of the prayer. {string.Join(". ", prayers)}. " + continuePrayer;

                                }
                                else
                                {
                                    msg = $"Which prayer would you like to archive? You can say the number of the prayer. {string.Join(". ", prayers)}. " + continuePrayer;
                                }
                                screenMsg = $"Choose a Prayer to Archive: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}." + "<br/><br/>" + continuePrayer;
                                session.Attributes["lastIntent"] = "ArchivePrayerIntent";
                                session.Attributes["currentPage"] = currentPage + 1;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer would you like to archive? You can say the number of the prayer";
                                session.Attributes.Remove("currentPage");
                                screenMsg = $"Choose a Prayer to Archive: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                            }
                            //msg += $"Here are your prayers: \n {string.Join("\n", prayers)} \n What item number will you like to archive?";
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            if (!isAudio)
                            {
                                resp.Response.Directives.Add(_builder.BuildScrollingSequenceDirective(msg, "Archive Prayer").Result);
                            }
                            return Task.FromResult(resp);
                        }
                        else if (prayers.Count == 1 && !string.IsNullOrEmpty(title))
                        {
                            var pry = actual.FirstOrDefault(c => c.Title.ToLower().Contains(title.ToLower()));
                            itm = pry.Sort.ToString();
                            input.Intent.Slots["id"].Value = itm;
                        }
                        else
                        {
                            if (processed < totalCount)
                            {
                                if (currentPage == 1)
                                {
                                    msg = $"Which prayer would you like to archive? You can say the number of the prayer. {string.Join(". ", prayers)}." + " " + continuePrayer;

                                }
                                else
                                {
                                    msg = $"{string.Join(". ", prayers)}. Which prayer do you want to archive? You can say the number of the prayer " + "or " + continuePrayer.ToLower();
                                }
                                screenMsg = $"Choose a Prayer to Archive: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}." + "<br/><br/>" + continuePrayer;
                                session.Attributes["lastIntent"] = "ArchivePrayerIntent";
                                session.Attributes["currentPage"] = currentPage + 1;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer would you like to archive? You can say the number of the prayer";
                                session.Attributes.Remove("currentPage");
                                screenMsg = $"Choose a Prayer to Archive: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                            }


                            Reprompt er = new Reprompt(msg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            if (!isAudio)
                            {
                                resp.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "Archive Prayer", "", "50vw", "50px").Result);
                            }
                            return Task.FromResult(resp);
                        }
                    }
                    else if (!string.IsNullOrEmpty(itm) && itm.Equals("?"))
                    {
                        msg = $"I did not get that, please say the number of the prayer you want to archive.";

                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                    var prayer = actualPrayers.FirstOrDefault(c => c.Sort == int.Parse(itm));
                    if (prayer == null)
                    {
                        msg = $"Prayer number {itm} can't be found." +
                            $" You could view your prayer list, add a new prayer request" +
                            $" or view more options. What would you like to do?";
                    }
                    else
                    {
                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");

                            }
                        }

                        if (input.Intent.Slots["id"].ConfirmationStatus == "NONE")
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"You want me to archive {prayer.Title} correct?");
                            var res = ResponseBuilder.DialogConfirmSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "DENIED")
                        {
                            input.Intent.Slots["id"].ConfirmationStatus = "NONE";
                            var deniedMsg = "Ok, which prayer would you like to archive? please say the number of the prayer.";
                            Reprompt rpt = new Reprompt(deniedMsg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(deniedMsg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(resp);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "CONFIRMED")
                        {
                            /// add the service
                            _prayerservice.ArchivePrayer(prayer.PrayerId);
                            msg = $"Done. What would you like to do next?";
                        }



                    }
                }
                else
                {
                    msg = "You have no prayer list. You could add a new prayer request," +
                        " view your prayer list" +
                        " or view more options. What would you like to do?";
                }
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Archive Prayer", "", "50vw", "50px").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                var msg = "I didn't get that. can you please rephrase your request";
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                return Task.FromResult(response);
            }

        }

        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }
        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}
