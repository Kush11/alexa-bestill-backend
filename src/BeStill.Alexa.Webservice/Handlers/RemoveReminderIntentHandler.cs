﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class RemoveReminderIntentHandler : IRemoveReminderIntentHandler
    {
        IReminderService _reminderservice;
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        private readonly ILogger<RemoveReminderIntentHandler> _logger;
        public RemoveReminderIntentHandler(IReminderService reminderservice, IDirectiveBuilder builder, IPrayerService prayerservice, ILogger<RemoveReminderIntentHandler> logger)
        {
            _reminderservice = reminderservice;
            _prayerservice = prayerservice;
            _builder = builder;
            _logger = logger;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string msg = "";
                string action = "<b>Tips to Managing Reminders</b>:" + "<br />" +
                        "•	Alexa, Set Reminder for #" + "<br />" +
                        "•	Alexa, Remove Reminder for #" + "<br />" +
                        "•	Alexa, List Reminders";
                var actual = ComposeList(uid);
                var prayerActual = ComposePrayerList(uid);
                if (actual.Count > 0)
                {

                    var reminders = actual.Select(d =>
                    {
                        var freq = d.Frequency;
                        var nextoccurence = nextOccurence(d);

                        var prayerObj = prayerActual.FirstOrDefault(c => c.PrayerId.ToString() == d.PrayerId);
                        var sortNumber = prayerObj != null ? prayerObj.Sort.ToString() : "NA";

                        var day = (freq == 7 && DateTime.Now > nextoccurence) ? "Tomorrow"
                        : ((freq < 7 || freq == 7) && DateTime.Now <= nextoccurence && d.StartDate.ToShortDateString() == DateTime.Now.AddDays(1).ToShortDateString()) ? "Tomorrow"
                        : ((freq < 7 && DateTime.Now <= nextoccurence && (DateTime.Now.ToShortDateString() == nextoccurence.ToShortDateString())) || (freq == 7 && DateTime.Now <= nextoccurence && (DateTime.Now.AddDays(1).ToShortDateString() == nextoccurence.ToShortDateString()))) ? "Today"
                        : nextoccurence.ToString("dddd dd MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                        var str = "";
                        if (prayerObj != null)
                        {
                            str = string.Concat("Number ", sortNumber, ". ", d.Title, " at ", d.StartDate.ToShortTimeString(), " ", (day == "Today" || day == "Tomorrow") ? day : "on " + day, ".");
                        }
                        return str;

                    }).Where(c => !string.IsNullOrEmpty(c)).ToList();
                    string itm = input.Intent.Slots["id"].Value;

                    if (string.IsNullOrEmpty(itm))
                    {
                        if (session.Attributes == null)
                        {
                            session.Attributes = new Dictionary<string, object>();
                        }
                        if (session.Attributes.ContainsKey("LastIntent"))
                        {
                            session.Attributes["LastIntent"] = input.Intent.Name;
                        }
                        else
                        {
                            session.Attributes.Add("LastIntent", input.Intent.Name);
                        }
                        if (reminders.Count > 0)
                        {
                            msg = "Which prayer's reminder do you want to remove? You can say the number of the prayer";

                        } else
                        {
                            msg = $"You have {reminders.Count} reminders. Would you like to add a new reminder?";
                            session.Attributes["lastIntent"] = "AddReminderIntent";
                            Reprompt rprompt = new Reprompt(msg);
                            var res = ResponseBuilder.Ask(msg, rprompt, session);
                            return Task.FromResult(res);
                        }

                        //msg = $" Choose a prayer to be reminded at a later date {string.Join("\n", reminders)} \n What item number will you like to delete?";
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        if (!isAudio)
                        {
                            var prayers = prayerActual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                            var dMsg = "";
                            if (reminders.Count > 0)
                            {
                                dMsg = $"Choose the reminder you want remove: " + "<br />" + $" {string.Join("<br />", reminders)}";

                            } else
                            {
                               dMsg = $"You have {reminders.Count} reminders.";

                            }
                            resp.Response.Directives.Add(_builder.BuildGridListDirective(dMsg, action, "Remove Reminder Item").Result);
                        }
                        return Task.FromResult(resp);
                    }
                    else if (!string.IsNullOrEmpty(itm) && itm.Equals("?"))
                    {
                        msg = $"I did not get that, please say the number of the prayer you want to remove the reminder for.";
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                    Reminder_DTO reminder = null;
                    var prayer = prayerActual.FirstOrDefault(c => c.Sort == int.Parse(itm));
                    if (prayer != null)
                    {
                        reminder = actual.FirstOrDefault(c => c.PrayerId == prayer.PrayerId.ToString());
                    }

                    if (reminder == null)
                    {
                        msg = $"Item number {itm} can't be found";
                    }
                    else
                    {
                        //if (input.Intent.ConfirmationStatus != "DENIED")
                        //{

                        //} else
                        //{
                        //    //
                        //    msg = "Okay, Great!" + '\n' + "Would you like to add a new reminder or view your reminder list?";
                        //}

                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                            if (session.Attributes.ContainsKey("lastIntent")) { session.Attributes.Remove("lastIntent"); }
                        }
                        if (session.Attributes == null)
                        {
                            session.Attributes = new Dictionary<string, object>();
                            session.Attributes.Add("lastIntent", "RemoveReminderIntent");
                        }
                        else
                        {
                            session.Attributes["lastIntent"] = "RemoveReminderIntent";
                        }
                        /// TODO add native service
                        var nextoccurence = nextOccurence(reminder);
                        if (!string.IsNullOrEmpty(reminder.Token))
                        {
                            try
                            {
                                var client = new RemindersClient(request);
                                client.Delete(reminder.Token).Wait();
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, ex.Message);
                            }

                        }

                        if (input.Intent.Slots["id"].ConfirmationStatus == "NONE")
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"You want me to remove {reminder.Title} from your reminders correct?");
                            var res = ResponseBuilder.DialogConfirmSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "DENIED")
                        {
                            input.Intent.Slots["id"].ConfirmationStatus = "NONE";
                            var deniedMsg = "which prayer's reminder would you like to remove? please say the number of the prayer";
                            Reprompt rpt = new Reprompt(deniedMsg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(deniedMsg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(resp);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "CONFIRMED")
                        {
                            /// add the service
                            _reminderservice.Delete(reminder.ReminderId);
                            msg = "Done. What would you like to do next?";
                        }

                    }
                }
                else
                {
                    msg = "You have no reminder on your list." + '\n' + "Would you like to add a new reminder or view your prayer list?";
                }
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Remove Reminder Item").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                var res = ResponseBuilder.DialogDelegate(session, input.Intent);
                return Task.FromResult(res);
            }

        }

        private List<Reminder_DTO> ComposeList(string uid)
        {

            return _reminderservice.GetReminders(uid).Result;
        }
        private List<Prayer_DTO> ComposePrayerList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }


        private DateTime nextOccurence(Reminder_DTO d)
        {
            DateTime _next = DateTime.Now;
            switch (d.Frequency)
            {
                case 7:
                    _next = DateTime.Now.AddDays(1);
                    break;
                case 14:
                    _next = DateTime.Now.AddDays(7);
                    break;
                case 30:
                    _next = DateTime.Now.AddMonths(1);
                    break;
            }
            var _nextoccur = (d.Frequency > 1) ? _next.ToShortDateString() : DateTime.Now.ToShortDateString();
            return DateTime.Parse($"{_nextoccur} {d.StartDate.ToShortTimeString()}");
        }
    }
}