﻿using Alexa.NET.APL;
using Alexa.NET.APL.Commands;
using Alexa.NET.APL.Components;
using Alexa.NET.APL.DataSources;
using Alexa.NET.Response;
using Alexa.NET.Response.APL;
using Alexa.NET.Response.Directive;
using Alexa.NET.Response.Ssml;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class DirectiveBuilder : HelperObj, IDirectiveBuilder
    {

        public Task<RenderDocumentDirective> BuildTextDirective(string sentences, string bg = "")
        {
            if (string.IsNullOrEmpty(bg))
            {
                bg = $"{BaseUrl}/images/alexabg.jpg";

            }
            var mainLayout = new Layout(
                new Container(
                    new Image(bg)
                    {
                        Width = "100vw",
                        Height = "100vh",
                        Position = new APLValue<string>("absolute"),
                        Scale = new APLValue<Scale?>(Scale.BestFill)
                    },
                    new Text(sentences)
                    {
                        FontSize = "40dp",
                        Style = new APLValue<string>("textStyleBody"),
                        TextAlign = new APLValue<string>("center"),
                        Id = "talker",
                        Content = APLValue.To<string>("${payload.script.properties.text}"),
                        Speech = APLValue.To<string>("${payload.script.properties.speech}")
                    }
                )
                { Width = "100vw", Height = "100vh", JustifyContent = new APLValue<string>("center"), Grow = new APLValue<int?>(1) }
            );
            mainLayout.Parameters = new List<Parameter>();
            mainLayout.Parameters.Add(new Parameter("payload"));

            var speech = new Speech(new PlainText(sentences));
            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = mainLayout
                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = new Dictionary<string, object>
                            {
                                { "ssml", speech.ToXml()}
                            },
                            Transformers = new List<APLTransformer>{
                                APLTransformer.SsmlToText(
                                    "ssml",
                                    "text"),
                                APLTransformer.SsmlToSpeech(
                                    "ssml",
                                    "speech")
                            }
                        }

                    }
                }

            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuildDefaultDirective(string sentences, string title = "", string subtitle = "")
        {
            var layout = new Layout
            {
                Description = "Main template",
                Parameters = new List<Parameter> { new Parameter("payload") },
                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Container
                                 {
                                     Grow = new APLValue<int?>(1),
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 40,
                                     Items = new APLValue<IList<APLComponent>>
                                     {
                                         Value = new List<APLComponent>
                                         {

                                             new Text(sentences)
                                             {
                                                FontSize = "30dp",
                                                Style = new APLValue<string>("textStyleBody"),
                                                Spacing = 12
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                    }
                }
            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }
                }

            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuildItemListDirective(string sentences, string title = "", string subtitle = "")
        {
            var items = new List<AlexaTextListItem>();
            foreach (var item in sentences.Split("\n"))
            {
                items.Add(new AlexaTextListItem { PrimaryText = item });
            }
            var layout = new Layout
            {
                Description = "Main template",
                Parameters = new List<Parameter> { new Parameter("payload") },
                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                        Items=new APLValue<IList<APLComponent>>
                        {
                            Value = new List<APLComponent>
                            {

                                new AlexaTextList
                                {
                                    BackgroundImageSource = BgUrl,
                                    HeaderTitle = title,
                                    HeaderSubtitle = subtitle,
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl),
                                    HeaderDivider = true,
                                    ColorOverlay = new APLValue<bool?>(true),
                                    BackgroundScale = new APLValue<Scale>(Scale.BestFill),
                                    BackgroundAlign = new APLValue<string>("center"),
                                    BackgroundColor = new APLValue<string>("transparent"),
                                    ListItems = new APLValue<List<AlexaTextListItem>>()
                                    {
                                        Value = items
                                    }
                                }
                            }
                        }
                    }
                }
            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }
                }

            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuildGridListDirective(string sentences, string actions, string title = "", string subtitle = "", string childwidth = "50vw", string paddingRight = "20px")
        {

            var mainLayout = new Layout(

                new Container
                {
                    Height = "100vh",
                    Width = "100vw",
                    PaddingLeft = "20px",
                    PaddingRight = "20px",
                   
                    Items = new APLValue<IList<APLComponent>>
                    {
                        Value = new List<APLComponent>
                        {
                            new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new CustomGridSequence
                                 {
                                     Width = "97vw",
                                     Height ="90vh",
                                     ChildWidths=new APLValue<IList<APLDimensionValue>>
                                     {
                                        Value = new List<APLDimensionValue>
                                        {
                                            new APLDimensionValue(Dimension.From("60%")),
                                            new APLDimensionValue(Dimension.From("40%"))
                                        }
                                     },
                                     ChildHeight="100%",
                                     ChildWidth=childwidth,
                                     Numbering="normal",
                                     Data = new APLValue<IList<object>>
                                     {
                                         Value = new List<object>
                                         {
                                             sentences,
                                             actions
                                         }
                                     },
                                     Items = new APLValue<IList<APLComponent>>
                                     {
                                         Value = new List<APLComponent>
                                         {
                                             new Text("${data}")
                                                {
                                                    FontSize = "22dp",
                                                    Style = new APLValue<string>("textStyleBody"),
                                                    PaddingRight = paddingRight

                                                }

                                         }
                                     },
                                     ScrollDirection = new APLValue<ScrollDirection?>
                                     {
                                         Value = ScrollDirection.Horizontal
                                     }
                                 }
                        }
                    },
                    JustifyContent = new APLValue<string>("center"),
                    Grow = new APLValue<int?>(1)

                }
            ); 
            mainLayout.Parameters = new List<Parameter>();
            mainLayout.Parameters.Add(new Parameter("payload"));

            var speech = new Speech(new PlainText(sentences));
            var renderDocument = new RenderDocumentDirective
            {
                
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = mainLayout,
                    Version = APLDocumentVersion.V1_4,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.2.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = new Dictionary<string, object>
                            {
                                { "ssml", speech.ToXml()}
                            },
                            Transformers = new List<APLTransformer>{
                                APLTransformer.SsmlToText(
                                    "ssml",
                                    "text"),
                                APLTransformer.SsmlToSpeech(
                                    "ssml",
                                    "speech")
                            }
                        }

                    }
                }

            };
            return Task.FromResult(renderDocument);
        }

        public Task<RenderDocumentDirective> BuildDataListDirective(string sentences, string title = "", string subtitle = "")
        {
            var items = new List<APLComponent>();
            var prayers = sentences.Split("\n");
            for (int i = 0; i < prayers.Length; i++)
            {
                items.Add(new Text($"{prayers[i]}")
                {
                    FontSize = "30dp",
                    Spacing = 12,
                    PaddingLeft = 60,
                    PaddingRight = 60
                });
            }
            var layout = new Layout
            {
                Description = "Main template",
                Parameters = new List<Parameter> { new Parameter("payload") },
                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Container
                                 {
                                     Grow = new APLValue<int?>(1),
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 40,
                                     Items = new APLValue<IList<APLComponent>>
                                     {
                                         Value = items
                                     }
                                 }
                             }
                         }
                    }
                }
            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }
                }

            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuildScrollingTextDirective(string sentences, string title = "", string subtitle = "")
        {

            var speech = new Speech(new PlainText(sentences));

            var layout = new Layout
            {
                Parameters = new List<Parameter> { new Parameter("payload") },

                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new ScrollView(
                                    new Text(sentences)
                                    {
                                        FontSize = "40dp",
                                        TextAlign = "Center",
                                        Id = "talker",
                                        Content = APLValue.To<string>("${payload.script.properties.text}"),
                                        Speech = APLValue.To<string>("${payload.script.properties.speech}"),
                                        OnMount = new APLValue<IList<APLCommand>>
                                        {
                                            Value = new List<APLCommand>
                                            {
                                                new SpeakItem
                                                {
                                                    ComponentId = "talker",
                                                    HighlightMode = new APLValue<HighlightMode?>(HighlightMode.Line)
                                                }
                                            }
                                        }
                                    }
                                )
                                { Width = "100vw", Height = "100vh",PaddingLeft = 60, PaddingRight = 60, PaddingBottom = 140 }
                             }
                         }
                    }
                }

            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = new Dictionary<string, object>
                            {
                                { "ssml", speech.ToXml()}
                            },
                            Transformers = new List<APLTransformer>{
                                APLTransformer.SsmlToText(
                                    "ssml",
                                    "text"),
                                APLTransformer.SsmlToSpeech(
                                    "ssml",
                                    "speech")
                            }
                        }
                    }
                }
            };
            return Task.FromResult(renderDocument);

        }
        public Task<RenderDocumentDirective> BuildScrollingSequenceDirective(string sentences, string title = "", string subtitle = "")
        {
            var items = new List<APLComponent>();
            var prayers = sentences.Split("\n");
            for (int i = 0; i < prayers.Length; i++)
            {
                items.Add(new Text()
                {
                    Id = $"itm{i.ToString()}",
                    Content = APLValue.To<string>("${payload.script.properties.text" + i.ToString() + "}"),
                    //Speech = APLValue.To<string>("${payload.script.properties.speech" + i.ToString() + "}")
                });
            }

            var ppt = new Dictionary<string, object>();
            Speech speech = null;
            for (int i = 0; i < prayers.Length; i++)
            {
                speech = new Speech(new PlainText(prayers[i]));
                ppt.Add($"ssml{i}", speech.ToXml());
            }

            var ff = new List<APLTransformer>();
            for (int i = 0; i < prayers.Length; i++)
            {
                ff.Add(APLTransformer.SsmlToText($"ssml{i}", $"text{i}"));
                ff.Add(APLTransformer.SsmlToSpeech($"ssml{i}", $"speech{i}"));
            }

            var layout = new Layout
            {
                Parameters = new List<Parameter> { new Parameter("payload") },

                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Sequence
                                 {
                                    Id = "mySequence",
                                    Width = "100vw",
                                    Height = "100vh",
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 140,
                                    Items = new APLValue<List<APLComponent>>
                                    {
                                        Value = items
                                    },
                                    OnMount = new APLValue<IList<APLCommand>>
                                    {
                                        Value = new List<APLCommand>
                                        {
                                            new SpeakList
                                            {
                                                ComponentId = "mySequence",
                                                Start = new APLValue<int>(0),
                                                Count = new APLValue<int>(prayers.Length),
                                                MinimumDwellTime = new APLValue<int?>(2000),
                                                Align = new APLValue<ItemAlignment?>(ItemAlignment.Center),
                                                ScreenLock = new APLValue<bool?>(true)
                                            }
                                        }
                                    }
                                 }


                             }
                         }
                    }
                }

            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = ppt,
                            Transformers = ff
                        }
                    }
                }
            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuilduietTimeQScrollingSequenceDirective(string sentences, int quietTime, string title = "", string subtitle = "")
        {
            var items = new List<APLComponent>();
            var prayers = sentences.Split("\n");
            for (int i = 0; i < prayers.Length; i++)
            {
                items.Add(new Text()
                {
                    Id = $"itm{i.ToString()}",
                    Content = APLValue.To<string>("${payload.script.properties.text" + i.ToString() + "}"),
                    Speech = APLValue.To<string>("${payload.script.properties.speech" + i.ToString() + "}")
                });
            }

            var ppt = new Dictionary<string, object>();
            Speech speech = null;
            for (int i = 0; i < prayers.Length; i++)
            {
                speech = new Speech(new PlainText(prayers[i]));
                ppt.Add($"ssml{i}", speech.ToXml());
            }

            var ff = new List<APLTransformer>();
            for (int i = 0; i < prayers.Length; i++)
            {
                ff.Add(APLTransformer.SsmlToText($"ssml{i}", $"text{i}"));
                ff.Add(APLTransformer.SsmlToSpeech($"ssml{i}", $"speech{i}"));
            }

            var layout = new Layout
            {
                Parameters = new List<Parameter> { new Parameter("payload") },

                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new AlexaBackground
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    VideoAudioTrack = new APLValue<string>("none"),
                                    BackgroundVideoSource = new APLValue<VideoSource>
                                    {
                                        Value = new VideoSource
                                        {
                                            Uri = new APLValue<Uri>(new Uri($"{BaseUrl}/videos/swf.mp4")),
                                            RepeatCount = new APLValue<int?>(10)
                                        }
                                    },
                                    VideoAutoPlay = new APLValue<bool?>(true)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Sequence
                                 {
                                    Id = "mySequence",
                                    Width = "100vw",
                                    Height = "100vh",
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 140,
                                    Items = new APLValue<List<APLComponent>>
                                    {
                                        Value = items
                                    },
                                    OnMount = new APLValue<IList<APLCommand>>
                                    {
                                        Value = new List<APLCommand>
                                        {
                                            new SpeakList
                                            {
                                                ComponentId = "mySequence",
                                                Start = new APLValue<int>(0),
                                                Count = new APLValue<int>(prayers.Length),
                                                MinimumDwellTime = new APLValue<int?>((quietTime * 1000) + 5000),
                                                Align = new APLValue<ItemAlignment?>(ItemAlignment.Center)
                                            }
                                        }
                                    }
                                 }


                             }
                         }
                    }
                }

            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = ppt,
                            Transformers = ff
                        }
                    }
                }
            };
            return Task.FromResult(renderDocument);
        }
        public Task<RenderDocumentDirective> BuildAutoPagerDirective(string sentences, int quietTime, string title = "", string subtitle = "")
        {
            var items = new List<APLComponent>();
            var prayers = sentences.Split("\n");
            for (int i = 0; i < prayers.Length; i++)
            {
                items.Add(new Text()
                {
                    Id = $"itm{i.ToString()}",
                    Width = "100vw",
                    Height = "100vh",
                    PaddingLeft = 40,
                    PaddingRight = 40,
                    TextAlignVertical = new APLValue<string>("center"),
                    TextAlign = new APLValue<string>("center"),
                    AlignSelf = new APLValue<string>("center"),
                    Content = APLValue.To<string>("${payload.script.properties.text" + i.ToString() + "}"),
                    //Speech = APLValue.To<string>("${payload.script.properties.speech" + i.ToString() + "}")
                });
            }

            var ppt = new Dictionary<string, object>();
            Speech speech = null;
            for (int i = 0; i < prayers.Length; i++)
            {
                speech = new Speech(new PlainText(prayers[i]));
                ppt.Add($"ssml{i}", speech.ToXml());
            }

            var ff = new List<APLTransformer>();
            for (int i = 0; i < prayers.Length; i++)
            {
                ff.Add(APLTransformer.SsmlToText($"ssml{i}", $"text{i}"));
                ff.Add(APLTransformer.SsmlToSpeech($"ssml{i}", $"speech{i}"));
            }

            var layout = new Layout
            {
                Parameters = new List<Parameter> { new Parameter("payload") },

                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new AlexaBackground
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    VideoAudioTrack = new APLValue<string>("none"),
                                    BackgroundVideoSource = new APLValue<VideoSource>
                                    {
                                        Value = new VideoSource
                                        {
                                            Uri = new APLValue<Uri>(new Uri($"{BaseUrl}/videos/swf.mp4")),
                                            RepeatCount = new APLValue<int?>(15)
                                        }
                                    },
                                    VideoAutoPlay = new APLValue<bool?>(true)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                 },

                                 new Pager
                                 {
                                    Id = "myPager",
                                    Width = "100vw",
                                    Height = "100vh",
                                    PaddingBottom = 140,
                                    AlignSelf = new APLValue<string>("center"),
                                    Items = new APLValue<List<APLComponent>>
                                    {
                                        Value = items
                                    },

                                    OnMount = new APLValue<IList<APLCommand>>
                                    {
                                        Value = new List<APLCommand>
                                        {
                                            new AutoPage
                                            {
                                                ComponentId = "myPager",
                                                ScreenLock = new APLValue<bool?>(true),
                                                DelayMilliseconds = new APLValue<int?>((quietTime * 1000) + 2000),
                                                Count = new APLValue<int?>(items.Count),
                                                Duration = new APLValue<int?>((quietTime * 1000) + 4000),
                                            }

                                        }
                                    }
                                 }



                             }
                         }
                    }
                }

            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = ppt,
                            Transformers = ff
                        }
                    }
                }
            };
            return Task.FromResult(renderDocument);



        }
        public Task<AudioPlayerPlayDirective> BuildAudioDirective(string audioUrl, string title, string subtitle, string bg = "")
        {
            if (string.IsNullOrEmpty(bg))
            {
                bg = $"{BaseUrl}/images/alexabg.jpg";

            }
            var audioPlayer = new AudioPlayerPlayDirective()
            {
                AudioItem = new AudioItem()
                {
                    Stream = new AudioItemStream()
                    {
                        Url = audioUrl,
                        Token = title
                    },
                    Metadata = new AudioItemMetadata
                    {
                        Title = title,
                        Subtitle = subtitle,
                        Art = new AudioItemSources
                        {
                            Sources = new List<AudioItemSource>
                            {
                                new AudioItemSource
                                {
                                    Url = bg
                                }
                            }
                        },
                        BackgroundImage = new AudioItemSources
                        {
                            Sources = new List<AudioItemSource>
                            {
                                new AudioItemSource
                                {
                                    Url = bg
                                }
                            }
                        },

                    },

                }

            };
            return Task.FromResult(audioPlayer);
        }
        public Task<RenderDocumentDirective> BuildScrollingSequenceWithVoiceDirective(string sentences, string title = "", string subtitle = "")
        {
            var items = new List<APLComponent>();
            var prayers = sentences.Split("\n");
            for (int i = 0; i < prayers.Length; i++)
            {
                items.Add(new Text()
                {
                    Id = $"itm{i.ToString()}",
                    Content = APLValue.To<string>("${payload.script.properties.text" + i.ToString() + "}"),
                    Speech = APLValue.To<string>("${payload.script.properties.speech" + i.ToString() + "}")
                });
            }

            var ppt = new Dictionary<string, object>();
            Speech speech = null;
            for (int i = 0; i < prayers.Length; i++)
            {
                speech = new Speech(new PlainText(prayers[i]));
                ppt.Add($"ssml{i}", speech.ToXml());
            }

            var ff = new List<APLTransformer>();
            for (int i = 0; i < prayers.Length; i++)
            {
                ff.Add(APLTransformer.SsmlToText($"ssml{i}", $"text{i}"));
                ff.Add(APLTransformer.SsmlToSpeech($"ssml{i}", $"speech{i}"));
            }

            var layout = new Layout
            {
                Parameters = new List<Parameter> { new Parameter("payload") },

                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<IList<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Sequence
                                 {
                                    Id = "mySequence",
                                    Width = "100vw",
                                    Height = "100vh",
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 140,

                                    Items = new APLValue<List<APLComponent>>
                                    {
                                        Value = items
                                    },
                                    OnMount = new APLValue<IList<APLCommand>>
                                    {
                                        Value = new List<APLCommand>
                                        {
                                            new SpeakList
                                            {
                                                ComponentId = "mySequence",
                                                Start = new APLValue<int>(0),
                                                Count = new APLValue<int>(prayers.Length),
                                                MinimumDwellTime = new APLValue<int?>(2000),
                                                Align = new APLValue<ItemAlignment?>(ItemAlignment.Center),
                                                ScreenLock = new APLValue<bool?>(true)
                                            }
                                        }
                                    }
                                 }


                             }
                         }
                    }
                }

            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }

                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = ppt,
                            Transformers = ff
                        }
                    }
                }
            };
            return Task.FromResult(renderDocument);
        }
    }
}
