﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class AddMorePrayerIntentHandler : IAddMorePrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        private readonly ILogger<AddMorePrayerIntentHandler> _logger;
        public AddMorePrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder, ILogger<AddMorePrayerIntentHandler> logger)
        {
            _prayerservice = prayerservice;
            _builder = builder;
            _logger = logger;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            var updatedIntent = input.Intent;
            
            string type = input.Intent.Slots["prayfor"].Value;
            string itm = input.Intent.Slots["body"].Value;
            string msg = "";
            Reprompt rp = new Reprompt(msg);
            _logger.LogInformation("AddMorePrayerIntentHandler");
            var response = ResponseBuilder.Ask(msg, rp, session);
            if (string.IsNullOrEmpty(type))
            {
                msg = "what do you want to pray for?";
                PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
                speech.Text = msg;
                _logger.LogInformation(msg);
                response = ResponseBuilder.DialogElicitSlot(speech, "prayfor", session, updatedIntent);
                displayDeviceResponseBuilder(isAudio, msg, response);

            }
            else if (string.IsNullOrEmpty(itm))
            {
                msg = "what is your prayer request";
                PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
                speech.Text = msg;
                _logger.LogInformation(msg);
                response = ResponseBuilder.DialogElicitSlot(speech, "prayfor", session, updatedIntent);
                displayDeviceResponseBuilder(isAudio, msg, response);
            }
            else
            {
                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();

                }
                if (string.IsNullOrEmpty(itm))
                {
                    msg = "I didn't get that. Please ask again.";
                }
                else
                {
                    if (session.Attributes != null)
                    {
                        if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                        //_prayerservice.Insert(uid, type, $"Pray for {type}", itm, "Alexa").Wait();
                        msg = $"Prayer has been added. Would you like to view your prayer list or view more options?";

                    }

                }

                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Prayer").Result);
                }
            }
            return Task.FromResult(response);
        }

        private void displayDeviceResponseBuilder(bool isAudio, string msg, SkillResponse response)
        {
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Reminder").Result);
            }
        }
    }
}
