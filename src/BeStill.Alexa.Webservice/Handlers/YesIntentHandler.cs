﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class YesIntentHandler : IYesIntentHandler
    {
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest input, bool isAudio = false)
        {
            Session session = input.Session;
            IntentRequest request = (IntentRequest)input.Request;
            var updatedIntent = request.Intent;
            if (session.Attributes != null)
            {
                if (session.Attributes.ContainsKey("lastIntent"))
                {
                    if (session.Attributes["lastIntent"].ToString() == "MenuIntent")
                    {
                        updatedIntent.Name = "MenuIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "NextMenuIntent")
                    {
                        updatedIntent.Name = "MenuIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "ViewPrayerIntent")
                    {
                        updatedIntent.Name = "ViewPrayerIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "AddPrayerIntent")
                    {
                        updatedIntent.Name = "AddPrayerIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "RemovePrayerIntent")
                    {
                        updatedIntent.Name = "RemovePrayerIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "ArchivePrayerIntent")
                    {
                        updatedIntent.Name = "ArchivePrayerIntent";
                        var res = ResponseBuilder.DialogDelegate(session, updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "AnsweredPrayerIntent")
                    {
                        updatedIntent.Name = "AnsweredPrayerIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "StartPrayerIntent")
                    {
                        updatedIntent.Name = "ASMIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }

                    if ((session.Attributes["lastIntent"].ToString() == "AddReminderIntent") || (session.Attributes["lastIntent"].ToString() == "RemoveReminderIntent"))
                    {
                        updatedIntent.Name = "ViewReminderIntent";
                        var res = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(res);
                    }
                    if (session.Attributes["lastIntent"].ToString() == "ViewReminderIntent")
                    {
                        updatedIntent.Name = "RemoveReminderIntent";
                        var msg = "Which prayer's reminder do you want to remove? You can say the number of the prayer";
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var res = ResponseBuilder.DialogElicitSlot(speech, "id", session, updatedIntent);
                        return Task.FromResult(res);

                    }
                }
               
            }
            updatedIntent.Name = "ViewPrayerIntent";
            var response = ResponseBuilder.DialogDelegate(updatedIntent);
            return Task.FromResult(response);
        }
    }
}
