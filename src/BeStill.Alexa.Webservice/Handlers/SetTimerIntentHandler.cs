﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class SetTimerIntentHandler : ISetTimerIntentHandler
    {
        IUserPreferenceService _userPreferenceService;
        IDirectiveBuilder _builder;
        public SetTimerIntentHandler(IUserPreferenceService userPreferenceService, IDirectiveBuilder builder)
        {
            _userPreferenceService = userPreferenceService;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string action = "", msg = "";
            try
            {
                //string msg = "";
                string qt = input.Intent.Slots["prayerTime"].Value;
                string bgm = input.Intent.Slots["backgroundMusic"].Value;

                if (session.Attributes != null)
                    if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                /// add the service
                if (input.DialogState == "STARTED")
                {
                    var resp = ResponseBuilder.DialogDelegate(input.Intent);
                    return Task.FromResult(resp);
                }
                if (!string.IsNullOrEmpty(qt))
                {
                    if (input.Intent.Slots["prayerTime"].ConfirmationStatus == "NONE")
                    {
                        if (!qt.Contains("10") && !qt.Contains("20") && !qt.Contains("30") && !qt.Contains("40") && !qt.Contains("60") && !qt.Contains("1"))
                        {
                            input.Intent.Slots["prayerTime"].ConfirmationStatus = "NONE";
                            var slotMsg = $"Ok, wrong option selected, do you want ten, twenty, thirty, fourty seconds or one minute?";
                            Reprompt er = new Reprompt(slotMsg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "prayerTime", session, input.Intent);
                            return Task.FromResult(resp);
                        }
                        else
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"you want to your quiet time to be {qt}. correct?");
                            var res = ResponseBuilder.DialogConfirmSlot(speech, "prayerTime", session, input.Intent);
                            return Task.FromResult(res);
                        }
                    }
                    if (input.Intent.Slots["prayerTime"].ConfirmationStatus == "DENIED")
                    {

                        input.Intent.Slots["prayerTime"].ConfirmationStatus = "NONE";
                        var slotMsg = $"Ok, How long would you like your quiet time to be. ten, twenty, thirty, fourty seconds or one minute?";
                        Reprompt er = new Reprompt(slotMsg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "prayerTime", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                }
                 if (string.IsNullOrEmpty(bgm))
                {
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"Ok, what type of music would you like to play during your prayers. Instrument, Melody or Silent?");
                    var res = ResponseBuilder.DialogElicitSlot(speech, "backgroundMusic", session, input.Intent);
                    return Task.FromResult(res);
                }
                else if (!string.IsNullOrEmpty(bgm))
                {
                    if (input.Intent.Slots["backgroundMusic"].ConfirmationStatus == "NONE")
                    {
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"you said {bgm}. is that correct?");
                        var res = ResponseBuilder.DialogConfirmSlot(speech, "backgroundMusic", session, input.Intent);
                        return Task.FromResult(res);
                    }
                    if (input.Intent.Slots["backgroundMusic"].ConfirmationStatus == "DENIED")
                    {

                        input.Intent.Slots["backgroundMusic"].ConfirmationStatus = "NONE";
                        var slotMsg = $"Ok, what type of music would you like to play during your prayers. Instrument, Melody or Silent?";
                        Reprompt er = new Reprompt(slotMsg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "backgroundMusic", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                }

                if (input.Intent.Slots["prayerTime"].ConfirmationStatus == "CONFIRMED" && input.Intent.Slots["backgroundMusic"].ConfirmationStatus == "CONFIRMED")
                {
                    if (bgm.ToLower() == "none")
                        bgm = "silent";
                    if (qt.ToLower() == "1 minute" || qt.ToLower() == "one minute" || qt.ToLower() == "1")
                        qt = "60";
                    var sp = qt.Split(" ");
                    qt = sp[0];
                    var num = 0;
                    if (int.TryParse(qt, out num))
                    {
                        _userPreferenceService.InsertOrUpdate(uid, Convert.ToInt32(qt), bgm, uid);
                        msg = $"Alright. {qt} seconds has been successfully set as your quiet time and your background music has been set to {bgm}. What else would you like to do?";
                        action = "Tips on Managing Your Prayers:" + "<br />" +
                                            "•	<b>Snooze a Prayer</b>: To pray for someone less frequently than every day, ask Alexa to snooze a prayer request until a later date. Say, “Alexa, snooze #”" + "<br />" +
                                            "•	<b>Answered Prayers</b>: Keep track of answered prayers by asking Alexa to mark a prayer as answered. Say, “Alexa, update prayer # as answered.” When a prayer is answered, Be Still will move it to your archive." + "<br />" +
                                            "•	<b>Archive a Prayer</b>: To remove a specific prayer from your list, ask Alexa to archive it. Say, “Alexa, archive prayer #.”" + "<br />" +
                                            "•	<b>Set a Reminder</b>: To remind yourself to prayer for a specific request, Say, “Alexa, set reminder for #.”";
                                        
                    }
                    else
                    {
                        input.Intent.Slots["prayerTime"].ConfirmationStatus = "NONE";
                        var slotMsg = $"Ok, do you want ten, twenty, thirty, fourty seconds or one minute?";
                        Reprompt er = new Reprompt(slotMsg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "prayerTime", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                }
                    Reprompt rp = new Reprompt(msg);
                    var response = ResponseBuilder.Ask(msg, rp, session);
                    if (!isAudio)
                    {
                        response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Set Quiet Time").Result);
                    }
                    return Task.FromResult(response);
                
            }
            catch (Exception e)
            {
                var res = ResponseBuilder.DialogDelegate(session, input.Intent);
                return Task.FromResult(res);
            }
        }
    }
}
