﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Reminders;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Alexa.NET.Response.Directive;
using Microsoft.Extensions.Logging;
using BeStill.Abstraction.DTO;
using Alexa.NET.Conversations;
using System.Threading.Tasks;
using IntentRequest = Alexa.NET.Request.Type.IntentRequest;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class AddReminderIntentHandler : IAddReminderIntentHandler
    {
        IReminderService _reminderService;
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        int currentPage = 1;
        private readonly ILogger<AddReminderIntentHandler> _logger;
        public AddReminderIntentHandler(IReminderService reminderservice, IDirectiveBuilder builder, IPrayerService prayerservice, ILogger<AddReminderIntentHandler> logger)
        {
            _reminderService = reminderservice;
            _prayerservice = prayerservice;
            _builder = builder;
            _logger = logger;
        }
        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {



            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string title = "";
                string freq = input.Intent.Slots["frequency"].Value;
                string start = input.Intent.Slots["startdate"].Value;
                string end = input.Intent.Slots["enddate"].Value;
                string time = input.Intent.Slots["time"].Value;
                string slotMsg = "";
                string prayerId = null;
                string continuePrayer = "Say next prayers to continue reading your prayer requests";
                string continuePrayerHeader = "Choose a prayer to be reminded at a later date and time:";
                string action = "<b>Tips to Managing Reminders</b>:" + "<br />" +
                        "•	Alexa, Set Reminder for #" + "<br />" +
                        "•	Alexa, Remove Reminder for #" + "<br />" +
                        "•	Alexa, List Reminders";
                //var actual = ComposeList(uid);
                Prayer_DTO prayer = null;

                string id = input.Intent.Slots["id"].Value;
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
                }
                if (currentPage == 1)
                {
                    if (session.Attributes.ContainsKey("currentPage"))
                    {
                        session.Attributes["currentPage"] = 1;
                    }
                    else
                    {
                        session.Attributes.Add("currentPage", 1);
                    }
                }
                var actual = GetPage(currentPage, 5, uid);
                var actualPrayers = ComposeList(uid);
                var processed = ((currentPage - 1) * 5) + 5;



                if (string.IsNullOrEmpty(id))
                {
                    if (session.Attributes == null)
                    {
                        session.Attributes = new Dictionary<string, object>();
                        session.Attributes.Add("LastIntent", input.Intent.Name);
                        session.Attributes.Add("lastIntent", input.Intent.Name);
                    }
                    else
                    {
                        session.Attributes["LastIntent"] = input.Intent.Name;
                        session.Attributes["lastIntent"] = input.Intent.Name;
                    }
                    //slotMsg = "";
                    var prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();

                    if (processed < totalCount)
                    {
                        if (currentPage == 1)
                        {
                            slotMsg += $"Here are your prayers:  {string.Join(". ", prayers)}. Which prayer would you like to be reminded? You can say the number of the prayer or " + continuePrayer;
                        }
                        else
                        {
                            slotMsg = $" {string.Join(". ", prayers)}. " + continuePrayer;
                        }
                        //slotMsg = $"Here are your prayers: {string.Join(". ", prayers)}. Which prayer would you like to be reminded? You can say the number of the prayer or say next prayers to continue reading your prayer list.";
                        session.Attributes["lastIntent"] = "AddReminderIntent";
                        session.Attributes["currentPage"] = currentPage + 1;
                    }
                    else
                    {
                        slotMsg = $"{string.Join(". ", prayers)}. Which prayer would you like to be reminded? You can say the number of the prayer.";
                        session.Attributes.Remove("currentPage");
                    }
                    Reprompt er = new Reprompt(slotMsg);
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                    var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                    if (!isAudio)
                    {
                        //var prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                        var dMsg = "";
                        if (processed < totalCount)
                        {

                            dMsg = continuePrayerHeader + "<br/>" + $" {string.Join("<br/>", prayers)}.";
                            session.Attributes["currentPage"] = currentPage + 1;
                        }
                        else
                        {
                            dMsg = continuePrayerHeader + "<br/>" + $" {string.Join("<br/>", prayers)}.";
                            session.Attributes.Remove("currentPage");
                        }
                        resp.Response.Directives.Add(_builder.BuildGridListDirective(dMsg, action, "Add Reminder", "", "60vw").Result);
                    }
                    return Task.FromResult(resp);
                }
                else if (!string.IsNullOrEmpty(id) && id.Equals("?"))
                {
                    slotMsg = $"I did not get that, please say the number of the prayer you want to add the reminder for.";

                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                    var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                    return Task.FromResult(resp);
                }
                else
                {
                    prayer = actualPrayers.FirstOrDefault(c => c.Sort == int.Parse(id));

                }
                if (prayer == null)
                {
                    slotMsg = $"Prayer number {id} can't be found." +
                            $" You could view your prayer list. add a new prayer request" +
                            $" or view more options. What would you like to do?";

                    Reprompt rp = new Reprompt(slotMsg);
                    var response = ResponseBuilder.Ask(slotMsg, rp, session);
                    response.Response.Directives.Add(_builder.BuildDefaultDirective(slotMsg, "Reminder").Result);
                    return Task.FromResult(response);
                }
                else
                {
                    title = prayer.Title;
                    prayerId = prayer.PrayerId.ToString();
                }


                string msg = $"Done." + '\n' + "Would you like to see your reminders?";
                string endDateStatus = input.Intent.Slots["enddate"].ConfirmationStatus;

                var updatedIntent = input.Intent;

                if (endDateStatus != "DENIED")
                {
                    if (input.DialogState == "STARTED")
                    {
                        var resp = ResponseBuilder.DialogDelegate(updatedIntent);
                        return Task.FromResult(resp);
                    }
                    else if (input.DialogState != "COMPLETED")
                    {
                        if (freq != null && freq == "once")
                        {
                            if (start != null)
                            {
                                GetSlotsValueAndSaveInDB(session, request, uid, title, freq, start, start, time, prayerId);
                                //PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
                                //speech.Text = msg;
                                Reprompt rpt = new Reprompt(msg);
                                var resp = ResponseBuilder.Ask(msg, rpt, session);
                                //var resp = ResponseBuilder.DialogElicitSlot(speech, "startdate", session, updatedIntent);
                                displayDeviceResponseBuilder(isAudio, msg, action, resp);
                                return Task.FromResult(resp);
                            }
                            var del = ResponseBuilder.DialogDelegate(updatedIntent);
                            return Task.FromResult(del);
                        }
                        else
                        {
                            if (end != null)
                                input.Intent.Slots["enddate"].Value = (end.Contains('/')) ? end : VerifyDate(end).ToShortDateString();

                            var res = ResponseBuilder.DialogDelegate(updatedIntent);
                            return Task.FromResult(res);
                        }
                    }

                    var rst = GetSlotsValueAndSaveInDB(session, request, uid, title, freq, start, end, time, prayerId).Result;
                    if (rst)
                    {
                        Reprompt rp = new Reprompt(msg);
                        var response = ResponseBuilder.Ask(msg, rp, session);
                        displayDeviceResponseBuilder(isAudio, msg, action, response);
                        return Task.FromResult(response);
                    }
                    else
                    {
                        String reminderText = "please go to the Alexa mobile app on your phone to grant native reminders permissions.";
                        Reprompt r = new Reprompt(reminderText);
                        var resp = ResponseBuilder.TellWithAskForPermissionConsentCard(reminderText, new string[] { "alexa::alerts:reminders:skill:readwrite" }, session);
                        return Task.FromResult(resp);
                    }
                }
                else
                {
                    var res = ResponseBuilder.DialogDelegate(updatedIntent);
                    return Task.FromResult(res);
                }
            }
            catch (Exception e)
            {
                var res = ResponseBuilder.DialogDelegate(session, input.Intent);
                return Task.FromResult(res);
            }
        }

        private void displayDeviceResponseBuilder(bool isAudio, string msg, string action, SkillResponse response)
        {
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildGridListDirective(msg, action, "Reminder").Result);
            }
        }



        private Task<bool> GetSlotsValueAndSaveInDB(Session session, SkillRequest request, string uid, string title, string freq, string start, string end, string time, string prayerId)
        {

            DateTime startTime = DateTime.Parse($"{VerifyDate(start).ToShortDateString()} {time}");
            _logger.LogInformation($"{title} {freq} {start} {end} {time}");
            DateTime endTime = DateTime.Parse($"{end} {time}");
            Frequency frequency = (freq.ToLower() == "daily") ? Frequency.Daily : (freq.ToLower() == "weekly") ? Frequency.Weekly : Frequency.Once;


            if (session.Attributes != null)
            {
                if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                if (session.Attributes.ContainsKey("lastIntent")) { session.Attributes.Remove("lastIntent"); }
            }
            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
            }
            session.Attributes.Add("lastIntent", "AddReminderIntent");


            // TODO Add native reminder
            var user = request.Context.System.User;
            String token = null;
            if (user.Permissions == null)
            {
                return Task.FromResult(false);
            }
            else
            {
                token = CreateReminder(request, startTime, endTime, title, freq).Result;
            }
            _reminderService.Insert(uid, (int)frequency, token, startTime, endTime, title, "Alexa", prayerId).Wait();

            return Task.FromResult(true);
        }



        private DateTime VerifyDate(string date)
        {
            var dates = date.Split('-');
            string day = "", month = "", year = "";
            if (dates.Length < 3)
            {
                if (dates.Length < 2)
                {
                    year = dates[0];
                    month = DateTime.Now.Month.ToString();
                    day = DateTime.Now.Day.ToString();
                }
                else
                {
                    year = dates[0];
                    if (dates[1].Contains('W'))
                    {
                        var weeknumber = dates[1].Replace("W", "");
                        var week = FirstDateOfWeekISO8601(int.Parse(year), int.Parse(weeknumber));
                        month = week.Month.ToString();
                        day = week.Day.ToString();
                    }
                    else
                    {
                        month = dates[1];
                        day = DateTime.Now.Day.ToString();
                    }
                }
            }
            else
            {
                year = dates[0]; month = dates[1]; day = dates[2];
            }

            return new DateTime(int.Parse(year), int.Parse(month), int.Parse(day));
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        public async Task<string> CreateReminder(SkillRequest request, DateTime start, DateTime end, string title, string freq)
        {
            string Token = null;
            try
            {
                List<string> days = new List<string>();
                if (freq.ToUpper() == "WEEKLY")
                {
                    var dayofweek = start.DayOfWeek.ToString().Substring(0, 2);
                    days.Add(dayofweek.ToUpper());
                }
                Trigger trigger;

                if (freq.ToLower() != "once")
                {
                    var reoccurence = new Recurrence(freq.ToUpper(), days.ToArray());
                    trigger = new AbsoluteTrigger(start, reoccurence);
                }
                else
                {
                    DateTime now = DateTime.Now;
                    var difference = (int)(end - now).TotalSeconds;
                    trigger = new RelativeTrigger(difference);
                }

                var reminder = new Reminder
                {
                    RequestTime = DateTime.Now,
                    Trigger = trigger,
                    AlertInformation = new AlertInformation(new[] { new SpokenContent(title, "en-US") }),
                    PushNotification = PushNotification.Enabled
                };

                _logger.LogInformation(Newtonsoft.Json.JsonConvert.SerializeObject(reminder));
                var client = new RemindersClient(request);
                var alertDetail = await client.Create(reminder);
                Token = alertDetail.AlertToken;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

            }

            return Token;
        }
        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var expiry = DateTime.Now.ToUniversalTime();
            var unSnoozedPrayers = ComposeList(uid);
            var actual = unSnoozedPrayers.Where(c => c.SnoozeExpiryDate >= expiry).ToList();
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}
