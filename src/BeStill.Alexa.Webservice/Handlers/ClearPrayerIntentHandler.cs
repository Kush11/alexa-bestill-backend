﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class ClearPrayerIntentHandler : IClearPrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        public ClearPrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            var response = ResponseBuilder.Empty();
            var msg = "";
            string screenMsg = "";
            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
            }

            if (input.Intent.ConfirmationStatus != "DENIED")
            {
                if (session.Attributes != null)
                    if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }
                var actual = ComposeList(uid);
                if(actual.Count > 0)
                {
                    _prayerservice.DeleteAll(uid);
                    msg = $"Done!" +
                        $" Do you want to start fresh and add a prayer to your list?";
                    screenMsg = "Clearing your list will delete app prayers from your list both on Alexa and in the Be Still app. " + "<br/>" +
                        "To delete or clear an individual prayer, say “Alexa archive prayer number #” or “Alexa, archive followed by the name of the prayer.”";

                } else
                {
                    msg = $"Your prayer list is empty. Would you like to add a new prayer to your prayer list?";
                      
                }


                session.Attributes["lastIntent"] = "AddPrayerIntent";
                Reprompt rp = new Reprompt(msg);
                response = ResponseBuilder.Ask(msg, rp, session);
                //return Task.FromResult(response);
            } else
            {
                msg = "Ok, what do you want to do?";
                screenMsg = "Ok, what do you want to do?";
                session.Attributes["lastIntent"] = "MenuIntent";
                Reprompt rp = new Reprompt(msg);
                response = ResponseBuilder.Ask(msg, rp, session);
            }
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildDefaultDirective(screenMsg, "Clear Prayer").Result);
            }
            return Task.FromResult(response);

            //else
            //{
            //    return Task.FromResult(ResponseBuilder.Empty());
            //}
        }

        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }
    }
}
