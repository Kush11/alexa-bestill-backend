﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class ASMIntentHandler: IASMIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        private readonly ILogger<ASMIntentHandler> _logger;
        public ASMIntentHandler(IDirectiveBuilder builder, IPrayerService prayerservice, ILogger<ASMIntentHandler> logger)
        {
            _prayerservice = prayerservice;
            _builder = builder;
            _logger = logger;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string msg = "";
            var prayerActual = ComposePrayerList(uid);
            var option = input.Intent.Slots["option"].Value;
           // var id = input.Intent.Slots["id"].Value;


            if (string.IsNullOrEmpty(option))
            {
                msg = $"which would you like to perform. archive, snooze or mark a prayer answered?";
                PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                var slotResp = ResponseBuilder.DialogElicitSlot(speech, "option", session, input.Intent);
                if (!isAudio)
                {
                    slotResp.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Option").Result);
                    //var prayers = prayerActual.Select(d => string.Concat(d.Sort, ". ", d.Title, ", ", d.Description)).ToList();
                    //var dMsg = $"Choose a prayer you want to archive, snooze or mark as answered: \n {string.Join("\n", prayers)}";
                    //slotResp.Response.Directives.Add(_builder.BuildScrollingSequenceDirective(dMsg, "Prayer List").Result);
                }
                return Task.FromResult(slotResp);
            }
            //if (string.IsNullOrEmpty(id))
            //{
            //    if(option.ToLower() == "archive")
            //    {
            //        msg = "Which prayerr would you like to archive? You can say the number of the prayer";
            //    }
            //    else if (option.ToLower() == "snooze")
            //    {
            //        msg = "Which prayerr would you like to snooze? You can say the number of the prayer";
            //    }
            //    else if (option.ToLower() == "mark")
            //    {
            //        msg = "Which prayerr would you like to mark as answered? You can say the number of the prayer";
            //    }
            //    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
            //    var slotResp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
            //    if (!isAudio)
            //    {
            //        var prayers = prayerActual.Select(d => string.Concat(d.Sort, ". ", d.Title, ", ", d.Description)).ToList();
            //        var dMsg = $"Choose a prayer you want to archive, snooze or mark as answered: \n {string.Join("\n", prayers)}";
            //        slotResp.Response.Directives.Add(_builder.BuildScrollingSequenceDirective(dMsg, "Prayer List").Result);
            //    }
            //    return Task.FromResult(slotResp);
            //}

            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
                session.Attributes.Add("lastIntent", "ASMIntent");
            }
            else
            {
                session.Attributes["lastIntent"] = "ASMIntent";
            }

            IntentRequest requestObj = (IntentRequest)request.Request;
            var updatedIntent = requestObj.Intent;

            if (option.ToLower() == "archive")
            {
                updatedIntent.Name = "ArchivePrayerIntent";
                var res = ResponseBuilder.DialogDelegate(updatedIntent);
                return Task.FromResult(res);
            }
            else if (option.ToLower() == "snooze")
            {
                msg = "Snooze prayer not available yet";
            }
            else if (option.ToLower() == "mark")
            {
                updatedIntent.Name = "AnsweredPrayerIntent";
                var res = ResponseBuilder.DialogDelegate(updatedIntent);
                return Task.FromResult(res);
            }
            else if (option.ToLower() == "add")
            {
                updatedIntent.Name = "AddPrayerIntent";
                var res = ResponseBuilder.DialogDelegate(updatedIntent);
                return Task.FromResult(res);
            }


            Reprompt rp = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rp, session);
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Remove Reminder Item").Result);
            }
            return Task.FromResult(response);
        }

       
        private List<Prayer_DTO> ComposePrayerList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }


    }
}
