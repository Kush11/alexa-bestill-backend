﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class NoIntentHandler : INoIntentHandler
    {
        IDirectiveBuilder _builder;
        public NoIntentHandler(IDirectiveBuilder builder)
        {
            _builder = builder;
        }

        public Task<SkillResponse> HandleIntent(string uid, SkillRequest input, bool isAudio = false)
        {
            string pry_msg;
            Session session = input.Session;
            IntentRequest request = (IntentRequest)input.Request;
            //var updatedIntent = request.Intent;
            var msg = "Ok, what would you like to do?";
            session.Attributes.Remove("currentPage");

            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
            }
            if (session.Attributes.ContainsKey("lastIntent"))
            {   
                //if(session.Attributes["lastIntent"].ToString() == "ArchivePrayerIntent")
                //{
                //    updatedIntent.Name = "ArchivePrayerIntent";
                //    msg = "Which prayer do you want to archive? You can say the number of the prayer";
                //    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                //    var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, updatedIntent);
                //    return Task.FromResult(resp);
                //}
                Reprompt rep = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rep, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Be Still").Result);
                }
                return Task.FromResult(response);
            }
            pry_msg = $"Would you like to hear other actions you may take?";
            session.Attributes["lastIntent"] = "MenuIntent";
            Reprompt rp = new Reprompt(pry_msg);
            var res = ResponseBuilder.Ask(pry_msg, rp, session);
            if (!isAudio)
            {
                res.Response.Directives.Add(_builder.BuildDefaultDirective(pry_msg, "Be Still").Result);
            }
            return Task.FromResult(res);

        }
    }
}
