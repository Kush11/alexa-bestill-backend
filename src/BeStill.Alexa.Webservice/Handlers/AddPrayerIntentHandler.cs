﻿using Alexa.NET;
using Alexa.NET.Conversations;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntentRequest = Alexa.NET.Request.Type.IntentRequest;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class AddPrayerIntentHandler : IAddPrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        public AddPrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {

            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string title = input.Intent.Slots["title"].Value;

                //string itm = input.Intent.Slots["description"].Value;
                string msg = "";
                string action = "";
                string screenMsg = "";

                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();

                }
                if (session.Attributes.ContainsKey("LastIntent"))
                {
                    session.Attributes.Remove("LastIntent");
                }


                if (title.ToLower() == "prayer")
                {
                    var slotMsg = $"what would you like to pray for?";
                    Reprompt er = new Reprompt(slotMsg);
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                    var resp = ResponseBuilder.DialogElicitSlot(speech, "title", session, input.Intent);
                    return Task.FromResult(resp);
                }




                if (input.Intent.Slots["title"].ConfirmationStatus == "NONE")
                {
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"you want to add {title} to your prayer list correct?");
                    var res = ResponseBuilder.DialogConfirmSlot(speech, "title", session, input.Intent);
                    return Task.FromResult(res);
                }
                if (input.Intent.Slots["title"].ConfirmationStatus == "DENIED")
                {

                    input.Intent.Slots["title"].ConfirmationStatus = "NONE";
                    var slotMsg = $"Ok, what would you like to pray for?";
                    Reprompt er = new Reprompt(slotMsg);
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(slotMsg);
                    var resp = ResponseBuilder.DialogElicitSlot(speech, "title", session, input.Intent);
                    return Task.FromResult(resp);
                }
                if (input.Intent.Slots["title"].ConfirmationStatus == "CONFIRMED")
                {
                    var prayerTitle = title.Contains("pray for") ? title : $"pray for {title}";
                    _prayerservice.Insert(uid, prayerTitle, prayerTitle, "Alexa").Wait();
                    screenMsg = $"The following prayer has been added: {title}. <br /> Say, “Start Prayer Time” to have Alexa read your prayer list with pauses between each item. To change the timing, say, Set PrayerTime.";
                    msg = "Got it!  Would you like to add another prayer or view your list?.";
                    action = "Tips on Managing Your Prayers:" + "<br /><br />" +
                                "•	<b>Snooze a Prayer</b>: To pray for someone less frequently than every day, ask Alexa to snooze a prayer request until a later date. Say, “Alexa, snooze #”" + "<br />" +
                                "•	<b>Answered Prayers</b>: Keep track of answered prayers by asking Alexa to mark a prayer as answered. Say, “Alexa, update prayer # as answered.” When a prayer is answered, Be Still will move it to your archive.”" + "<br />" +
                                "•	<b>Archive a Prayer</b>: To remove a specific prayer from your list, ask Alexa to archive it. Say, “Alexa, archive prayer #.”" + "<br />" +
                                "•	<b>Set a Reminder</b>: To remind yourself to pray for a specific request, Say, “Alexa, set reminder for #.”";


                }

                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "Prayer", "", "45vw").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                var msg = "I didn't get that. can you please rephrase your request";
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                return Task.FromResult(response);
            }

        }
    }
}
