﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class PlayDevotionalIntentHandler : IPlayDevotionalIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        public PlayDevotionalIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            var resp = ResponseBuilder.Empty();
            var day = input.Intent.Slots["day"].Value;
            string devotionalDay = "20200706";
            DayOfWeek weekDay = DateTime.Now.DayOfWeek;
            if (!string.IsNullOrEmpty(day))
            {
                day = day.Replace("'s", "");
                if (day.ToLower().Contains("today"))
                {
                    weekDay = DateTime.Now.DayOfWeek;
                }
                else if (day.ToLower().Contains("tomorrow"))
                {
                    weekDay = DateTime.Now.AddDays(1).DayOfWeek;
                }
                else if (day.ToLower().Contains("yesterday"))
                {
                    weekDay = DateTime.Now.AddDays(-1).DayOfWeek;
                }
            }
            if ((weekDay == DayOfWeek.Saturday) || (weekDay == DayOfWeek.Sunday))
            {
                var msg = $"No devotional on weekend, You ask me to play yesterday, today or tomorrow's devotional.";
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp);
                return Task.FromResult(response);
            }
            else
            {
                if (!string.IsNullOrEmpty(day))
                {
                    day = day.Replace("'s", "");
                    if (day.ToLower().Contains("today"))
                    {
                        devotionalDay = DateTime.UtcNow.ToString("yyyyMMdd");
                    }
                    else if (day.ToLower().Contains("tomorrow"))
                    {
                        devotionalDay = DateTime.UtcNow.AddDays(1).ToString("yyyyMMdd");
                    }
                    else if (day.ToLower().Contains("yesterday"))
                    {
                        devotionalDay = DateTime.UtcNow.AddDays(-1).ToString("yyyyMMdd");
                    }
                    else if (day.ToLower().Contains("next"))
                    {
                        devotionalDay = DateTime.UtcNow.AddDays(1).ToString("yyyyMMdd");
                    }
                    else
                    {
                        var msg = $"Be still does not support {day}, You can ask me to play yesterday's, today's or tomorrow's devotional.";
                        Reprompt rp = new Reprompt(msg);
                        var response = ResponseBuilder.Ask(msg, rp);
                        return Task.FromResult(response);
                    }

                    var audioUrl = "https://djejflde2khrv.cloudfront.net/broadcasts/desktop/win20200706.mp3".Replace("20200706", devotionalDay);
                    var d = day.ToUpper() == "NEXT" ? "TOMORROW'S" : day.ToUpper();
                    var audioPlayer = _builder.BuildAudioDirective(audioUrl, $"{d}'S DEVOTIONAL", "The Winning walk").Result;
                    resp.Response.Directives.Add(audioPlayer);
                }
                else
                {
                    var msg = $"I did not get that, try again.";
                    Reprompt rp = new Reprompt(msg);
                    var response = ResponseBuilder.Ask(msg, rp);
                    return Task.FromResult(response);
                }
            }
            
            return Task.FromResult(resp);
        }
    }
}
