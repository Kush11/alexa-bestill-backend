﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class RemovePrayerHandler : IRemovePrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        int currentPage = 1;
        public RemovePrayerHandler(IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            try
            {
                string msg = "";
                string screenMsg = "";
                string continuePrayer = "Say read next prayers to continue reading your prayers.";

               
                List<string> prayers = null;

                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();
                }
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
                }
                if (currentPage == 1)
                {
                    if (session.Attributes.ContainsKey("currentPage"))
                    {
                        session.Attributes["currentPage"] = 1;
                    }
                    else
                    {
                        session.Attributes.Add("currentPage", 1);
                    }
                }
                var actual = GetPage(currentPage, 5, uid);
                var actualPrayers = ComposeList(uid);

                if (actual.Count > 0)
                {
                    string title = input.Intent.Slots["title"].Value;
                    var processed = ((currentPage - 1) * 5) + 5;

                    if (!string.IsNullOrEmpty(title))
                    {
                        if (!title.Contains("number"))
                        {
                            prayers = actual.Where(c => c.Title.ToLower().Contains(title.ToLower())).Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                        }
                    }
                    else
                    {
                        prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                    }
                    string itm = input.Intent.Slots["id"].Value;

                    if (string.IsNullOrEmpty(itm) || !string.IsNullOrEmpty(title))
                    {

                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");
                            }
                        }
                        session.Attributes.Add("LastIntent", input.Intent.Name);
                        if (prayers.Count == 0)
                        {
                            prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                            if (processed < totalCount)
                            {
                                if (currentPage == 1)
                                {
                                    msg = $"{title} can't be found. ";
                                    msg += $"Here are your prayers:  {string.Join(". ", prayers)}. Which prayer will you like to delete? You can say the number of the prayer or" + continuePrayer;
                                }
                                else
                                {
                                    msg = $" {string.Join(". ", prayers)}. " + continuePrayer;
                                }
                                screenMsg = $"Choose a prayer you want to delete: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                                session.Attributes["lastIntent"] = "RemovePrayerIntent";
                                session.Attributes["currentPage"] = currentPage + 1;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer will you like to delete? You can say the number of the prayer";
                                session.Attributes.Remove("currentPage");
                                screenMsg = $"Choose a prayer you want to delete: " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                            }

                            //screenMsg = $"{title} can't be found. " + "<br/>";
                            //screenMsg += $"{string.Join("<br />", prayers)}.";




                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            if (!isAudio)
                            {
                                resp.Response.Directives.Add(_builder.BuildScrollingSequenceDirective(screenMsg, "Remove Prayer Item").Result);
                            }
                            return Task.FromResult(resp);
                        }
                        else if (prayers.Count == 1 && !string.IsNullOrEmpty(title))
                        {
                            var pry = actual.FirstOrDefault(c => c.Title.ToLower().Contains(title.ToLower()));
                            itm = pry.Sort.ToString();
                            input.Intent.Slots["id"].Value = itm;
                        }
                        else
                        {
                           

                            if (processed < totalCount)
                            {
                                if (currentPage == 1)
                                {
                                    msg = $"Here are your prayers: {string.Join(". ", prayers)}. Which prayer will you like to delete? " + continuePrayer;
                                }
                                else
                                {
                                    msg = $" {string.Join(". ", prayers)}. " + continuePrayer;
                                }
                                screenMsg = $"Choose a prayer you want to delete. " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                                session.Attributes["lastIntent"] = "RemovePrayerIntent";
                                session.Attributes["currentPage"] = currentPage + 1;
                            }
                            else
                            {
                                msg = $"{string.Join(". ", prayers)}. Which prayer would you like to delete? You can say the number of the prayer";
                                session.Attributes.Remove("currentPage");
                                screenMsg = $"Choose a prayer you want to delete. " + "<br/>";
                                screenMsg += $"{string.Join("<br />", prayers)}.";
                            }
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            if (!isAudio)
                            {
                                resp.Response.Directives.Add(_builder.BuildScrollingSequenceDirective(screenMsg, "Remove Prayer Item").Result);
                            }
                            return Task.FromResult(resp);
                        }

                    }
                    else if (!string.IsNullOrEmpty(itm) && itm.Equals("?"))
                    {
                        msg = $"I did not get that, please say the number of the prayer you want to remove.";
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                        return Task.FromResult(resp);
                    }
                    var prayer = actualPrayers.FirstOrDefault(c => c.Sort == int.Parse(itm));
                    if (prayer == null)
                    {
                        msg = $"Item number {itm} can't be found." +
                            $" You could view your prayer list, add a new prayer request" +
                            $" or view more options. What would you like to do?";
                    }
                    else
                    {

                        if (session.Attributes != null)
                        {
                            if (session.Attributes.ContainsKey("LastIntent"))
                            {
                                session.Attributes.Remove("LastIntent");
                            }
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "NONE")
                        {
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech($"You want me to remove {prayer.Title}" +
                                $" from your prayer list correct?");
                            var res = ResponseBuilder.DialogConfirmSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(res);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "DENIED")
                        {
                            input.Intent.Slots["id"].ConfirmationStatus = "NONE";
                            var deniedMsg = "Which prayer would you like to remove? please say the number of the prayer";
                            Reprompt rpt = new Reprompt(deniedMsg);
                            PlainTextOutputSpeech speech = new PlainTextOutputSpeech(deniedMsg);
                            var resp = ResponseBuilder.DialogElicitSlot(speech, "id", session, input.Intent);
                            return Task.FromResult(resp);
                        }
                        if (input.Intent.Slots["id"].ConfirmationStatus == "CONFIRMED")
                        {
                            /// add the service
                            _prayerservice.Delete(prayer.PrayerId);
                            msg = $"Prayer ({prayer.Title}) has been removed successfully. " +
                                $"What would you like to do next?";
                        }


                    }
                }
                else
                {
                    msg = "You have no prayer list. You could add a new prayer request," +
                        " view your prayer list or view more options. What would you like to do?";
                }
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                if (!isAudio)
                {
                    response.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Remove Prayer Item").Result);
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {

                var msg = "I didn't get that. can you please rephrase your request";
                Reprompt rp = new Reprompt(msg);
                var response = ResponseBuilder.Ask(msg, rp, session);
                return Task.FromResult(response);
            }

        }

        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }
        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}
