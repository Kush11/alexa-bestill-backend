﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class ViewReminderIntentHandler : IViewReminderIntentHandler
    {
        IReminderService _reminderservice; 
        IPrayerService _prayerservice;
        IDirectiveBuilder _builder;
        public ViewReminderIntentHandler(IReminderService reminderservice, IPrayerService prayerservice, IDirectiveBuilder builder)
        {
            _reminderservice = reminderservice;
            _prayerservice = prayerservice;
            _builder = builder;
        }
        public Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string msg = "";
            string screenMsg = "";
            string action = "<b>Tips to Managing Reminders</b>:" + "<br />" +
                    "•	Alexa, Set Reminder for #" + "<br />" +
                    "•	Alexa, Remove Reminder for #" + "<br />" +
                    "•	Alexa, List Reminders";
            string remindersListFooter = "Would you like to delete any of the reminder?";
            var actual = ComposeList(uid).Where(c => !string.IsNullOrEmpty(c.PrayerId)).ToList();
            var prayerActual = ComposePrayerList(uid);
            if (actual.Count > 0)
            {
                var reminders = actual.Select(d =>
                {
                    var freq = d.Frequency;
                    var nextoccurence = nextOccurence(d);

                    var prayerObj = prayerActual.FirstOrDefault(c => c.PrayerId.ToString() == d.PrayerId);
                    var sortNumber = prayerObj != null ? prayerObj.Sort.ToString() : "NA";

                    var day = (freq == 7 && DateTime.Now > nextoccurence) ? "Tomorrow"
                    : ((freq < 7 || freq == 7) && DateTime.Now <= nextoccurence && d.StartDate.ToShortDateString() == DateTime.Now.AddDays(1).ToShortDateString()) ? "Tomorrow"
                    : ((freq < 7 && DateTime.Now <= nextoccurence && (DateTime.Now.ToShortDateString() == nextoccurence.ToShortDateString())) || (freq == 7 && DateTime.Now <= nextoccurence && (DateTime.Now.AddDays(1).ToShortDateString() == nextoccurence.ToShortDateString()))) ? "Today"
                    : nextoccurence.ToString("dddd dd MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                    var str = "";
                    if (prayerObj != null)
                    {
                        str = string.Concat("Number ", sortNumber, ". ", d.Title, " at ", d.StartDate.ToShortTimeString(), " ", (day == "Today" || day == "Tomorrow") ? day : "on " + day, ".");
                    }
                    return str;

                }).Where(c => !string.IsNullOrEmpty(c)).ToList();

                string remindersListHeader = $"You have {reminders.Count} Prayers with Reminders: ";
                if(reminders.Count > 0)
                {
                    msg = remindersListHeader + '\n' + $"{string.Join("\n", reminders)}" + '\n' + remindersListFooter;
                    screenMsg = remindersListHeader + "<br/>" + $"{string.Join("<br/>", reminders)}. " + "<br/>" + remindersListFooter;

                }
                else
                {
                    msg = remindersListHeader + '\n' + $"{string.Join("\n", reminders)}";
                    screenMsg = remindersListHeader + "<br/>" + $"{string.Join("<br/>", reminders)}";


                }
                if (session.Attributes == null)
                {
                    session.Attributes = new Dictionary<string, object>();
                    session.Attributes.Add("LastIntent", input.Intent.Name);
                    session.Attributes.Add("lastIntent", input.Intent.Name);
                }
                else
                {
                    session.Attributes["LastIntent"] = input.Intent.Name;
                    session.Attributes["lastIntent"] = input.Intent.Name;
                }
            }
            else
            {
                if (session.Attributes != null)
                    if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }

                msg = "You have no reminder on your list." + '\n' + "What will you like to do, add a new reminder or view your prayer list?";
            }
            Reprompt er = new Reprompt(msg);
            var resp = ResponseBuilder.Ask(msg, er, session);
            if (!isAudio)
            {
                if (actual.Count > 0)
                {
                    //var prayers = prayerActual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                    var reminders = actual.Select(d =>
                    {
                        var freq = d.Frequency;
                        var nextoccurence = nextOccurence(d);

                        var prayerObj = prayerActual.FirstOrDefault(c => c.PrayerId.ToString() == d.PrayerId);
                        var sortNumber = prayerObj != null ? prayerObj.Sort.ToString() : "NA";

                        var day = (freq == 7 && DateTime.Now > nextoccurence) ? "Tomorrow"
                        : ((freq < 7 || freq == 7) && DateTime.Now <= nextoccurence && d.StartDate.ToShortDateString() == DateTime.Now.AddDays(1).ToShortDateString()) ? "Tomorrow"
                        : ((freq < 7 && DateTime.Now <= nextoccurence && (DateTime.Now.ToShortDateString() == nextoccurence.ToShortDateString())) || (freq == 7 && DateTime.Now <= nextoccurence && (DateTime.Now.AddDays(1).ToShortDateString() == nextoccurence.ToShortDateString()))) ? "Today"
                        : nextoccurence.ToString("dddd dd MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                        var str = "";
                        if (prayerObj != null)
                        {
                            str = string.Concat(sortNumber, ". ", d.Title, " at ", d.StartDate.ToShortTimeString(), " ", (day == "Today" || day == "Tomorrow") ? day : "on " + day, ".");
                        }
                        return str;

                    }).Where(c => !string.IsNullOrEmpty(c)).ToList();
                    var dMsg = $"Choose a reminder to be deleted:" + "<br/>";
                        dMsg = $"{ string.Join("<br/>", reminders)}";
                    resp.Response.Directives.Add(_builder.BuildGridListDirective(dMsg, action, "Your Reminder List", "", "60vw").Result);
                }
                else
                {
                    resp = ResponseBuilder.Ask(msg, er, session);
                    resp.Response.Directives.Add(_builder.BuildGridListDirective(screenMsg, action, "Your Reminder List", "", "60vw").Result);
                }
            }
            else
            {
                resp = ResponseBuilder.Ask(msg, er, session);
            }

            return Task.FromResult(resp);
        }

        private List<Reminder_DTO> ComposeList(string uid)
        {

            return _reminderservice.GetReminders(uid).Result;
        }

        private List<Prayer_DTO> ComposePrayerList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }

        private DateTime nextOccurence(Reminder_DTO d)
        {
            DateTime _next = DateTime.Now;
            switch (d.Frequency)
            {
                case 7:
                    _next = DateTime.Now.AddDays(1);
                    break;
                case 14:
                    _next = DateTime.Now.AddDays(7);
                    break;
                case 30:
                    _next = DateTime.Now.AddMonths(1);
                    break;
            }
            var _nextoccur = d.StartDate > DateTime.Now ? d.StartDate.ToShortDateString() : (d.Frequency > 1) ? _next.ToShortDateString() : DateTime.Now.ToShortDateString();
            return DateTime.Parse($"{_nextoccur} {d.StartDate.ToShortTimeString()}");
        }
    }
}
