﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Alexa.Abstraction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class LaunchRequestHandler : ILaunchRequestHandler
    {
        IDirectiveBuilder _builder;
        public LaunchRequestHandler(IDirectiveBuilder builder)
        {
            _builder = builder;
        }
        public Task<SkillResponse> Launch(Session session, bool isAudio)
        {
            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();

            }

            string msg = $"Hi, welcome to Be Still." +
                $" Would you like to hear the actions you can take?";
            session.Attributes["lastIntent"] = "MenuIntent";
            Reprompt rp = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rp, session);
            if (!isAudio)
            {
                response.Response.Directives.Add(_builder.BuildDefaultDirective(msg,"Be Still").Result);
            }
            return Task.FromResult(response);
        }
    }
}
