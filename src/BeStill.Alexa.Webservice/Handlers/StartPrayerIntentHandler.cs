﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using Kevsoft.Ssml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice.Handlers
{
    public class StartPrayerIntentHandler : Helpers.HelperObj, IStartPrayerIntentHandler
    {
        IPrayerService _prayerservice;
        IUserPreferenceService _userPreferenceService;
        IDirectiveBuilder _builder;
        int totalCount = 0;
        public StartPrayerIntentHandler(IPrayerService prayerservice, IDirectiveBuilder builder, IUserPreferenceService userPreferenceService)
        {
            _prayerservice = prayerservice;
            _builder = builder;
            _userPreferenceService = userPreferenceService;
        }
        public async Task<SkillResponse> HandleIntent(string uid, SkillRequest request, bool isAudio = false)
        {
            Session session = request.Session;
            IntentRequest input = (IntentRequest)request.Request;
            string msg = "";
            //string xmlStr = "";
            var sos = new SsmlOutputSpeech();

            var qt = input.Intent.Slots["quietTime"].Value;
            if (!string.IsNullOrEmpty(qt))
            {
                if (qt.ToLower() == "1 minute" || qt.ToLower() == "one minute")
                    qt = "60";
                await _userPreferenceService.InsertOrUpdate(uid, Convert.ToInt32(qt), "silent", uid);
            }


            if (string.IsNullOrEmpty(qt))
            {
                var lastPrayed = await _prayerservice.GetLastPrayedDate(request.Context.System.User.UserId);
                if (lastPrayed <= DateTime.MinValue)
                {
                    msg = $"Before you begin. how much time would you like for me to wait between reading each request. 10, 20, 30, 40 seconds or 1 minute?";
                    Reprompt rp = new Reprompt(msg);
                    PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                    var slotResp = ResponseBuilder.DialogElicitSlot(speech, "quietTime", session, input.Intent);
                    return slotResp;
                }
                else
                {
                    TimeSpan difference = DateTime.UtcNow - lastPrayed;
                    var days = difference.TotalDays;
                    if (days >= 90)
                    {
                        msg = $"Before you begin. how much time would you like for me to wait between reading each request. 10, 20, 30, 40 seconds or 1 minute?";
                        Reprompt rp = new Reprompt(msg);
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech(msg);
                        var slotResp = ResponseBuilder.DialogElicitSlot(speech, "quietTime", session, input.Intent);
                        return slotResp;
                    }
                }
            }

            
            

            var quietTime = await _userPreferenceService.GetUserPreferenceById(uid);

            if (quietTime == null)
            {
                quietTime = new UserPreference_OutputDTO
                {
                    PrayerTime = 10,
                    BackgroundMusic = "instrument"
                };
            }
            var prayerTime = quietTime == null ? 10 : quietTime.PrayerTime;

            var batch = input.Intent.Slots["batch"].Value;
            var isFirst = batch == null ? true : false;
            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();
            }
            if (isFirst)
            {
                if (session.Attributes.ContainsKey("currentPage"))
                {
                    session.Attributes["currentPage"] = 1;
                }
                else
                {
                    session.Attributes.Add("currentPage", 1);
                }
            }
            var currentPage = 1;
            if (session.Attributes.ContainsKey("currentPage")) {
                currentPage = Convert.ToInt32(session.Attributes["currentPage"]);
            }
            var actual = GetPage(currentPage, quietTime.PrayerTime > 40 ? 2 : 3, uid);
            session.Attributes["currentPage"] = currentPage + 1;

            List<string> prayers = new List<string>(); 
            if (actual.Count > 0)
            {
                await _prayerservice.UpdateLastPrayedDate(uid);
                prayers = actual.Select(d => string.Concat(d.Sort, ". ", d.Title)).ToList();
                msg = $"{string.Join("\n", prayers)}";
            }
            else
            {
                if(currentPage == 1)
                {
                    msg = "You have no prayer";
                }
                else
                {
                    if (session.Attributes == null)
                    {
                        session.Attributes = new Dictionary<string, object>();
                        session.Attributes.Add("lastIntent", "StartPrayerIntent");
                    }
                    else
                    {
                        session.Attributes["lastIntent"] = "StartPrayerIntent";
                    }
                    msg = "Amen! You finished your list. Would you like to Add, Archive, or Snooze a Prayer from your list?";
                }
            }
            if (session.Attributes != null)
                if (session.Attributes.ContainsKey("LastIntent")) { session.Attributes.Remove("LastIntent"); }



            var sb = new StringBuilder();
            var defaultBGM = $"{BaseUrl}/musics/qtime10.mp3";
            if (string.IsNullOrEmpty(quietTime?.BackgroundMusic))
                quietTime.BackgroundMusic = "instrument";
            if(quietTime.BackgroundMusic.ToLower() == "melody")
            {
                defaultBGM = $"{BaseUrl}/musics/melody10.mp3";
            }
            else if(quietTime.BackgroundMusic.ToLower() == "silent")
            {
                defaultBGM = $"{BaseUrl}/musics/silent10.mp3";
            }
            var audioUrl = defaultBGM.Replace("10", prayerTime.ToString());
            sb.Append("<speak>");
            sb.Append("Starting prayer time now. ");

            foreach (var item in prayers)
            {
                sb.Append(item.Replace("\n", ""));
                sb.Append($"<audio src ='{audioUrl}' />");
            }
            if (prayers.Count == 3)
            {
                var proccessed = ((currentPage - 1) * 3) + 3;
                if (proccessed < totalCount)
                {
                    sb.Append("You can start the next prayers by saying alexa fetch next prayers");
                } else
                {
                    sb.Append("Amen! You have finished your prayer list. Would you like to Add a prayer, Archive a prayer, or Snooze a Prayer from your list?");
                    msg = "Amen! You have finished your prayer list";
                }
            }
            sb.Append("</speak>");

            if (actual.Count > 0)
            {
                msg = $"{string.Join("\n", prayers)}";
            }

            sos.Ssml = sb.ToString();
            Reprompt erx = new Reprompt
            {
                OutputSpeech = new SsmlOutputSpeech
                {
                    Ssml = sos.Ssml
                }
            };

            Reprompt er = new Reprompt(msg);
            var resp = ResponseBuilder.Empty();
            if (!isAudio)
            {
                if (actual.Count > 0)
                {
                    resp = ResponseBuilder.Ask(sos, new Reprompt(""), session);
                    //resp = ResponseBuilder.Ask("", new Reprompt(""), session);
                    resp.Response.Directives.Add(_builder.BuildAutoPagerDirective(msg, prayerTime, "Prayer time",$"You have {totalCount} prayer request").Result);
                    //resp.Response.Directives.Add(_builder.BuilduietTimeQScrollingSequenceDirective(msg, prayerTime, "Prayer Time").Result);
                }
                else
                {
                    resp = ResponseBuilder.Ask(msg, er, session);
                    resp.Response.Directives.Add(_builder.BuildDefaultDirective(msg, "Your Prayer List").Result);
                }
            }
            else
            {
                resp = ResponseBuilder.Ask(sos, erx, session);
            }
            return resp;
        }

        private List<Prayer_DTO> ComposeList(string uid)
        {

            return _prayerservice.GetPrayers(uid).Result;
        }

        private List<Prayer_DTO> GetPage(int page, int limit, string uid)
        {
            var actual = ComposeList(uid);
            totalCount = actual.Count;
            var dt = actual.Skip((page - 1) * limit).Take(limit).ToList();
            return dt;
        }
    }
}
