using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alexa.NET;
using Alexa.NET.Conversations;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.Directive;
using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using IntentRequest = Alexa.NET.Request.Type.IntentRequest;

namespace BeStill.Alexa.Webservice.Controllers
{
    [Route("api/v1/alexa")]
    public class AlexaController : BaseApiController
    {
        private readonly IAddPrayerIntentHandler _addPrayerIntentHandler;
        private readonly IAddMorePrayerIntentHandler _addMorePrayerIntentHandler;
        private readonly ILaunchRequestHandler _launchRequest;
        private readonly IArchivePrayerIntentHandler _archivePrayerIntentHandler;
        private readonly ISnoozePrayerIntentHandler _snoozePrayerIntentHandler;
        private readonly IUnsnoozePrayerIntentHandler _unsnoozePrayerIntentHandler;
        private readonly IUserDeviceService _userDeviceService;
        private readonly ILogger<AlexaController> _logger;
        private readonly IClearPrayerIntentHandler _clearPrayerIntentHandler;
        private readonly IRemovePrayerIntentHandler _removePrayerIntentHandler;
        private readonly IAnsweredPrayerIntentHandler _answeredPrayerIntentHandler;
        private readonly IPrayerIntentHandler _prayerIntentHandler;
        private readonly IViewPrayerIntentHandler _viewPrayerIntentHandler;
        private readonly IPlayDevotionalIntentHandler _playDevotionalIntentHandler;
        private readonly IStartPrayerIntentHandler _startPrayerIntentHandler;
        private readonly ISetTimerIntentHandler _setTimerIntentHandler;
        private readonly IRemoveReminderIntentHandler _removeReminderIntentHandler;
        private readonly IAddReminderIntentHandler _addReminderIntentHandler;
        private readonly IViewReminderIntentHandler _viewReminderIntentHandler;
        private readonly IYesIntentHandler _yesIntentHandler;
        private readonly INoIntentHandler _noIntentHandler;
        private readonly IMenuIntentHandler _menuIntentHandler;
        private readonly IASMIntentHandler _asmIntentHandler;


        public AlexaController(IMenuIntentHandler menuIntentHandler, IAddPrayerIntentHandler addPrayerIntentHandler, ILaunchRequestHandler launchRequest, IArchivePrayerIntentHandler archivePrayerIntentHandler,
            IClearPrayerIntentHandler clearPrayerIntentHandler, IRemovePrayerIntentHandler removePrayerIntentHandler, IAnsweredPrayerIntentHandler answeredPrayerIntentHandler, ISnoozePrayerIntentHandler snoozePrayerIntentHandler,
            IStartPrayerIntentHandler startPrayerIntentHandler, ISetTimerIntentHandler setTimerIntentHandler, IAddMorePrayerIntentHandler addMorePrayerIntentHandler,
            IPrayerIntentHandler prayerIntentHandler, IViewPrayerIntentHandler viewPrayerIntentHandler, IPlayDevotionalIntentHandler playDevotionalIntentHandler,
            IRemoveReminderIntentHandler removeReminderIntentHandler, IViewReminderIntentHandler viewReminderIntentHandler, IAddReminderIntentHandler addReminderIntentHandler,
            IUserDeviceService userDeviceService, ILogger<AlexaController> logger, IYesIntentHandler yesIntentHandler, INoIntentHandler noIntentHandler, IASMIntentHandler asmIntentHandler, IUnsnoozePrayerIntentHandler unsnoozePrayerIntentHandler)
        {
            _addPrayerIntentHandler = addPrayerIntentHandler;
            _addMorePrayerIntentHandler = addMorePrayerIntentHandler;
            _launchRequest = launchRequest;
            _archivePrayerIntentHandler = archivePrayerIntentHandler;
            _userDeviceService = userDeviceService;
            _logger = logger;
            _clearPrayerIntentHandler = clearPrayerIntentHandler;
            _removePrayerIntentHandler = removePrayerIntentHandler;
            _answeredPrayerIntentHandler = answeredPrayerIntentHandler;
            _prayerIntentHandler = prayerIntentHandler;
            _viewPrayerIntentHandler = viewPrayerIntentHandler;
            _playDevotionalIntentHandler = playDevotionalIntentHandler;
            _startPrayerIntentHandler = startPrayerIntentHandler;
            _setTimerIntentHandler = setTimerIntentHandler;
            _removeReminderIntentHandler = removeReminderIntentHandler;
            _viewReminderIntentHandler = viewReminderIntentHandler;
            _addReminderIntentHandler = addReminderIntentHandler;
            _yesIntentHandler = yesIntentHandler;
            _noIntentHandler = noIntentHandler;
            _menuIntentHandler = menuIntentHandler;
            _asmIntentHandler = asmIntentHandler;
            _snoozePrayerIntentHandler = snoozePrayerIntentHandler;
            _unsnoozePrayerIntentHandler = unsnoozePrayerIntentHandler;

        }



        [HttpPost()]
        [Consumes("application/json")]
        public async Task<SkillResponse> bestil()
        {

            var dd = "";
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                dd = await reader.ReadToEndAsync();
            }

            SkillRequest input = JsonConvert.DeserializeObject<SkillRequest>(dd, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            _logger.LogInformation(JsonConvert.SerializeObject(input));

            Session session = input.Session;
            Context context = input.Context;
            SkillResponse response = ResponseBuilder.Empty();
            HelperObj.BaseUrl = string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, Request.PathBase);
            HelperObj.BgUrl = string.Format("{0}://{1}{2}/images/bestillbackground.png", Request.Scheme, Request.Host, Request.PathBase);
            HelperObj.LogoUrl = string.Format("{0}://{1}{2}/images/bestilllogo.png", Request.Scheme, Request.Host, Request.PathBase);
            var isAudio = !input.Context.System.Device.IsInterfaceSupported("Display");

            try
            {
                var ud = await _userDeviceService.GetUserDeviceById(context.System.User.UserId);

                if (ud == null)
                {
                    await _userDeviceService.Insert(context.System.User.UserId, "", "Alexa", context.System.Device.DeviceID, "Active", "ALEXA");
                }

                if (session.Attributes != null)
                {
                    if (session.Attributes.ContainsKey("LastIntent"))
                    {
                        object lastintent = session.Attributes["LastIntent"];
                        var intentRequest = (IntentRequest)input.Request;
                        if (intentRequest.Intent.Name.ToString().Contains("RemovePrayerIntent"))
                        {
                            switch (lastintent)
                            {
                                case "ArchivePrayerIntent":
                                    _logger.LogInformation("ArchivePrayerIntent Request started");
                                    if (session.Attributes["lastIntent"].ToString() != "ArchivePrayerIntent")
                                    {
                                        session.Attributes.Remove("currentPage");
                                    }
                                    response = _archivePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                                    break;
                                case "AnsweredPrayerIntent":
                                    if (session.Attributes["lastIntent"].ToString() != "AnsweredPrayerIntent")
                                    {
                                        session.Attributes.Remove("currentPage");
                                    }
                                    response = _answeredPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                                    break;
                                case "RemovePrayerIntent":
                                    if (session.Attributes["lastIntent"].ToString() != "RemovePrayerIntent")
                                    {
                                        session.Attributes.Remove("currentPage");

                                    }
                                    response = _removePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                                    break;
                                case "RemoveReminderIntent":
                                    response = _removeReminderIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                                    break;
                                case "ViewReminderIntent":
                                    response = _removeReminderIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                                    break;
                            }
                            return response;
                        }
                        else if(lastintent.ToString() == "SnoozePrayerIntent" && intentRequest.Intent.Name == "SetTimerIntent")
                        {
                            response = _snoozePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                        }
                    }
                }
                if (input.GetRequestType() == typeof(LaunchRequest))
                {
                    _logger.LogInformation("Launch Request started");
                    response = _launchRequest.Launch(session, isAudio).Result;
                }
                else if (input.GetRequestType() == typeof(IntentRequest))
                {
                    var intentRequest = (IntentRequest)input.Request;
                    _logger.LogInformation(intentRequest.Intent.Name);
                    switch (intentRequest.Intent.Name)
                    {
                        case "AMAZON.StopIntent":
                            session.Attributes.Remove("currentPage");
                            response = ResponseBuilder.AudioPlayerStop();
                            break;
                        case "AMAZON.CancelIntent":
                            session.Attributes.Remove("currentPage");
                            response = ResponseBuilder.AudioPlayerStop();
                            break;
                        case "AMAZON.PauseIntent":
                            session.Attributes.Remove("currentPage");
                            response = ResponseBuilder.AudioPlayerStop();
                            break;
                        case "AddPrayerIntent":
                            session.Attributes.Remove("currentPage");
                            _logger.LogInformation("AddPrayerIntent Request started");
                            response = _addPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        //case "AddMorePrayerIntent":
                        //    _logger.LogInformation("AddPrayerIntent Request started");
                        //    response = _addMorePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                        //    break;
                        case "ArchivePrayerIntent":
                            if (session.Attributes["lastIntent"].ToString() != "ArchivePrayerIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            _logger.LogInformation("ArchivePrayerIntent Request started");
                            response = _archivePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;

                        case "SnoozePrayerIntent":
                            session.Attributes.Remove("currentPage");
                            response = _snoozePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "UnsnoozePrayerIntent":
                            if (session.Attributes["lastIntent"].ToString() != "UnsnoozePrayerIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            response = _unsnoozePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "ClearPrayerIntent":
                            session.Attributes.Remove("currentPage");
                            response = _clearPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "AnsweredPrayerIntent":
                            if (session.Attributes["lastIntent"].ToString() != "AnsweredPrayerIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            response = _answeredPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "RemovePrayerIntent":
                            if (session.Attributes["lastIntent"].ToString() != "RemovePrayerIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            response = _removePrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        //case "ListPrayerIntent":
                        //    response = _prayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                        //    break;
                        case "ViewPrayerIntent":
                            if (session.Attributes["lastIntent"].ToString() != "ViewPrayerIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            response = _viewPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "PlayDevotionalIntent":
                            session.Attributes.Remove("currentPage");
                            response = _playDevotionalIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "StartPrayerIntent":
                            response = _startPrayerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "SetTimerIntent":
                            session.Attributes.Remove("currentPage");
                            response = _setTimerIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "RemoveReminderIntent":
                            session.Attributes.Remove("currentPage");
                            response = _removeReminderIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "ViewReminderIntent":
                            session.Attributes.Remove("currentPage");
                            response = _viewReminderIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "AddReminderIntent":
                            if (session.Attributes["lastIntent"].ToString() != "AddReminderIntent")
                            {
                                session.Attributes.Remove("currentPage");

                            }
                            response = _addReminderIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "AMAZON.YesIntent":
                            response = _yesIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "AMAZON.NoIntent":
                            response = _noIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "MenuIntent":
                            session.Attributes.Remove("currentPage");
                            response = _menuIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "ASMIntent":
                            session.Attributes.Remove("currentPage");
                            response = _asmIntentHandler.HandleIntent(ud.UserId, input, isAudio).Result;
                            break;
                        case "AMAZON.FallbackIntent":
                            session.Attributes.Remove("currentPage");
                            var msg = "I didn't get that. can you please rephrase your request";
                            Reprompt rp = new Reprompt(msg);
                            response = ResponseBuilder.Ask(msg, rp, session);
                            break;
                    }
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                var msg = "I didn't get that. can you please rephrase your request";
                Reprompt rp = new Reprompt(msg);
                response = ResponseBuilder.Ask(msg, rp, session);

            }

            return response;
        }

    }
}