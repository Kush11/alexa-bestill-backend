﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Alexa.Webservice.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/devices")]
    public class DeviceController : BaseApiController
    {
        private readonly IDeviceService _deviceService;
        private readonly ILogger<DeviceController> _logger;

        public DeviceController(IDeviceService deviceService, ILogger<DeviceController> logger)
        {
            _deviceService = deviceService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]Device_InputDTO request)
        {
            try
            {
                await _deviceService.Insert(request.DeviceId, request.Model, request.Name, request.Status, request.CreatedBy);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
    }
}