﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeStill.Alexa.Webservice.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/userdevices")]
    public class UserDeviceController : BaseApiController
    {

        private readonly IUserDeviceService _userDeviceService;
        private readonly ILogger<UserDeviceController> _logger;

        public UserDeviceController(IUserDeviceService userDeviceService, ILogger<UserDeviceController> logger)
        {
            _userDeviceService = userDeviceService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]UserDevice_InputDTO request)
        {
            try
            {
                await _userDeviceService.Insert(request.UserId, request.DeviceId, request.DeviceModel, request.DeviceName, request.Status, request.CreatedBy);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }

        [HttpGet("userdevice")]
        public async Task<IActionResult> GetUserDeviceByIdAsync(string userId)
        {
            try
            {
                var retVal = await _userDeviceService.GetUserDeviceById(userId);

                return Ok(retVal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CreateApiException(ex);
            }
        }
    }
}