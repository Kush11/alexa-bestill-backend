﻿using BeStill.Abstraction.Service;
using BeStill.Alexa.Abstraction.Interface;
using BeStill.Alexa.Webservice.Handlers;
using BeStill.Business.Service;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.FireStore;
using BeStill.Data.FireStore.Services;
using BeStill.Data.Service.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeStill.Alexa.Webservice
{
    public class ServiceRegistration
    {
        public static void Setup(IServiceCollection services, bool useFirebaseForDatabase, bool useMockForIntegrations)
        {
            //data layer services
            if (useFirebaseForDatabase)
            {
                services.AddScoped<DbContext>();
                services.AddScoped<UserFireStoreService>();
                services.AddScoped<UserPrayerFireStoreService>();
                services.AddScoped<DeviceFireStoreService>();

                services.AddTransient<IDeviceDataService, DeviceFireStoreService>();
                services.AddTransient<IUserDeviceDataService, UserDeviceFireStoreService>();
                services.AddTransient<IIntentHistoryLogDataService, IntentHistoryLogFireStoreService>();
                services.AddTransient<IPrayerDataService, PrayerFireStoreService>();
                services.AddTransient<IReminderDataService, ReminderFireStoreService>();
                services.AddTransient<IIntentHistoryLogTempDataService, IntentHistoryLogTempFireStoreService>();
                services.AddTransient<IUserPreferenceDataService, UserPreferenceFireStoreService>();
            }
            else
            {
                services.AddTransient<IDeviceDataService, DeviceDataService>();
                services.AddTransient<IUserDeviceDataService, UserDeviceDataService>();
                services.AddTransient<IIntentHistoryLogDataService, IntentHistoryLogDataService>();
                services.AddTransient<IPrayerDataService, PrayerDataService>();
                services.AddTransient<IReminderDataService, ReminderDataService>();
                services.AddTransient<IIntentHistoryLogTempDataService, IntentHistoryLogTempDataService>();
                services.AddTransient<IUserPreferenceDataService, UserPreferenceDataService>();
            }

            services.AddTransient<IDeviceService, DeviceService>();
            services.AddTransient<IUserDeviceService, UserDeviceService>();
            services.AddTransient<IPrayerService, PrayerService>();
            services.AddTransient<IReminderService, ReminderService>();
            services.AddTransient<IIntentHistoryLogService, IntentHistoryLogService>();
            services.AddTransient<IIntentHistoryLogTempService, IntentHistoryLogTempService>();
            services.AddTransient<IUserPreferenceService, UserPreferenceService>();


            services.AddTransient<IAddPrayerIntentHandler, AddPrayerIntentHandler>();
            services.AddTransient<IAddMorePrayerIntentHandler, AddMorePrayerIntentHandler>();
            services.AddTransient<ILaunchRequestHandler, LaunchRequestHandler>();
            services.AddTransient<IArchivePrayerIntentHandler, ArchivePrayerIntentHandler>();
            services.AddTransient<ISnoozePrayerIntentHandler, SnoozePrayerIntentHandler>();
            services.AddTransient<IUnsnoozePrayerIntentHandler, UnsnoozePrayerIntentHandler>();
            services.AddTransient<IClearPrayerIntentHandler, ClearPrayerIntentHandler>();
            services.AddTransient<IPrayerIntentHandler, PrayerIntentHandler>();      
            services.AddTransient<IViewPrayerIntentHandler, ViewPrayerIntentHandler>();      
            services.AddTransient<IClearPrayerIntentHandler, ClearPrayerIntentHandler>();
            services.AddTransient<IAnsweredPrayerIntentHandler, AnsweredPrayerIntentHandler>();
            services.AddTransient<IRemovePrayerIntentHandler, RemovePrayerHandler>();
            services.AddTransient<IDirectiveBuilder, DirectiveBuilder>();
            services.AddTransient<IPlayDevotionalIntentHandler, PlayDevotionalIntentHandler>();
            services.AddTransient<IStartPrayerIntentHandler, StartPrayerIntentHandler>();
            services.AddTransient<ISetTimerIntentHandler, SetTimerIntentHandler>();
            services.AddTransient<IAddReminderIntentHandler, AddReminderIntentHandler>();
            services.AddTransient<IViewReminderIntentHandler, ViewReminderIntentHandler>();
            services.AddTransient<IRemoveReminderIntentHandler, RemoveReminderIntentHandler>();
            services.AddTransient<IMenuIntentHandler, MenuIntentHandler>();
            services.AddTransient<IYesIntentHandler, YesIntentHandler>();
            services.AddTransient<INoIntentHandler, NoIntentHandler>();
            services.AddTransient<IASMIntentHandler, ASMIntentHandler>();



        }
    }
}
