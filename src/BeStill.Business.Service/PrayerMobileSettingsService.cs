﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class PrayerMobileSettingsService : IPrayerMobileSettingsService
    {
        private readonly IPrayerMobileSettingsDataService _prayerMobileSettingDataService;

        public PrayerMobileSettingsService(IPrayerMobileSettingsDataService prayerMobileSettingService)
        {
            _prayerMobileSettingDataService = prayerMobileSettingService;
        }

        public async Task<PrayerMobileSettings> GetPrayerSetting(int UserId)
        {
            var ps = await _prayerMobileSettingDataService.GetPrayerSetting(UserId);
            return ps;
        }

        public Task Insert(int Frequency, DateTime Date, DateTime Time, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            return _prayerMobileSettingDataService.Insert(Frequency, Date, Time, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }
    }
}
