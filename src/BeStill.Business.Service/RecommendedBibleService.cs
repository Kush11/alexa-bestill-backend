﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class RecommendedBibleService : IRecommendedBibleService
    {
        private readonly IRecommendedBibleDataService _recommendedBibleDataService;

        public RecommendedBibleService(IRecommendedBibleDataService recommendedBibleDataService)
        {
            _recommendedBibleDataService = recommendedBibleDataService;
        }
        public async Task<List<RecommendedBible>> GetAllBibles()
        {
            var rb = await _recommendedBibleDataService.GetAllBibles();
            return rb;
        }

        public Task Insert(string Title, string Abbreviation, string SubTitle,
            string Description, string Link, string Status, string CreatedBy,
            DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            return _recommendedBibleDataService.Insert(Title, Abbreviation, SubTitle, Description,
                Link, Status, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }
    }
}
