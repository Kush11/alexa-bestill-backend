﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class ReminderService : IReminderService
    {
        private readonly IReminderDataService _reminderDataService;
        private readonly IMapper _mapper;

        public ReminderService(IReminderDataService reminderDataService, IMapper mapper)
        {
            _reminderDataService = reminderDataService;
            _mapper = mapper;
        }

        public Task Delete(Guid ReminderId)
        {
            return _reminderDataService.Delete(ReminderId);
        }

        public async Task<List<Reminder_DTO>> GetReminders(string UserId)
        {
            var rem = await _reminderDataService.GetReminders(UserId);
            return _mapper.Map<List<Reminder_DTO>>(rem);
        }

        public Task Insert(string UserId, int Frequency, string Token, DateTime StartDate, DateTime EndDate, string Title, string CreatedBy, string prayerId)
        {
            return _reminderDataService.Insert(UserId, Frequency, Token, StartDate, EndDate, Title, CreatedBy, prayerId);
        }
    }
}
