﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class PrayerService : IPrayerService
    {
        private readonly IPrayerDataService _prayerDataService;
        private readonly IMapper _mapper;

        public PrayerService(IPrayerDataService prayerDataService, IMapper mapper)
        {
            _prayerDataService = prayerDataService;
            _mapper = mapper;
        }

        public Task Delete(Guid prayerId)
        {
            return _prayerDataService.Delete(prayerId);
        }

        public Task DeleteAll(string UserId)
        {
            return _prayerDataService.DeleteAll(UserId);
        }

        public async Task<Prayer_DTO> GetPrayer(Guid PrayerId)
        {
            var pr = await _prayerDataService.GetPrayer(PrayerId);
            return _mapper.Map<Prayer_DTO>(pr);
        }

        public async Task<List<Prayer_DTO>> GetPrayers(string UserId)
        {
            var pr = await _prayerDataService.GetPrayers(UserId);
            return _mapper.Map<List<Prayer_DTO>>(pr);
        }

        public Task Insert(string UserId, string Type, string Title, string CreatedBy)
        {
            return _prayerDataService.Insert(UserId, Type, Title, CreatedBy);
        }

        public async Task PrayerAnswer(Guid PrayerId)
        {
            await _prayerDataService.MarkAsAnswer(PrayerId);
        }

        public async Task ArchivePrayer(Guid PrayerId)
        {
            await _prayerDataService.ArchivePrayer(PrayerId);
        }
        public async Task UnsnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            await _prayerDataService.UnsnoozePrayer(PrayerId, SnoozeExpiryDate);
        }
        public async Task SnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            await _prayerDataService.SnoozePrayer(PrayerId, SnoozeExpiryDate);
        }


        public async Task<DateTime> GetLastPrayedDate(string UserId)
        {
            return await _prayerDataService.GetLastPrayedDate(UserId);
        }

        public async Task UpdateLastPrayedDate(string UserId)
        {
            await _prayerDataService.UpdateLastPrayedDate(UserId);
        }
    }
}
