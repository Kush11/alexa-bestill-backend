﻿using AutoMapper;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class MobileSettingService : IMobileSettingService
    {
        private readonly IMobileSettingDataService _mobileSettingDataService;
        private readonly IMapper _mapper;

        public MobileSettingService(IMobileSettingDataService mobileSettingService, IMapper mapper)
        {
            _mobileSettingDataService = mobileSettingService;
            _mapper = mapper;
        }

        public Task Insert(string Appearance, string DefaultSortBy, int DefaultSnoozeDuration, string ArchiveAutoDelete,
            string IncludeAnsweredPrayerAutoDelete, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {

            return _mobileSettingDataService.Insert(Appearance, DefaultSortBy, DefaultSnoozeDuration, ArchiveAutoDelete,
             IncludeAnsweredPrayerAutoDelete, Status, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }

        public async Task<MobileSettings> GetMobileSettings(int UserId)
        {
            var ms = await _mobileSettingDataService.GetSettings(UserId);
            return ms;
        }


    }
}
