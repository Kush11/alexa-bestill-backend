﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class GroupService : IGroupService
    {
        private readonly IMapper _mapper;
        private readonly IGroupDataService _groupDataService;

        public GroupService(IGroupDataService groupDataService, IMapper mapper)
        {
            _groupDataService = groupDataService;
            _mapper = mapper;
        }

        public async Task AddMemberToGroup(string UserId, bool IsAdmin, bool IsModerator, int GroupId, string CreatedBy)
        {
            await _groupDataService.AddMemberToGroup(UserId, IsAdmin, IsModerator, GroupId, CreatedBy);
        }

        public async Task ChangeMemberType(int GroupId, string UserId)
        {
            var gu = await _groupDataService.IsAdmin(UserId);
            if (gu.IsAdmin)
            {
                await _groupDataService.ChangeMemberType(GroupId, UserId);
            }
        }

        public async Task Delete(int GroupId)
        {
            await _groupDataService.Delete(GroupId);
        }

        public async Task<List<Group_DTO>> GetAllGroups(string UserId)
        {
            var g = await _groupDataService.GetAllGroups(UserId);
            return _mapper.Map<List<Group_DTO>>(g);
        }

        public async Task<Group_DTO> GetGroup(int GroupId)
        {
            var g = await _groupDataService.GetGroup(GroupId);
            return _mapper.Map<Group_DTO>(g);
        }

        public async Task<GroupInvite_DTO> GetInviteStatus(string UserId, int GroupId)
        {
            var g = await _groupDataService.GetInviteStatus(UserId, GroupId);
            return _mapper.Map<GroupInvite_DTO>(g);
        }

        public async Task Insert(string Name, string Description, string Organization, string Location, string CreatedBy)
        {
            await _groupDataService.Insert(Name, Description, Organization, Location, CreatedBy);
        }

        public async Task InsertInviteStatus(string UserId, int GroupId, string Status, string CreatedBy)
        {
            await _groupDataService.InsertInviteStatus(UserId, GroupId, Status, CreatedBy);
        }

        public async Task RemoveMemberFromGroup(int GroupId, string UserId)
        {
            await _groupDataService.RemoveMemberFromGroup(GroupId, UserId);
        }

        public async Task Update(int GroupId, string Name, string Description, string Organization, string Location, string MoodifiedBy)
        {
            await _groupDataService.Update(GroupId, Name, Description, Organization, Location, MoodifiedBy);
        }

        public async Task UpdateInviteStatus(string UserId, int GroupId, string Status)
        {
            await _groupDataService.UpdateInviteStatus(UserId, GroupId, Status);
        }
    }
}
