﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class UserDeviceService: IUserDeviceService
    {
        private readonly IUserDeviceDataService _userDeviceDataService;
        private readonly IMapper _mapper;

        public UserDeviceService(IUserDeviceDataService userDeviceDataService, IMapper mapper)
        {
            _userDeviceDataService = userDeviceDataService;
            _mapper = mapper;
        }

        public async Task Insert(string UserId, string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
            await _userDeviceDataService.Insert(UserId, DeviceId, Model, Name, Status, CreatedBy);
        }

        public async Task<UserDevice_OutputDTO> GetUserDeviceById(string UserId)
        {
            var ud = await _userDeviceDataService.GetUserDeviceById(UserId);
            var retVal = _mapper.Map<UserDevice_OutputDTO>(ud);
            return retVal;
        }
    }
}
