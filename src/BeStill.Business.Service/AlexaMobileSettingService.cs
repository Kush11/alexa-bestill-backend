﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class AlexaMobileSettingService : IAlexaMobileSettingService
    {
        private readonly IAlexaMobileSettingDataService _alexaMobileSettingService;

        public AlexaMobileSettingService(IAlexaMobileSettingDataService alexaMobileSettingService)
        {
            _alexaMobileSettingService = alexaMobileSettingService;
        }

        public async Task<MobileSettingsAlexa> GetAlexaSetting(int UserId)
        {
            var ams = await _alexaMobileSettingService.GetAlexaSettings(UserId);
            return ams;
        }

        public Task Insert(string AllowPrayerTimeNotification, string SyncAlexa)
        {
            return _alexaMobileSettingService.Insert(AllowPrayerTimeNotification, SyncAlexa);
        }
    }
}
