﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class DeviceService : IDeviceService
    {
        private readonly IDeviceDataService _deviceDataService;

        public DeviceService(IDeviceDataService deviceDataService)
        {
            _deviceDataService = deviceDataService;
        }
        public async Task Insert(string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
            await _deviceDataService.Insert(DeviceId, Model, Name, Status, CreatedBy);
        }
    }
}
