﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class UserPreferenceService : IUserPreferenceService
    {
        private readonly IUserPreferenceDataService _userPreferenceDataService;
        private readonly IMapper _mapper;

        public UserPreferenceService(IUserPreferenceDataService userPreferenceDataService, IMapper mapper)
        {
            _userPreferenceDataService = userPreferenceDataService;
            _mapper = mapper;
        }
        public async Task<UserPreference_OutputDTO> GetUserPreferenceById(string UserId)
        {
            var ud = await _userPreferenceDataService.GetUserPreferenceById(UserId);
            var retVal = _mapper.Map<UserPreference_OutputDTO>(ud);
            return retVal;
        }

        public async Task InsertOrUpdate(string UserId, int PrayerTime, string BackgroundMusic, string CreatedBy)
        {
            await _userPreferenceDataService.InsertOrUpdate(UserId, PrayerTime, BackgroundMusic, CreatedBy);
        }
    }
}
