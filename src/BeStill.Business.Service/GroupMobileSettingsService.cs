﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class GroupMobileSettingsService : IGroupMobileSettingService
    {
        private readonly IGroupMobileSettingsDataService _groupMobileSettingDataService;

        public GroupMobileSettingsService(IGroupMobileSettingsDataService groupMobileSettingDataService)
        {
            _groupMobileSettingDataService = groupMobileSettingDataService;
        }

        public async Task<GroupMobileSettings> GetSettingByUserId(int UserId)
        {
            var gs = await _groupMobileSettingDataService.GetSettingByUserId(UserId);
            return gs;
        }

        public Task Insert(int DeviceId, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            return _groupMobileSettingDataService.Insert(DeviceId, Status, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }
    }
}
