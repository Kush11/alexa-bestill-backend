﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class IntentHistoryLogService : IIntentHistoryLogService
    {
        private readonly IIntentHistoryLogDataService _intentHistoryLogDataService;

        public IntentHistoryLogService(IIntentHistoryLogDataService intentHistoryLogDataService)
        {
            _intentHistoryLogDataService = intentHistoryLogDataService;
        }

        public async Task InsertFromTemp()
        {
            await _intentHistoryLogDataService.InsertFromTemp();
        }
    }
}
