﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class MobileSettingPushNotificationService : IMobileSettingPushNotificationService
    {
        private readonly IMobileSettingPushNotificationDataService _mobileSettingPushNotification;

        public MobileSettingPushNotificationService(IMobileSettingPushNotificationDataService mobileSettingPushNotification)
        {
            _mobileSettingPushNotification = mobileSettingPushNotification;
        }

        public async Task<MobileSettingPushNotification> GetPushSettings(int UserId)
        {
            var ps = await _mobileSettingPushNotification.GetPushSettings(UserId);
            return ps;
        }

        public Task Insert(string AllowPushNotification, string AllowTextNotification, string EmailUpdateNotification,
            int EmailUpdateFrequency, string NotifyMeSomeOneSharePrayerWithMe, string NotifyMeSomeOnePostOnGroup,
            string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            return _mobileSettingPushNotification.Insert(AllowPushNotification, AllowTextNotification, EmailUpdateNotification,
                EmailUpdateFrequency, NotifyMeSomeOnePostOnGroup, NotifyMeSomeOneSharePrayerWithMe, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }
    }
}
