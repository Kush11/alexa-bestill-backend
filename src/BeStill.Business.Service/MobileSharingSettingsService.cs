﻿using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class MobileSharingSettingsService : IMobileSharingSettingsService
    {
        private readonly IMobileSharingSettingsDataService _mobileSharingSettingDataService;

        public MobileSharingSettingsService(IMobileSharingSettingsDataService mobilesharingSettingService)
        {
            _mobileSharingSettingDataService = mobilesharingSettingService;
        }

        public async Task<MobileSettingsSharing> GetMobileSharingSetting(int UserId)
        {
            var ms = await _mobileSharingSettingDataService.GetMobileSharingSetting(UserId);
            return ms;
        }

        public Task Insert(string EnableSharingViaText, string EnableSharingViaEmail, int ChurchId,
            string Email, string Phone, string Status, string CreatedBy, DateTime CreatedOn,
            string ModifiedBy, DateTime ModifiedOn)
        {
            return _mobileSharingSettingDataService.Insert(EnableSharingViaText, EnableSharingViaEmail, ChurchId,
                Email, Phone, Status, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn);
        }
    }
}
