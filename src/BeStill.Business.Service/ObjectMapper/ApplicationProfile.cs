﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Business.Service.ObjectMapper
{
    public class ApplicationProfile: Profile
    {
        public ApplicationProfile()
        {
            CreateMap<UserDevice, UserDevice_OutputDTO>().ReverseMap();
            CreateMap<Prayer, Prayer_DTO>().ReverseMap();
            CreateMap<IntentHistoryLogTemp, IntentHistoryLogTemp_InputDTO>().ReverseMap();
            CreateMap<UserPreference, UserPreference_OutputDTO>().ReverseMap();
            CreateMap<Reminder, Reminder_DTO>().ReverseMap();
            CreateMap<Group, Group_DTO>().ReverseMap();
        }
    }
}
