﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class GroupPrayerService : IGroupPrayerService
    {
        private readonly IMapper _mapper;
        private readonly IGroupPrayerDataService _groupPrayerDataService;

        public GroupPrayerService(IGroupPrayerDataService groupPrayerDataService, IMapper mapper)
        {
            _mapper = mapper;
            _groupPrayerDataService = groupPrayerDataService;
        }

        public async Task AddPrayerToGroup(int prayerId, int groupId, string createdBy)
        {
            await _groupPrayerDataService.AddPrayerToGroup(prayerId, groupId, createdBy);
        }

        public async Task ChangeGroupPrayerStatus(int PrayerId, int GroupId)
        {
            await _groupPrayerDataService.ChangeGroupPrayerStatus(PrayerId, GroupId);
        }

        public async Task FlagPrayer(int prayerId)
        {
            await _groupPrayerDataService.FlagPrayer(prayerId);
        }

        public async Task<List<Prayer_DTO>> GetGroupPrayers(int GroupId, string CreatedBy)
        {
            var g = await _groupPrayerDataService.GetGroupPrayers(GroupId, CreatedBy);
            return _mapper.Map<List<Prayer_DTO>>(g);
        }

        public Task HidePrayer(string UserId, int id, string CreatedBy)
        {
            return _groupPrayerDataService.HidePrayer(UserId, id, CreatedBy);
        }
    }
}
