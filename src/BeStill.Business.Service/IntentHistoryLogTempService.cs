﻿using AutoMapper;
using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Business.Service
{
    public class IntentHistoryLogTempService : IIntentHistoryLogTempService
    {
        private readonly IIntentHistoryLogTempDataService _intentHistoryLogTempDataService;
        private readonly IMapper _mapper;

        public IntentHistoryLogTempService(IIntentHistoryLogTempDataService intentHistoryLogTempDataService, IMapper mapper)
        {
            _intentHistoryLogTempDataService = intentHistoryLogTempDataService;
            _mapper = mapper;
        }

        public async Task BulkInsert(List<IntentHistoryLogTemp_InputDTO> intentHistories)
        {
            var bulk = _mapper.Map<List<IntentHistoryLogTemp>>(intentHistories);
            await _intentHistoryLogTempDataService.BulkInsert(bulk);
        }
    }
}
