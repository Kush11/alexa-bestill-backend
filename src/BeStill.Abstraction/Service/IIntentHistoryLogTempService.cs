﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IIntentHistoryLogTempService
    {
        Task BulkInsert(List<IntentHistoryLogTemp_InputDTO> intentHistories);
    }
}
