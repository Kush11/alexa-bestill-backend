﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IDeviceService
    {
        Task Insert(string DeviceId, string Model, string Name, string Status, string CreatedBy);
    }
}
