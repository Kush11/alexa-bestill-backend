﻿using BeStill.Abstraction.DTO;
using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IGroupService
    {
        Task Insert(string Name, string Description, string Organization, string Location, string CreatedBy);
        Task Delete(int GroupId);
        Task Update(int GroupId, string Name, string Description, string Organization, string Location, string ModifiedBy);
        Task<List<Group_DTO>> GetAllGroups(string UserId);
        Task<Group_DTO> GetGroup(int GroupId);
        Task ChangeMemberType(int GroupId, string UserId);
        Task AddMemberToGroup(string UserId, bool IsAdmin, bool IsModerator, int GroupId, string CreatedBy);
        Task RemoveMemberFromGroup(int GroupId, string UserId);

        Task<GroupInvite_DTO> GetInviteStatus(string UserId, int GroupId);
        Task UpdateInviteStatus(string UserId, int GroupId, string Status);
        Task InsertInviteStatus(string UserId, int GroupId, string Status, string CreatedBy);
    }
}
