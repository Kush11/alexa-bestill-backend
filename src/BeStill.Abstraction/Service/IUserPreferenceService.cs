﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IUserPreferenceService
    {
        Task InsertOrUpdate(string UserId, int PrayerTime, string BackgroundMusic, string CreatedBy);
        Task<UserPreference_OutputDTO> GetUserPreferenceById(string UserId);
    }
}
