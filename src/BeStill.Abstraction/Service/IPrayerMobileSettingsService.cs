﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IPrayerMobileSettingsService
    {
        Task Insert(int Frequency, DateTime Date, DateTime Time, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn);

        Task<PrayerMobileSettings> GetPrayerSetting(int UserId);
    }
}

