﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IMobileSettingPushNotificationService
    {
        Task Insert(string AllowPushNotification, string AllowTextNotification, string EmailUpdateNotification,
            int EmailUpdateFrequency, string NotifyMeSomeOneSharePrayerWithMe, string NotifyMeSomeOnePostOnGroup,
            string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn);

        Task<MobileSettingPushNotification> GetPushSettings(int UserId);

    }
}
