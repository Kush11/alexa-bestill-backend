﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IGroupPrayerService
    {
        
        Task<List<Prayer_DTO>> GetGroupPrayers(int groupId, string createdBy);
        Task ChangeGroupPrayerStatus(int prayerId, int groupId);
        Task HidePrayer(string userId, int prayerId, string createdBy);
        Task FlagPrayer(int prayerId);
        Task AddPrayerToGroup(int prayerId, int groupId, string createdBy);
    }
}
