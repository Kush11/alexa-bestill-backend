﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IUserDeviceService
    {
        Task Insert(string UserId, string DeviceId, string Model, string Name, string Status, string CreatedBy);
        Task<UserDevice_OutputDTO> GetUserDeviceById(string UserId);
    }
}
