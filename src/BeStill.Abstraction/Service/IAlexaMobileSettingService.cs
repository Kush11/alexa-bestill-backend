﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IAlexaMobileSettingService
    {
        Task Insert(string AllowPrayerTimeNotification, string SyncAlexa);

        Task<MobileSettingsAlexa> GetAlexaSetting(int UserId);
    }
}
