﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IPrayerService
    {
        Task Insert(string UserId, string Type, string Title, string CreatedBy);
        Task<List<Prayer_DTO>> GetPrayers(string UserId);
        Task<Prayer_DTO> GetPrayer(Guid PrayerId);
        Task PrayerAnswer(Guid PrayerId);
        Task ArchivePrayer(Guid PrayerId);
        Task UnsnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate);
        Task SnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate);
        Task DeleteAll(string UserId);
        Task Delete(Guid prayerId);
        Task<DateTime> GetLastPrayedDate(string UserId);
        Task UpdateLastPrayedDate(string UserId);
    }
}
