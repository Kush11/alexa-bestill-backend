﻿using BeStill.Abstraction.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Abstraction.Service
{
    public interface IReminderService
    {
        Task Insert(string UserId, int Frequency, string Token, DateTime StartDate, DateTime EndDate, string Title, string CreatedBy, string prayerId);
        Task<List<Reminder_DTO>> GetReminders(string UserId);
        Task Delete(Guid ReminderId);
      
    }
}



