﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeStill.Abstraction.Model
{
    public class GroupPrayerModel
    {
        public int GroupId { get; set; }
        public int PrayerId { get; set; }
        public string Status { get; set; }
    }
}
