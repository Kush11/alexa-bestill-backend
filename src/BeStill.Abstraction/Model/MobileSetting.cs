﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.Model
{
    public class MobileSetting
    {
        public int UserId { get; set; }
        public string Appearance { get; set; }
        public string DefaultSortBy { get; set; }
        public int DefaultSnoozeDuration { get; set; }
        public string ArchiveAutoDelete { get; set; }
        public string IncludeAnsweredPrayerAutoDelete { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
