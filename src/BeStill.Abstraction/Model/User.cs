﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.Model
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
    }
}
