﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.Settings
{
    public class ClientAppSettings
    {
        public string Url { get; set; }
        public string EmailConfirmationPath { get; set; }
        public string ResetPasswordPath { get; set; }
    }
}
