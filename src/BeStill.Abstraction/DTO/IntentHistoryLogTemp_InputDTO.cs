﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class IntentHistoryLogTemp_InputDTO
    {
		public string NextToken { get; set; }
		public string SkillId { get; set; }
		public string DialogAct { get; set; }
		public string IntentName { get; set; }
		public string IntentConfidence { get; set; }
		public string SlotName { get; set; }
		public string InteractionType { get; set; }
		public string PublicationStatus { get; set; }
		public string UtteranceText { get; set; }
		public string Locale { get; set; }
		public string Stage { get; set; }
		public DateTime Date { get; set; }
	}
}
