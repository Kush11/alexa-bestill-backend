﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class UserDevice_InputDTO
    {
        public string UserId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceName { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
    }
}
