﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class GroupInvite_DTO
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
        public string Status { get; set; }

    }
}
