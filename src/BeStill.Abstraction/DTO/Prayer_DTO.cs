﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class Prayer_DTO
    {
        public Guid PrayerId { get; set; }
        public string UserId { get; set; }
        public int Sequence { get; set; }
        public bool IsFavourite { get; set; }
        public bool HasReminder { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string IsAnswer { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public int Sort { get; set; }
        public DateTime SnoozeExpiryDate { get; set; }
    }
}
