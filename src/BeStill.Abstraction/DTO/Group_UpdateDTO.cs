﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class Group_UpdateDTO
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Organization { get; set; }
        public string Location { get; set; }
        public string ModifiedBy { get; set; }

    }
}
