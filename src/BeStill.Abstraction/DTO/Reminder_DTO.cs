﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class Reminder_DTO
    {
        public Guid ReminderId { get; set; }
        public string PrayerId { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public int Frequency { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public int Sort { get; set; }
        public string CreatedBy { get; set; }
    }
}
