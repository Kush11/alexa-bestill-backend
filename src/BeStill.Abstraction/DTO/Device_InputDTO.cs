﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Abstraction.DTO
{
    public class Device_InputDTO
    {
        public string DeviceId { get; set; }

        public string Model { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
    }
}
