﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class MobileSettingDataService : BaseObj, IMobileSettingDataService
    {
        private readonly string _connString;

        public MobileSettingDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");
        }

        public async Task Insert(string Appearance, string DefaultSortBy, int DefaultSnoozeDuration, string ArchiveAutoDelete,
            string IncludeAnsweredPrayerAutoDelete, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "MobileSetting_Insert";
                using(SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Appearance", Appearance);
                    command.Parameters.AddWithValue("@DefaultSortBy", DefaultSortBy);
                    command.Parameters.AddWithValue("@DefaultSnoozeDuration", DefaultSnoozeDuration);
                    command.Parameters.AddWithValue("@ArchiveAutoDelete", ArchiveAutoDelete);
                    command.Parameters.AddWithValue("@IncludeAnsweredPrayerAutoDelete", IncludeAnsweredPrayerAutoDelete);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            } 
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<MobileSettings> GetSettings(int UserId)
        {
            try
            {
                string commandText = "MobileSetting_GetByUserId";

                MobileSettings obj = null;

                using(SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using(SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if(reader.Read())
                        {
                            obj = new MobileSettings();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.Appearance = reader["Appearance"].ToString();
                            obj.ArchiveAutoDelete = reader["ArchiveAutoDelete"].ToString();
                            obj.DefaultSnoozeDuration = int.Parse(reader["DefaultSnoozeDuration"].ToString());
                            obj.DefaultSortBy = reader["DefaultSortBy"].ToString();
                            obj.IncludeAnsweredPrayerAutoDelete = reader["IncludeAnsweredPrayerAutoDelete"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();
                        }
                    }

                    return obj;
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
