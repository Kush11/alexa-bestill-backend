﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class UserPreferenceDataService : BaseObj, IUserPreferenceDataService
    {
        public async Task<UserPreference> GetUserPreferenceById(string UserId)
        {
			try
			{
				string CommandText = "UserPreference_GetByUserId";

				UserPreference obj = null;

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@UserId", UserId);

					connection.Open();

					using (SqlDataReader reader = await command.ExecuteReaderAsync())
					{
						while (reader.Read())
						{
							obj = new UserPreference();
							obj.UserId = reader["UserId"].ToString();
							obj.PrayerTime = Convert.ToInt32(reader["PrayerTime"]);
							obj.BackgroundMusic = reader["BackgroundMusic"].ToString();
							obj.CreatedBy = reader["CreatedBy"].ToString();
							obj.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
							obj.ModifiedBy = reader["ModifiedBy"].ToString();
							obj.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
						}
					}
				}
				return obj;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public async Task InsertOrUpdate(string UserId, int PrayerTime, string BackgroundMusic, string CreatedBy)
        {
			try
			{
				string CommandText = "UserPreference_Insert";
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@UserId", UserId);
					command.Parameters.AddWithValue("@PrayerTime", PrayerTime);
					command.Parameters.AddWithValue("@BackgroundMusic", BackgroundMusic);
					command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
					connection.Open();
					await command.ExecuteNonQueryAsync();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}
