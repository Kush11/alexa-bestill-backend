﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class AlexaMobileSettingDataService : BaseObj, IAlexaMobileSettingDataService
    {

        private readonly string _connString;

        public AlexaMobileSettingDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");

        }
        public async Task<MobileSettingsAlexa> GetAlexaSettings(int UserId)
        {
            try
            {
                string commandText = "AlexaMobileSettings_GetByUserId";

                MobileSettingsAlexa obj = null;

                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new MobileSettingsAlexa();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.AllowPrayerTimeNotification = reader["AllowPrayerTimeNotification"].ToString();
                            obj.SyncAlexa = reader["SyncAlexa"].ToString();

                        }
                    }

                    return obj;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(string AllowPrayerTimeNotification, string SyncAlexa)
        {
            try
            {
                string commandText = "AlexaSettings_Insert";
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AllowPrayerTimeNotification", AllowPrayerTimeNotification);
                    command.Parameters.AddWithValue("@SyncAlexa", SyncAlexa);
                 
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
