﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class RecommendedBibleDataService : BaseObj, IRecommendedBibleDataService
    {

        private readonly string _connString;

        public RecommendedBibleDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");
        }


        public async Task<List<RecommendedBible>> GetAllBibles()
        {
            try
            {
                string commandText = "RecommendedBible_GetAll";


                List<RecommendedBible> result = null;

                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        result = new List<RecommendedBible>();
                        while (reader.Read())
                        {
                            RecommendedBible obj = new RecommendedBible();
                            obj.Id = int.Parse(reader["Id"].ToString());
                            obj.Title = reader["Title"].ToString();
                            obj.Abbreviation = reader["Abbreviation"].ToString();
                            obj.SubTitle = reader["SubTitle"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.Link = reader["Link"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();
                            result.Add(obj);
                        }
                    }

                }
                return result;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(string Title, string Abbreviation, string SubTitle,
            string Description, string Link, string Status, string CreatedBy, DateTime CreatedOn,
            string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "RecommendedBible_Insert";
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Title", Title);
                    command.Parameters.AddWithValue("@Abbreviation", Abbreviation);
                    command.Parameters.AddWithValue("@SubTitle", SubTitle);
                    command.Parameters.AddWithValue("@Description", Description);
                    command.Parameters.AddWithValue("@Link", Link);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
