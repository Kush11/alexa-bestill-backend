﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class MobileSharingSettingsDataService : BaseObj, IMobileSharingSettingsDataService
    {
        private readonly string _connString;

        public MobileSharingSettingsDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");

        }

        public async Task<MobileSettingsSharing> GetMobileSharingSetting(int UserId)
        {
            try
            {
                string commandText = "ShareSetting_GetByUserId";
                MobileSettingsSharing obj = null;
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new MobileSettingsSharing();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.EnableSharingViaText = reader["EnableSharingViaText"].ToString();
                            obj.EnableSharingViaEmail = reader["EnableSharingViaEmail"].ToString();
                            obj.ChurchId = int.Parse(reader["ChurchId"].ToString());
                            obj.Email = reader["Email"].ToString();
                            obj.Phone = reader["Phone"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();

                        }
                    }
                    return obj;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(string EnableSharingViaText, string EnableSharingViaEmail, int ChurchId,
            string Email, string Phone, string Status, string CreatedBy, DateTime CreatedOn,
            string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "ShareSetting_Insert";
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@EnableSharingViaText", EnableSharingViaText);
                    command.Parameters.AddWithValue("@EnableSharingViaEmail", EnableSharingViaEmail);
                    command.Parameters.AddWithValue("@ChurchId", ChurchId);
                    command.Parameters.AddWithValue("@Email", Email);
                    command.Parameters.AddWithValue("@Phone", Phone);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}
