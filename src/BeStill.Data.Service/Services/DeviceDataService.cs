﻿using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class DeviceDataService : BaseObj, IDeviceDataService
	{
        public async Task<Guid> Insert(string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
			try
			{
				string CommandText = "Device_Insert";
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@DeviceId", DeviceId);
					command.Parameters.AddWithValue("@Model", Model);
					command.Parameters.AddWithValue("@Name", Name);
					command.Parameters.AddWithValue("@Status", Status);
					command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
					connection.Open();
					await command.ExecuteNonQueryAsync();
				}
				return Guid.NewGuid();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}
