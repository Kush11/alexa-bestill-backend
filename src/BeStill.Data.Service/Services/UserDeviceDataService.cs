﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class UserDeviceDataService : BaseObj, IUserDeviceDataService
    {
        public async Task Insert(string UserId, string DeviceId, string Model, string Name, string Status, string CreatedBy)
        {
			try
			{
				string CommandText = "UserDevice_Insert";
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@UserId", UserId);
					command.Parameters.AddWithValue("@DeviceId", DeviceId);
					command.Parameters.AddWithValue("@Model", Model);
					command.Parameters.AddWithValue("@Name", Name);
					command.Parameters.AddWithValue("@Status", Status);
					command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
					connection.Open();
					await command.ExecuteNonQueryAsync();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<UserDevice> GetUserDeviceById(string UserId)
		{
			try
			{
				string CommandText = "UserDevice_GetByUserId";

				UserDevice obj = null;

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@UserId", UserId);

					connection.Open();

					using (SqlDataReader reader = await command.ExecuteReaderAsync())
					{
						while (reader.Read())
						{
							obj = new UserDevice();
							obj.UserId = reader["UserId"].ToString();
							obj.DeviceId = reader["DeviceId"].ToString();
							obj.Status = reader["Status"].ToString();
							obj.CreatedBy = reader["CreatedBy"].ToString();
							obj.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
							obj.ModifiedBy = reader["ModifiedBy"].ToString();
							obj.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
						}
					}
				}
				return obj;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
