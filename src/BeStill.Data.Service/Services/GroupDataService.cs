﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class GroupDataService : BaseObj, IGroupDataService
    {
        public async Task Insert(string Name, string Description, string Organization, string Location, string CreatedBy)
        {
            try
            {
                string CommandText = "Group_Insert";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", 0);
                    command.Parameters.AddWithValue("@Name", Name);
                    command.Parameters.AddWithValue("@Status", "active");
                    command.Parameters.AddWithValue("@Description", Description);
                    command.Parameters.AddWithValue("@Organization", Organization);
                    command.Parameters.AddWithValue("@Location", Location);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task Delete(int GroupId)
        {
            try
            {
                string CommandText = "Group_DeleteById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Status", "active");
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Update(int GroupId, string Name, string Description, string Organization, string Location, string ModifiedBy)
        {
            try
            {
                string CommandText = "Group_UpdateById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Name", Name);
                    command.Parameters.AddWithValue("@Status", "active");
                    command.Parameters.AddWithValue("@Description", Description);
                    command.Parameters.AddWithValue("@Organization", Organization);
                    command.Parameters.AddWithValue("@Location", Location);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Group>> GetAllGroups(string UserId)
        {
            try
            {
                string CommandText = "Group_GetAllByUserId";

                List<Group> result = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        result = new List<Group>();
                        while (reader.Read())
                        {
                            Group obj = new Group();
                            obj.UserId = reader["UserId"].ToString();
                            obj.Name = reader["Name"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.Organization = reader["Organization"].ToString();
                            obj.Location = reader["Location"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            result.Add(obj);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Group> GetGroup(int GroupId)
        {
            try
            {
                string CommandText = "Group_GetById";

                Group obj = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new Group();
                            obj.UserId = reader["UserId"].ToString();
                            obj.Name = reader["Name"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.Organization = reader["Organization"].ToString();
                            obj.Location = reader["Location"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GroupUser> IsAdmin(string UserId)
        {
            try
            {
                string CommandText = "GroupUser_IsAdmin";

                GroupUser obj = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new GroupUser();
                            obj.IsAdmin = Convert.ToBoolean(reader["IsAdmin"]);
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task ChangeMemberType(int GroupId, string UserId)
        {
            try
            {
                string CommandText = "GroupUser_UpdateGroupMemberType";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task AddMemberToGroup(string UserId, bool IsAdmin, bool IsModerator, int GroupId, string CreatedBy)
        {
            try
            {
                string CommandText = "GroupUser_AddMemberToGroup";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Status", "active");
                    command.Parameters.AddWithValue("@IsAdmin", 0);
                    command.Parameters.AddWithValue("@IsModerator", 1);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task RemoveMemberFromGroup(int GroupId, string UserId)
        {
            try
            {
                string CommandText = "GroupUser_RemoveMemberFromGroup";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GroupInvite> GetInviteStatus(string UserId, int GroupId)
        {
            try
            {
                string CommandText = "Group_GetInvitationStatus";

                GroupInvite result = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@GroupId", GroupId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            result = new GroupInvite();
                            result.UserId = reader["UserId"].ToString();
                            result.GroupId = int.Parse(reader["GroupId"].ToString());
                            result.Status = reader["Status"].ToString();
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task UpdateInviteStatus(string UserId, int GroupId, string Status)
        {
            try
            {
                string CommandText = "Group_UpdateInviteStatus";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Status", Status);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task InsertInviteStatus(string UserId, int GroupId, string Status, string CreatedBy)
        {
            try
            {
                string CommandText = "Group_InsertInvitationStatus";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupInviteId", 0);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.Parameters.AddWithValue("@CreatedBy", "SYSTEM");
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
