﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class IntentHistoryLogTempDataService : BaseObj, IIntentHistoryLogTempDataService
    {
        public async Task BulkInsert(List<IntentHistoryLogTemp> intentHistories)
        {
			try
			{
				
				DataTable tbl = new DataTable();
				tbl.Columns.Add(new DataColumn("NextToken", typeof(string)));
				tbl.Columns.Add(new DataColumn("SkillId", typeof(string)));
				tbl.Columns.Add(new DataColumn("DialogAct", typeof(string)));
				tbl.Columns.Add(new DataColumn("IntentName", typeof(string)));
				tbl.Columns.Add(new DataColumn("IntentConfidence", typeof(string)));
				tbl.Columns.Add(new DataColumn("SlotName", typeof(string)));
				tbl.Columns.Add(new DataColumn("InteractionType", typeof(string)));
				tbl.Columns.Add(new DataColumn("PublicationStatus", typeof(string)));
				tbl.Columns.Add(new DataColumn("UtteranceText", typeof(string)));
				tbl.Columns.Add(new DataColumn("Locale", typeof(string)));
				tbl.Columns.Add(new DataColumn("Stage", typeof(string)));
				tbl.Columns.Add(new DataColumn("Date", typeof(DateTime)));

				foreach (var item in intentHistories)
				{
					DataRow dr = tbl.NewRow();
					dr["NextToken"] = item.NextToken;
					dr["SkillId"] = item.SkillId;
					dr["DialogAct"] = item.DialogAct;
					dr["IntentName"] = item.IntentName;
					dr["IntentConfidence"] = item.IntentConfidence;
					dr["SlotName"] = item.SlotName;
					dr["InteractionType"] = item.InteractionType;
					dr["PublicationStatus"] = item.PublicationStatus;
					dr["UtteranceText"] = item.UtteranceText;
					dr["Locale"] = item.UtteranceText;
					dr["Stage"] = item.UtteranceText;
					dr["Date"] = item.Date;
					tbl.Rows.Add(dr);
				}


				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlBulkCopy objbulk = new SqlBulkCopy(connection);
					objbulk.DestinationTableName = "IntentHistoryLogTemp";
					objbulk.ColumnMappings.Add("NextToken", "NextToken");
					objbulk.ColumnMappings.Add("SkillId", "SkillId");
					objbulk.ColumnMappings.Add("DialogAct", "DialogAct");
					objbulk.ColumnMappings.Add("IntentName", "IntentName");
					objbulk.ColumnMappings.Add("IntentConfidence", "IntentConfidence");
					objbulk.ColumnMappings.Add("SlotName", "SlotName");
					objbulk.ColumnMappings.Add("InteractionType", "InteractionType");
					objbulk.ColumnMappings.Add("PublicationStatus", "PublicationStatus");
					objbulk.ColumnMappings.Add("UtteranceText", "UtteranceText");
					objbulk.ColumnMappings.Add("Locale", "UtteranceText");
					objbulk.ColumnMappings.Add("Stage", "UtteranceText");
					objbulk.ColumnMappings.Add("Date", "Date");

					connection.Open();
					//insert bulk Records into DataBase.  
					await objbulk.WriteToServerAsync(tbl);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}
