﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class MobileSettingPushNotificationDataService : BaseObj, IMobileSettingPushNotificationDataService
    {
        private readonly string _connString;
        
        public MobileSettingPushNotificationDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");

        }

        public async Task Insert(string AllowPushNotification, string AllowTextNotification, string EmailUpdateNotification,
            int EmailUpdateFrequency, string NotifyMeSomeOneSharePrayerWithMe, string NotifyMeSomeOnePostOnGroup,
            string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "PushNotificationSetting_Insert";
                using(SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AllowPushNotification", AllowPushNotification);
                    command.Parameters.AddWithValue("@AllowTextNotification", AllowTextNotification);
                    command.Parameters.AddWithValue("@EmailUpdateNotification", EmailUpdateNotification);
                    command.Parameters.AddWithValue("@EmailUpdateFrequency", EmailUpdateFrequency);
                    command.Parameters.AddWithValue("@NotifyMeSomeOneSharePrayerWithMe", NotifyMeSomeOneSharePrayerWithMe);
                    command.Parameters.AddWithValue("@NotifyMeSomeOnePostOnGroup", NotifyMeSomeOnePostOnGroup);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<MobileSettingPushNotification> GetPushSettings(int UserId)
        {
            try
            {
                string commandText = "PushNotificationSetting_GetByUserId";

                MobileSettingPushNotification obj = null;

                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new MobileSettingPushNotification();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.AllowPushNotification = reader["AllowPushNotification"].ToString();
                            obj.AllowTextNotification = reader["AllowTextNotification"].ToString();
                            obj.EmailUpdateNotification = reader["EmailUpdateNotification"].ToString();
                            obj.EmailUpdateFrequency = int.Parse(reader["EmailUpdateFrequency"].ToString());
                            obj.NotifyMeSomeOneSharePrayerWithMe = reader["NotifyMeSomeOneSharePrayerWithMe"].ToString();
                            obj.NotifyMeSomeOnePostOnGroup = reader["NotifyMeSomeOnePostOnGroup"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();
                        }
                    }

                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<MobileSettingPushNotification> GetSettings(int UserId)
        {
            throw new NotImplementedException();
        }
    }
}
