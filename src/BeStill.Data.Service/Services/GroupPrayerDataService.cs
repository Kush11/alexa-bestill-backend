﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class GroupPrayerDataService : BaseObj, IGroupPrayerDataService
    {
        public async Task AddPrayerToGroup(int prayerId, int groupId, string createdBy)
        {
            try
            {
                string CommandText = "Prayer_InsertToGroup";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    command.Parameters.AddWithValue("@GroupId", groupId);
                    command.Parameters.AddWithValue("@CreatedBy", createdBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task ChangeGroupPrayerStatus(int PrayerId, int GroupId)
        {
            try
            {
                string CommandText = "Prayer_ChangeGroupPrayerStatus";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", PrayerId);
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@Status", "active");
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task FlagPrayer(int prayerId)
        {
            try
            {
                string CommandText = "Prayer_FlagById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Prayer>> GetGroupPrayers(int GroupId, string CreatedBy)
        {
            try
            {
                string CommandText = "Prayer_GetAllByGroupId";

                List<Prayer> result = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@GroupId", GroupId);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        result = new List<Prayer>();
                        int start = 0;
                        while (reader.Read())
                        {
                            Prayer obj = new Prayer();
                            obj.GroupId = int.Parse(reader["GroupId"].ToString());
                            obj.PrayerId = reader["PrayerId"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.Type = reader["Type"].ToString();
                            obj.Title = reader["Title"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.IsAnswer = reader["IsAnswer"].ToString();
                            obj.IsFavourite = Convert.ToBoolean(reader["IsFavourite"]);
                            obj.Sequence = Convert.ToInt32(reader["Sequence"]);
                            obj.Sort = ++start;
                            result.Add(obj);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task HidePrayer(string UserId, int prayerId, string CreatedBy)
        {
            try
            {
                string CommandText = "Prayer_HideFromMe";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
