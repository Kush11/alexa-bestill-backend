﻿using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class IntentHistoryLogDataService : BaseObj, IIntentHistoryLogDataService
    {
        public async Task InsertFromTemp()
        {
			try
			{
				string CommandText = "IntentHistoryLog_InsertFromTemp";
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlCommand command = new SqlCommand(CommandText, connection);
					command.CommandType = CommandType.StoredProcedure;
					connection.Open();
					await command.ExecuteNonQueryAsync();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}
