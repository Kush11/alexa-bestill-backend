﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class ReminderDataService : BaseObj, IReminderDataService
    {
        public async Task Delete(Guid ReminderId)
        {
            try
            {
                string CommandText = "Reminder_DeleteById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ReminderId", ReminderId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Reminder>> GetReminders(string UserId)
        {
            try
            {
                string CommandText = "Reminder_GetByUserId";

                List<Reminder> result = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        result = new List<Reminder>();
                        int start = 0;
                        while (reader.Read())
                        {
                            Reminder obj = new Reminder();
                            obj.ReminderId = Guid.Parse(reader["ReminderId"].ToString());
                            obj.PrayerId = reader["PrayerId"]?.ToString();
                            obj.UserId = reader["UserId"].ToString();
                            obj.Title = reader["Title"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"]?.ToString();
                            obj.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            obj.EndDate = Convert.ToDateTime(reader["EndDate"]);
                            obj.Token = reader["Token"]?.ToString();
                            obj.Frequency = int.Parse(reader["Frequency"].ToString());
                            obj.Sort = ++start;
                            result.Add(obj);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(string UserId, int Frequency, string Token, DateTime StartDate, DateTime EndDate, string Title, string CreatedBy, string prayerId)
        {
            try
            {
                string CommandText = "Reminder_Insert";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ReminderId", 0);
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@Title", Title);
                    command.Parameters.AddWithValue("@Token", Token);
                    command.Parameters.AddWithValue("@Frequency", Frequency);
                    command.Parameters.AddWithValue("@StartDate", StartDate);
                    command.Parameters.AddWithValue("@EndDate", EndDate);
                    command.Parameters.AddWithValue("@Status", "active");
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
