﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class PrayerMobileSettingsDataService : BaseObj, IPrayerMobileSettingsDataService
    {
        private readonly string _connString;

        public PrayerMobileSettingsDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");
        }

        public async Task<PrayerMobileSettings> GetPrayerSetting(int UserId)
        {
            try
            {
                string commandText = "PrayerMobileSettings_GetByUserId";

                PrayerMobileSettings obj = null;

                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new PrayerMobileSettings();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.Frequency = int.Parse(reader["Frequency"].ToString());
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();
                        }
                    }

                    return obj;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(int Frequency, DateTime Date, DateTime Time, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "PrayerMobileSettings_Insert";
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Frequency", Frequency);
                    command.Parameters.AddWithValue("@Date", Date);
                    command.Parameters.AddWithValue("@Time", Time);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
