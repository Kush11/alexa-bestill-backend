﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class GroupMobileSettingsDataService : BaseObj, IGroupMobileSettingsDataService
    {

        private readonly string _connString;

        public GroupMobileSettingsDataService(IConfiguration configuration)
        {
            _connString = configuration.GetConnectionString("ConnectionString");
        }

        public async Task<GroupMobileSettings> GetSettingByUserId(int UserId)
        {
            try
            {
                string commandText = "GroupMobileSettings_GetByUserId";

                GroupMobileSettings obj = null;

                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new GroupMobileSettings();
                            obj.UserId = int.Parse(reader["UserId"].ToString());
                            obj.DeviceId = int.Parse(reader["DeviceId"].ToString());
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.ModifiedBy = reader["ModifiedBy"].ToString();
                        }
                    }

                    return obj;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(int DeviceId, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn)
        {
            try
            {
                string commandText = "GroupMobileSettings_Insert";
                using (SqlConnection connection = new SqlConnection(_connString))
                {
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@DeviceId", DeviceId);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    command.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                    command.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", ModifiedOn);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
