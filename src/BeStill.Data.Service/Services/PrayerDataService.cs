﻿using BeStill.Data.Abstraction.Models;
using BeStill.Data.Abstraction.Services.Data;
using BeStill.Data.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Service.Services
{
    public class PrayerDataService : BaseObj, IPrayerDataService
    {
        public async Task ArchivePrayer(Guid PrayerId)
        {
            try
            {
                string CommandText = "Prayer_ArchiveById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", PrayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task UnsnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            try
            {
                string CommandText = "Prayer_ArchiveById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", PrayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task SnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate)
        {
            try
            {
                string CommandText = "Prayer_ArchiveById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", PrayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task Delete(Guid prayerId)
        {
            try
            {
                string CommandText = "Prayer_DeleteById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteAll(string userId)
        {
            try
            {
                string CommandText = "Prayer_DeleteAllByUserId";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", userId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DateTime> GetLastPrayedDate(string UserId)
        {
            try
            {
                string CommandText = "User_GetLastPrayedDateByUserId";

                DateTime date = DateTime.MinValue;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            date = Convert.ToDateTime(reader["LastPrayed"]);
                        }
                    }
                }
                return date;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Prayer> GetPrayer(Guid PrayerId)
        {
            try
            {
                string CommandText = "Prayer_GetById";

                Prayer obj = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", PrayerId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            obj = new Prayer();
                            obj.UserId = reader["UserId"].ToString();
                            obj.PrayerId = reader["PrayerId"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.Type = reader["Type"].ToString();
                            obj.Title = reader["Title"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.IsAnswer = reader["IsAnswer"].ToString();
                            obj.IsFavourite = Convert.ToBoolean(reader["IsFavourite"]);
                            obj.Sequence = Convert.ToInt32(reader["Sequence"]);
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Prayer>> GetPrayers(string UserId)
        {
            try
            {
                string CommandText = "Prayer_GetAllByUserId";

                List<Prayer> result = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);

                    connection.Open();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        result = new List<Prayer>();
                        int start = 0;
                        while (reader.Read())
                        {
                            Prayer obj = new Prayer();
                            obj.UserId = reader["UserId"].ToString();
                            obj.PrayerId = reader["PrayerId"].ToString();
                            obj.Status = reader["Status"].ToString();
                            obj.CreatedBy = reader["CreatedBy"].ToString();
                            obj.Type = reader["Type"].ToString();
                            obj.Title = reader["Title"].ToString();
                            obj.Description = reader["Description"].ToString();
                            obj.IsAnswer = reader["IsAnswer"].ToString();
                            obj.IsFavourite = Convert.ToBoolean(reader["IsFavourite"]);
                            obj.Sequence = Convert.ToInt32(reader["Sequence"]);
                            obj.Sort = ++start;
                            result.Add(obj);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Insert(string UserId, string Type, string Title, string CreatedBy)
        {
            try
            {
                string CommandText = "Prayer_Insert";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@PrayerId", DBNull.Value);
                    command.Parameters.AddWithValue("@Sequence", 0);
                    command.Parameters.AddWithValue("@IsFavourite", false);
                    command.Parameters.AddWithValue("@MusicId", 0);
                    command.Parameters.AddWithValue("@ChurchId", 0);
                    command.Parameters.AddWithValue("@Status", "active");
                    command.Parameters.AddWithValue("@IsAnswer", "n");
                    command.Parameters.AddWithValue("@Type", Type);
                    command.Parameters.AddWithValue("@Title", Title);
                    command.Parameters.AddWithValue("@Description", "");
                    command.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task MarkAsAnswer(Guid prayerId)
        {
            try
            {
                string CommandText = "Prayer_MarkAsAnsweredById";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PrayerId", prayerId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task UpdateLastPrayedDate(string UserId)
        {
            try
            {
                string CommandText = "User_UpdateLastPrayed";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand(CommandText, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserId", UserId);
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
