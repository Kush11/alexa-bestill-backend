﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class UserPreference
    {
        public string UserId { get; set; }
        public int PrayerTime { get; set; }
        public string BackgroundMusic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
