﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class GroupInvite
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
        public string Status { get; set; }
    }
}
