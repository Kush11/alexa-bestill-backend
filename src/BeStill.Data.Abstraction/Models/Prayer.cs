﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class Prayer
    {
        public string PrayerId { get; set; }
        public string UserId { get; set; }
        public int GroupId { get; set; }
        public int Sequence { get; set; }
        public bool IsFavourite { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string IsAnswer { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public bool HasReminder { get; set; }
        public DateTime SnoozeExpiryDate { get; set; }
        public int Sort { get; set; }
    }
}
