﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class MobileSettingPushNotification
    {
        public int UserId { get; set; }
        public string AllowPushNotification { get; set; }
        public string AllowTextNotification { get; set; }
        public string EmailUpdateNotification { get; set; }
        public int EmailUpdateFrequency { get; set; }
        public string NotifyMeSomeOneSharePrayerWithMe { get; set; }
        public string NotifyMeSomeOnePostOnGroup { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
