﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class GroupUser
    {
        public int GroupId { get; set; }
        public string UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsModerator { get; set; }
        public string CreatedBy { get; set; }
    }
}
