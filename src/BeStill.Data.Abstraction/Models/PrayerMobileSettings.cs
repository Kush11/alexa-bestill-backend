﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class PrayerMobileSettings
    {
        public int UserId { get; set; }
        public int Frequency { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
