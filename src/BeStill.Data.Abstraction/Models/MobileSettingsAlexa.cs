﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class MobileSettingsAlexa
    {
        public int UserId { get; set; }
        public string AllowPrayerTimeNotification { get; set; }
        public string SyncAlexa { get; set; }
    }
}
