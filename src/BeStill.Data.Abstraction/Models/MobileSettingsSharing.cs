﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Data.Abstraction.Models
{
    public class MobileSettingsSharing
    {
        public int UserId { get; set; }
        public string EnableSharingViaText { get; set; }
        public string EnableSharingViaEmail { get; set; }
        public int ChurchId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
