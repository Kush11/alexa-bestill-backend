﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IMobileSharingSettingsDataService
    {
        Task Insert(string EnableSharingViaText, string EnableSharingViaEmail, int ChurchId, string Email,
                    string Phone, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn);

        Task<MobileSettingsSharing> GetMobileSharingSetting(int UserId);

    }
}
