﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IIntentHistoryLogTempDataService
    {
        Task BulkInsert(List<IntentHistoryLogTemp> intentHistories);
    }
}
