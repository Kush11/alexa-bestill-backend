﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IUserDeviceDataService
    {
        Task Insert(string UserId, string DeviceId, string Model, string Name, string Status, string CreatedBy);
        Task<UserDevice> GetUserDeviceById(string UserId);
    }
}
