﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IPrayerDataService
    {
        Task Insert(string UserId, string Type, string Title, string CreatedBy);
        Task<List<Prayer>> GetPrayers(string UserId);
        Task<Prayer> GetPrayer(Guid PrayerId);
        Task DeleteAll(string userId);
        Task Delete(Guid prayerId);
        Task MarkAsAnswer(Guid prayerId);
        Task ArchivePrayer(Guid PrayerId);
        Task UnsnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate);
        Task SnoozePrayer(Guid PrayerId, DateTime SnoozeExpiryDate);
        Task<DateTime> GetLastPrayedDate(string UserId);
        Task UpdateLastPrayedDate(string UserId);
    }
}
