﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IRecommendedBibleDataService
    {
        Task Insert(string Title, string Abbreviation, string SubTitle, string Description,
            string Link, string Status, string CreatedBy, DateTime CreatedOn, string ModifiedBy, DateTime ModifiedOn);

        Task<List<RecommendedBible>> GetAllBibles();
    }
}
