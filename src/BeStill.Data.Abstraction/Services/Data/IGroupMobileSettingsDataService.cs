﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IGroupMobileSettingsDataService
    {
        Task Insert(int DeviceId, string Status, string CreatedBy, DateTime CreatedOn,
            string ModifiedBy, DateTime ModifiedOn);

        Task<GroupMobileSettings> GetSettingByUserId(int UserId);
    }
}
