﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IAlexaMobileSettingDataService
    {
        Task Insert(string AllowPrayerTimeNotification, string SyncAlexa);

        Task<MobileSettingsAlexa> GetAlexaSettings(int UserId);
    }
}
