﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IMobileSettingDataService
    {
        Task Insert(string Appearance, string DefaultSortBy, int DefaultSnoozeDuration, string ArchiveAutoDelete,
            string IncludeAnsweredPrayerAutoDelete, string Status, string CreatedBy, DateTime CreatedOn,
            string ModifiedBy, DateTime ModifiedOn);

        Task<MobileSettings> GetSettings(int UserId);
    }
}
