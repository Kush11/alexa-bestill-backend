﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IReminderDataService
    {
        Task Insert(string UserId, int Frequency, string Token, DateTime StartDate, DateTime EndDate, string Title, string CreatedBy, string prayerId);
        Task<List<Reminder>> GetReminders(string UserId);
        Task Delete(Guid ReminderId);
    }
}
