﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IGroupDataService
    {
        Task Insert(string Name, string Description, string Organization, string Location, string CreatedBy);
        Task Delete(int GroupId);
        Task Update(int GroupId, string Name, string Description, string Organization, string Location, string ModifiedBy);
        Task<List<Group>> GetAllGroups(string UserId);
        Task<Group> GetGroup(int GroupId);
        Task<GroupUser> IsAdmin(string UserId);
        Task ChangeMemberType(int GroupId, string UserId);
        Task AddMemberToGroup(string UserId, bool IsAdmin, bool IsModerator, int GroupId, string CreatedBy);
        Task RemoveMemberFromGroup(int GroupId, string UserId);
        Task<GroupInvite> GetInviteStatus(string UserId, int GroupId);
        Task UpdateInviteStatus(string UserId, int GroupId, string Status);
        Task InsertInviteStatus(string UserId, int GroupId, string Status, string CreatedBy);

    }
}
