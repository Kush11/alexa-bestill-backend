﻿using BeStill.Data.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IUserPreferenceDataService
    {
        Task InsertOrUpdate(string UserId, int PrayerTime, string BackgroundMusic, string CreatedBy);
        Task<UserPreference> GetUserPreferenceById(string UserId);
    }
}
