﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IDeviceDataService
    {
        Task<Guid> Insert(string DeviceId, string Model, string Name, string Status, string CreatedBy);
    }
}
