﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Data.Abstraction.Services.Data
{
    public interface IIntentHistoryLogDataService
    {
        Task InsertFromTemp();
    }
}
