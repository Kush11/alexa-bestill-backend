﻿using BeStill.Abstraction.DTO;
using BeStill.Abstraction.Service;
using BeStill.Webjob.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Webjob.AlexaRequestLog
{
    public class IntentHistoryLog
    {
        private readonly IIntentHistoryLogTempService _intentHistoryLogTempService;
        private readonly IIntentHistoryLogService _intentHistoryLogService;
        private readonly IConfiguration _configuration; 
        //private readonly ILogger<IntentHistoryLog> _logger;

        private const string StorageConnectionStringKey = "StorageConnectionString";


        public IntentHistoryLog(IIntentHistoryLogTempService intentHistoryLogTempService, IIntentHistoryLogService intentHistoryLogService, IConfiguration configuration)//, ILogger<IntentHistoryLog> logger)
        {
            _intentHistoryLogTempService = intentHistoryLogTempService;
            _intentHistoryLogService = intentHistoryLogService;
            _configuration = configuration;
            //_logger = logger;
        }



        [Singleton]
        public async Task ProcessIntentRequestHistoryLog([TimerTrigger("* * * * *", RunOnStartup = true)] TimerInfo myTimer, ILogger _logger)
        {
            try
            {
                bool fetchNext = true;
                List<IntentHistoryLogTemp_InputDTO> intentHistoryLogs = new List<IntentHistoryLogTemp_InputDTO>();
                IntentHistoryLogTemp_InputDTO obj;
                Uri u = new Uri($"{_configuration.GetValue<string>("BaseUrl")}v1/skills/{_configuration.GetValue<string>("SkillId")}/history/intentRequests");
                _logger.LogInformation("First page loading...");
                var history = await GetHistories(u);
                _logger.LogInformation("First page loaded");
                if (history != null)
                {
                    while (fetchNext)
                    {
                        foreach (var item in history.Items)
                        {
                            obj = new IntentHistoryLogTemp_InputDTO();
                            obj.Date = DateTime.Now;
                            obj.DialogAct = item.DialogAct?.Name;
                            obj.IntentConfidence = item.Intent.Confidence?.Bin;
                            obj.IntentName = item.Intent.Name;
                            obj.InteractionType = item.InteractionType;
                            obj.NextToken = history.NextToken;
                            obj.PublicationStatus = item.PublicationStatus;
                            obj.SkillId = history.SkillId;
                            obj.SlotName = item.Intent.Slots?.Answer?.Name;
                            obj.UtteranceText = item.UtteranceText;
                            obj.Locale = item.Locale;
                            obj.Stage = item.Stage;
                            intentHistoryLogs.Add(obj);
                        }

                        if (history.IsTruncated)
                        {
                            u = new Uri($"{_configuration.GetValue<string>("BaseUrl")}{history._Links.Next.Href}");
                            _logger.LogInformation("Fetching next page...");
                            history = await GetHistories(u);
                            _logger.LogInformation("Next page fetched");
                        }
                        else
                        {
                            fetchNext = false;
                            _logger.LogInformation("No more page to fetch");
                        }
                    }
                }

                _logger.LogInformation("Insert bulk records to the temporal logging table");
                await _intentHistoryLogTempService.BulkInsert(intentHistoryLogs);
                _logger.LogInformation("Push record to logging table");
                await _intentHistoryLogService.InsertFromTemp();
                _logger.LogInformation("Finished");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

        }

        public async Task<IntentRequestHistory> GetHistories(Uri u)
        {
            
            var token = _configuration.GetValue<string>("AccessToken");
            IntentRequestHistory history = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage result = await client.GetAsync(u);
                result.EnsureSuccessStatusCode();
                var resp = await result.Content.ReadAsStringAsync();

                history = JsonConvert.DeserializeObject<IntentRequestHistory>(resp);
            }

            return history;
        }
    }
}
