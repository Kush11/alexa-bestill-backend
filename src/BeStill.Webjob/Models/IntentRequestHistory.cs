﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Webjob.Models
{
    public partial class IntentRequestHistory
    {
        public Links _Links { get; set; }
        public string NextToken { get; set; }
        public long TotalCount { get; set; }
        public string SkillId { get; set; }
        public long StartIndex { get; set; }
        public bool IsTruncated { get; set; }
        public List<Item> Items { get; set; }
    }

    public partial class Item
    {
        public DialogAct DialogAct { get; set; }
        public Intent Intent { get; set; }
        public string Locale { get; set; }
        public string InteractionType { get; set; }
        public string Stage { get; set; }
        public string PublicationStatus { get; set; }
        public string UtteranceText { get; set; }
    }

    public partial class DialogAct
    {
        public string Name { get; set; }
    }

    public partial class Intent
    {
        public string Name { get; set; }
        public Confidence Confidence { get; set; }
        public Slots Slots { get; set; }
    }

    public partial class Confidence
    {
        public string Bin { get; set; }
    }

    public partial class Slots
    {
        public DialogAct Answer { get; set; }
    }

    public partial class Links
    {
        public Next Self { get; set; }
        public Next Next { get; set; }
    }

    public partial class Next
    {
        public string Href { get; set; }
    }

}
