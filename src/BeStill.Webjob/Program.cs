﻿using AutoMapper;
using BeStill.Alexa.Webservice;
using BeStill.Business.Service.ObjectMapper;
using BeStill.Data.Service.Common;
using BeStill.Webjob.AlexaRequestLog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace BeStill.Webjob
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (host)
            {
                // //initialize auto mapper
                //var mapper = new MappingConfiguration();
                //mapper.Configure();

                await host.RunAsync();
            }



        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var builder = Host.CreateDefaultBuilder(args);

            builder.ConfigureWebJobs(b =>
            {
                b.AddTimers();
                b.AddAzureStorageCoreServices();
                b.AddAzureStorage();
            });

            //order matters - it looks like one of the ConfigureWebJobs adds appsettings.json back to bottom
            builder.ConfigureAppConfiguration((context, config) =>
            {
                var env = context.HostingEnvironment;

                config.SetBasePath(Directory.GetCurrentDirectory());
                config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

                //allow for machine specific overrides - developer convenience - (optional - no need to check in developer machine config)
                config.AddJsonFile($"appsettings.{Environment.MachineName}.json", optional: true);

                config.AddEnvironmentVariables();
                config.AddCommandLine(args);
            });


            builder.ConfigureServices((context, services) =>
            {

                var connectionString = context.Configuration.GetConnectionString("ConnectionString");
                BaseObj.ConnectionString = connectionString;

                services.AddTransient<IntentHistoryLog>();

                ServiceRegistration.Setup(services, false, false);
                var config = new MapperConfiguration(c => c.AddProfile(new ApplicationProfile()));
                var mapper = config.CreateMapper();
                services.AddSingleton(mapper);

            });


            builder.ConfigureLogging((context, b) =>
            {
                //b.AddApplicationInsights();
                //b.AddFilter("Microsoft", LogLevel.Warning);
                //b.AddFilter("System", LogLevel.Warning);
                //b.SetMinimumLevel(LogLevel.Debug);
                b.AddConsole();
            });

            return builder;
        }

    }
}
