﻿CREATE TABLE [dbo].[UserPreference]
(
	[UserId] NVARCHAR(500) NOT NULL PRIMARY KEY, 
    [PrayerTime] INT NOT NULL,
    [BackgroundMusic] NVARCHAR(50) NULL,
	[CreatedBy]   NVARCHAR (300)   NULL,
	[CreatedOn]   DATETIME         NULL,
	[ModifiedBy]  NVARCHAR (300)   NULL,
	[ModifiedOn]  DATETIME         NULL, 
)
