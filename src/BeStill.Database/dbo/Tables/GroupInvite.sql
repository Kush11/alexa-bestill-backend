﻿CREATE TABLE [dbo].[GroupInvite] (
    [GroupInviteId]     INT              NOT NULL IDENTITY,
    [UserId]      NVARCHAR(500) NOT NULL,
    [GroupId]     INT              NOT NULL,
    [Status]      NVARCHAR (10)    NOT NULL,
    [CreatedBy]   NVARCHAR (300)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (300)   NULL,
    [ModifiedOn]  DATETIME         NULL
);

