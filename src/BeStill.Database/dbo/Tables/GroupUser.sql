﻿CREATE TABLE [dbo].[GroupUser] (
    [GroupId]     INT              NOT NULL,
    [UserId]      NVARCHAR(500) NOT NULL,
    [IsAdmin]     BIT              NOT NULL,
    [IsModerator] BIT              NOT NULL,
    [Status]      NVARCHAR (10)    NOT NULL,
    [CreatedBy]   NVARCHAR (300)   NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  NVARCHAR (300)   NULL,
    [ModifiedOn]  DATETIME         NULL
);

