﻿CREATE TABLE [dbo].[MobileSettingPushNotification]
(
	[UserId] INT NOT NULL IDENTITY, 
    [AllowPushNotification] NCHAR(10) NOT NULL, 
    [AllowTextNotification] NCHAR(10) NOT NULL, 
    [EmailUpdateNotification] NVARCHAR(300) NOT NULL, 
    [EmailUpdateFrequency] INT NOT NULL, 
    [NotifyMeSomeOneSharePrayerWithMe] NCHAR(10) NOT NULL, 
    [NotifyMeSomeOnePostOnGroup] NCHAR(10) NOT NULL, 
    [CreatedBy] NVARCHAR(300) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NOT NULL, 
    [ModifiedOn] DATETIME NOT NULL
)
