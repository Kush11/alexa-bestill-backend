﻿CREATE TABLE [dbo].[PrayerDisable] (
    [GroupPrayerId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]     NVARCHAR(500) NOT NULL,
    [PrayerId]    UNIQUEIDENTIFIER            NOT NULL,
    [Status]      NVARCHAR (10)  NOT NULL,
    [CreatedBy]   NVARCHAR (300) NULL,
    [CreatedOn]   DATETIME       NULL,
    [ModifiedBy]  NVARCHAR (300) NULL,
    [ModifiedOn]  DATETIME       NULL
);
