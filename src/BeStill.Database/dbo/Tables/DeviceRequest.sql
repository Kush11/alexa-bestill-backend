﻿CREATE TABLE [dbo].[DeviceRequest] (
    [DeviceRequestId] UNIQUEIDENTIFIER NOT NULL,
    [DeviceId]        INT   NOT NULL,
    [Status]          NVARCHAR (10)    NOT NULL,
    [CreatedBy]       NVARCHAR (300)   NULL,
    [CreatedOn]       DATETIME         NULL,
    [ModifiedBy]      NVARCHAR (300)   NULL,
    [ModifiedOn]      DATETIME         NULL, 
    CONSTRAINT [FK_DeviceRequest_ToDevice] FOREIGN KEY ([DeviceId]) REFERENCES [Device]([Id])
);

