﻿CREATE TABLE [dbo].[Prayer] (
    [PrayerId]    UNIQUEIDENTIFIER             NOT NULL,
    [Type]        NVARCHAR (50)   NOT NULL,
    [Title]       NVARCHAR (50)   NOT NULL,
    [Description] NVARCHAR (1000) NOT NULL,
    [IsAnswer]    NCHAR (1)       NOT NULL,
    IsInAppropriate NCHAR(1)    NOT NULL,
    [MusicId]     INT             NULL,
    [ChurchId]    INT             NULL,
    [Status]      NVARCHAR (10)   NULL,
    [CreatedBy]   NVARCHAR (300)  NULL,
    [CreatedOn]   DATETIME        NULL,
    [ModifiedBy]  NVARCHAR (300)  NULL,
    [ModifiedOn]  DATETIME        NULL
);

