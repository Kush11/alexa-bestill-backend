﻿CREATE TABLE [dbo].[UserDevice]
(
	[UserId] NVARCHAR(500) NOT NULL, 
	[DeviceId] INT NOT NULL,
	[Status]      NVARCHAR (10)    NULL,
	[CreatedBy]   NVARCHAR (300)   NULL,
	[CreatedOn]   DATETIME         NULL,
	[ModifiedBy]  NVARCHAR (300)   NULL,
	[ModifiedOn]  DATETIME         NULL, 
    CONSTRAINT [FK_UserDevice_ToDevice] FOREIGN KEY ([DeviceId]) REFERENCES [Device]([Id])
)
