﻿CREATE TABLE [dbo].[MobileSettingsSharing]
(
	[UserId] INT NOT NULL IDENTITY, 
    [EnableSharingViaText] NCHAR(10) NOT NULL, 
    [EnableSharingViaEmail] NCHAR(10) NOT NULL, 
    [ChurchId] INT NOT NULL, 
    [Email] NVARCHAR(300) NOT NULL, 
    [Phone] NVARCHAR(50) NOT NULL, 
    [Status] NCHAR(10) NOT NULL, 
    [CreatedBy] NVARCHAR(300) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NOT NULL, 
    [ModifiedOn] DATETIME NOT NULL
)
