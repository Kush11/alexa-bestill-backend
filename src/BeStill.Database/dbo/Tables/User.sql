﻿CREATE TABLE [dbo].[User] (
    [UserId]     NVARCHAR(500) NOT NULL,
    [FirstName]  NVARCHAR (50)    NULL,
    [LastName]   NVARCHAR (50)    NULL,
    [Phone]      NVARCHAR (15)    NULL,
    [Email]      NVARCHAR (300)   NULL,
    [Dob]        DATETIME         NULL,
    [ChurchId]   INT              NULL,
    [Status]     NVARCHAR (10)    NULL,
    [CreatedBy]  NVARCHAR (300)   NULL,
    [CreatedOn]  DATETIME         NULL,
    [ModifiedBy] NVARCHAR (300)   NULL,
    [ModifiedOn] DATETIME         NULL, 
    [LastPrayed] DATETIME NULL
);

