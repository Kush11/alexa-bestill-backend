﻿CREATE TABLE [dbo].[AudioDevotion] (
    [DevotionId] INT              NOT NULL IDENTITY,
    [DocumentId] UNIQUEIDENTIFIER NOT NULL,
    [StartDate]  DATETIME         NOT NULL,
    [EndDate]    DATETIME         NOT NULL,
    [Status]     NVARCHAR (10)    NOT NULL,
    [CreatedBy]  NVARCHAR (300)   NULL,
    [CreatedOn]  DATETIME         NULL,
    [ModifiedBy] NVARCHAR (300)   NULL,
    [ModifiedOn] DATETIME         NULL
);

