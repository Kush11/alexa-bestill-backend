﻿CREATE TABLE [dbo].[MobileSettingAlexa]
(
	[UserId] INT NOT NULL IDENTITY, 
    [AllowPrayerTimeNotification] NVARCHAR(50) NOT NULL, 
    [SyncAlexa] NVARCHAR(50) NOT NULL
)
