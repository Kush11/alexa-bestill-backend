﻿CREATE TABLE [dbo].[Role] (
    [RoleId]     INT            NOT NULL,
    [RoleName]   NVARCHAR (300) NOT NULL,
    [Status]     NVARCHAR (10)  NOT NULL,
    [CreatedBy]  NVARCHAR (300) NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] NVARCHAR (300) NULL,
    [ModifiedOn] DATETIME       NULL
);

