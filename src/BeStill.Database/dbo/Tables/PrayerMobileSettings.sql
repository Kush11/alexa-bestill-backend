﻿CREATE TABLE [dbo].[PrayerMobileSettings]
(
	[UserId] INT NOT NULL IDENTITY, 
    [Frequency] INT NOT NULL, 
    [Date] DATETIME NOT NULL, 
    [Time] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(300) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NOT NULL, 
    [ModifiedOn] DATETIME NOT NULL	
)
