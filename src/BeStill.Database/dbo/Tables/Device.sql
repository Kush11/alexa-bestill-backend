﻿CREATE TABLE [dbo].[Device] (
      [Id] INT NOT NULL,
      [DeviceId]   NVARCHAR (500) NOT NULL,
      [Model] NVARCHAR(500) NULL,
      [Name] NVARCHAR(500) NULL,

    [Status]     NVARCHAR (10)  NOT NULL,
    [CreatedBy]  NVARCHAR (300) NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] NVARCHAR (300) NULL,
    [ModifiedOn] DATETIME       NULL, 
   
    CONSTRAINT [PK_Device] PRIMARY KEY ([Id])
);

