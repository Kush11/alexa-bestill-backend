﻿CREATE TABLE [dbo].[RecommendedBible]
(
	[Id] INT NOT NULL IDENTITY, 
    [Title] NVARCHAR(300) NOT NULL, 
    [Abbreviation] NVARCHAR(50) NOT NULL, 
    [SubTitle] NVARCHAR(300) NOT NULL, 
    [Description] NVARCHAR(300) NOT NULL, 
    [Link] NVARCHAR(500) NOT NULL, 
    [Status] NVARCHAR(50) NOT NULL, 
    [CreatedBy] NVARCHAR(300) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NOT NULL, 
    [ModifiedOn] DATETIME NOT NULL
)
