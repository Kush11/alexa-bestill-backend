﻿CREATE TABLE [dbo].[MobileSettings]
(
	[UserId] INT NOT NULL IDENTITY, 
    [Appearance] NVARCHAR(50) NOT NULL, 
    [DefaultSortBy] NVARCHAR(50) NOT NULL, 
    [DefaultSnoozeDuration] INT NOT NULL, 
    [ArchiveAutoDelete] NCHAR(10) NOT NULL, 
    [IncludeAnsweredPrayerAutoDelete] NCHAR(10) NOT NULL, 
    [Status] NVARCHAR(10) NOT NULL, 
    [CreatedBy] NVARCHAR(300) NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NULL, 
    [ModifiedOn] DATETIME NOT NULL
)
