﻿CREATE TABLE [dbo].[Church] (
    [ChurchId]              INT            NOT NULL IDENTITY,
    [Name]                  NVARCHAR (300) NOT NULL,
    [Address]               NVARCHAR (500) NOT NULL,
    [City]                  NVARCHAR (200) NOT NULL,
    [State]                 NVARCHAR (200) NOT NULL,
    [Email]                 NVARCHAR (300) NOT NULL,
    [Phone]                 NVARCHAR (15)  NOT NULL,
    [MethodRecievingPrayer] NVARCHAR (50)  NOT NULL,
    [WebiteUrl]             NVARCHAR (300) NOT NULL,
    [Status]                NVARCHAR (10)  NOT NULL,
    [CreatedBy]             NVARCHAR (300) NULL,
    [CreatedOn]             DATETIME       NULL,
    [ModifiedBy]            NVARCHAR (300) NULL,
    [ModifiedOn]            DATETIME       NULL
);

