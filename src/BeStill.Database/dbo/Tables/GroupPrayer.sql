﻿CREATE TABLE [dbo].[GroupPrayer] (
    [GroupId]     INT            NOT NULL,
    [PrayerId]    UNIQUEIDENTIFIER            NOT NULL,
    [Sequence]    INT            NOT NULL,
    [IsFavourite] BIT            NOT NULL,
    [Status]      NVARCHAR (10)  NOT NULL,
    [CreatedBy]   NVARCHAR (300) NULL,
    [CreatedOn]   DATETIME       NULL,
    [ModifiedBy]  NVARCHAR (300) NULL,
    [ModifiedOn]  DATETIME       NULL
);

