﻿CREATE TABLE [dbo].[IntentHistoryLogTemp]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[NextToken] [nvarchar](50) NOT NULL,
	[SkillId] [nvarchar](300) NOT NULL,
	[DialogAct] [nvarchar](50) NULL,
	[IntentName] [nvarchar](50) NULL,
	[IntentConfidence] [nvarchar](50) NULL,
	[SlotName] [nvarchar](50) NULL,
	[InteractionType] [nvarchar](50) NULL,
	[PublicationStatus] [nvarchar](50) NULL,
	[UtteranceText] [nvarchar](500) NULL,
	[Locale] [nvarchar](50) NULL,
	[Stage] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL
)
