﻿CREATE TABLE [dbo].[Group] (
    [GroupId]         INT            NOT NULL IDENTITY,
    [Name]       NVARCHAR (300) NOT NULL,
    [Status]     NVARCHAR (10)  NOT NULL,
    [Description] NVARCHAR (1000) NOT NULL,
    [Organization] NVARCHAR (500) NOT NULL,
    [Location] NVARCHAR (300) NOT NULL,
    [CreatedBy]  NVARCHAR (300) NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] NVARCHAR (300) NULL,
    [ModifiedOn] DATETIME       NULL
);

