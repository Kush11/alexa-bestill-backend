﻿CREATE TABLE [dbo].[GroupMobileSettings]
(
	[UserId] INT NOT NULL IDENTITY, 
    [DeviceId] INT NOT NULL, 
    [Status] NVARCHAR(50) NOT NULL, 
    [CreatedBy] NVARCHAR(300) NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] NVARCHAR(300) NOT NULL, 
    [ModifiedOn] DATETIME NOT NULL
)
