﻿CREATE TABLE [dbo].[Reminder] (
    [ReminderId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]  NVARCHAR (500) NOT NULL,
    [Frequency]   INT            NOT NULL,
    [StartDate]  DATETIME       NOT NULL,
    [EndDate]    DATETIME       NOT NULL,
    [Title]   NVARCHAR (500)            NOT NULL,
    [Status]     NVARCHAR (10)  NOT NULL,
    [CreatedBy]  NVARCHAR (300) NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] NVARCHAR (300) NULL,
    [ModifiedOn] DATETIME       NULL, 
    [Token] NVARCHAR(MAX) NULL, 
    [PrayerId] NVARCHAR(500) NULL
);

