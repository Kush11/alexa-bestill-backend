﻿

/********************************************************************************************************************
Procedure Name:	IntentHistoryLog_InsertFromTemp
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE [dbo].[IntentHistoryLog_InsertFromTemp]
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int
DECLARE
	@Id int

/* Beginning of procedure */
BEGIN TRANSACTION

insert into dbo.IntentHistoryLog (
		NextToken,
		SkillId,
		DialogAct,
		IntentName,
		IntentConfidence,
		SlotName,
		InteractionType,
		PublicationStatus,
		UtteranceText,
		Locale,
		Stage,
		[Date]
	)
	select 
		hlt.NextToken,
		hlt.SkillId,
		hlt.DialogAct,
		hlt.IntentName,
		hlt.IntentConfidence,
		hlt.SlotName,
		hlt.InteractionType,
		hlt.PublicationStatus,
		hlt.UtteranceText,
		hlt.Locale,
		hlt.Stage,
		hlt.[Date]
	from dbo.IntentHistoryLogTemp hlt left outer join dbo.IntentHistoryLog hl on hlt.IntentName = hl.IntentName and hlt.UtteranceText = hl.UtteranceText
	where hl.IntentName is null;

	delete dbo.IntentHistoryLogTemp;

/* error-handling */
SELECT @error = @@error, @Id = @@IDENTITY

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/


