﻿
CREATE PROCEDURE dbo.Prayer_InsertToGroup
(
	@PrayerId uniqueidentifier,
	@GroupId int,
	@UserId nvarchar(500),
	@Sequence int,
	@IsFavourite bit,
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO GroupPrayer 
(
	GroupId,
	PrayerId,
	[Sequence],
	IsFavourite,
	[Status],
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@GroupId,
	@PrayerId,
	@Sequence,
	@IsFavourite,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
