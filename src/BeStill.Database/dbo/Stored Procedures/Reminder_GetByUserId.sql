﻿
/********************************************************************************************************************
Procedure Name:	Reminder_GetByUserId
Copyright �	2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Reminder_GetByUserId
(
	@UserId as nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	r.ReminderId,
	r.PrayerId,
	r.UserId,
	r.Frequency,
	r.StartDate,
	r.EndDate,
	r.Token,
	r.Title,
	r.[Status],
	r.CreatedBy,
	r.CreatedOn,
	r.ModifiedBy,
	r.ModifiedOn
FROM 
	Reminder r where r.UserId = @UserId
	and ((r.Frequency > 1 and Status = 'active') or (r.Frequency = 1 and GETDATE() < r.StartDate and Status = 'active'))


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/




