﻿

CREATE PROCEDURE dbo.UserPreference_GetByUserId
(
	@UserId as nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	u.UserId,
	u.PrayerTime,
	u.BackgroundMusic,
	u.CreatedBy,
	u.CreatedOn,
	u.ModifiedBy,
	u.ModifiedOn
FROM 
	UserPreference u
WHERE u.UserId = @UserId


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/


