﻿

/********************************************************************************************************************
Procedure Name:		Prayer_MarkAsAnsweredById
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Prayer_HideFromMe
(
	@PrayerId uniqueidentifier,
	@UserId nvarchar(500),
	@CreatedBy nvarchar(300)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION


INSERT INTO PrayerDisable
(
	PrayerId,
	UserId,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@PrayerId,
	@UserId,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/

