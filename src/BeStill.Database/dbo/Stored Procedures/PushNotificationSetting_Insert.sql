﻿CREATE PROCEDURE dbo.PushNotificationSetting_Insert
	(
    @AllowPushNotification nchar(10),
    @AllowTextNotification nchar(10),
    @EmailUpdateNotification nvarchar(300),
    @EmailUpdateFrequency int,
    @NotifyMeSomeOneSharePrayerWithMe nchar(10),
    @NotifyMeSomeOnePostOnGroup nchar(10),
    @CreatedBy nvarchar(300),
    @CreatedOn DateTime,
    @ModifiedBy nvarchar(300),
    @ModifiedOn DateTime
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO MobileSettingPushNotification
(
    AllowPushNotification,
    AllowTextNotification,
    EmailUpdateNotification,
    EmailUpdateFrequency,
    NotifyMeSomeOneSharePrayerWithMe,
    NotifyMeSomeOnePostOnGroup,
    CreatedBy,
    CreatedOn,
    ModifiedBy,
    ModifiedOn
)

VALUES
(
    @AllowPushNotification,
    @AllowTextNotification,
    @EmailUpdateNotification,
    @EmailUpdateFrequency,
    @NotifyMeSomeOneSharePrayerWithMe,
    @NotifyMeSomeOnePostOnGroup,
    @CreatedBy,
    GetDate(),
    @ModifiedBy,
    GetDate()
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/


