﻿
CREATE PROCEDURE dbo.Group_InsertInvitationStatus
(
	@GroupInviteId int OUTPUT,
	@UserId nvarchar(300),
	@Status nvarchar (10),
	@GroupId int,
    @CreatedBy nvarchar(300)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO [GroupInvite]
(
	UserId,
	GroupId,
	Status,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UserId,
	@GroupId,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
