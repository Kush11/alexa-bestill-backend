﻿
/********************************************************************************************************************
Procedure Name:		Prayer_DeleteById
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Prayer_DeleteById
(
	@PrayerId as uniqueidentifier
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

DELETE UserPrayer
WHERE PrayerId = @PrayerId

DELETE Prayer where PrayerId = @PrayerId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
