﻿
CREATE PROCEDURE dbo.Group_Insert
(
	@GroupId int OUTPUT,
	@Name nvarchar(300),
	@Status nvarchar (10),
    @Description nvarchar (1000),
    @Organization nvarchar (500),
    @Location nvarchar (300),
    @CreatedBy nvarchar(300)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO [Group]
(
	Name,
	Status,
	Description,
	Organization,
	Location,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@Name,
	@Status,
	@Description,
	@Organization,
	@Location,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
