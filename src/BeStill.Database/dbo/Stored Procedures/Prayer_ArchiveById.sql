﻿
CREATE PROCEDURE dbo.[Prayer_ArchiveById]
(
	@PrayerId uniqueidentifier

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

UPDATE Prayer SET
	[Status] = 'inactive'
WHERE PrayerId = @PrayerId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/


