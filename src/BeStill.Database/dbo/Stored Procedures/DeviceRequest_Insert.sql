﻿
CREATE PROCEDURE dbo.DeviceRequest_Insert
(
	@DeviceRequestId uniqueidentifier,
	@DeviceId nvarchar(500),
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int,
	@_deviceId int

/* Beginning of procedure */
BEGIN TRANSACTION
SELECT @_deviceId = Id FROM Device where DeviceId = @DeviceId

INSERT INTO DeviceRequest 
(
	DeviceRequestId,
	DeviceId,
	Status,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@DeviceRequestId,
	@_deviceId,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/




