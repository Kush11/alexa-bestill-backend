﻿
/********************************************************************************************************************
Procedure Name:	Prayer_GetAllByUserId
Copyright �	2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Prayer_GetAllByGroupId
(

	@GroupId varchar(300),
	@UserId nvarchar(500)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT
	gp.GroupId,
	gp.PrayerId,
	gp.[Sequence],
	gp.IsFavourite,
	gp.[Status],
	gp.CreatedBy,
	gp.CreatedOn,
	gp.ModifiedBy,
	gp.ModifiedOn,
	p.ChurchId,
	p.[Description],
	p.IsAnswer,
	p.MusicId,
	p.Title,
	p.[Type]
FROM GroupPrayer gp
	--(SELECT p.* FROM Prayer p WHERE p.PrayerId NOT IN
	--	(SELECT pd.PrayerId FROM dbo.PrayerDisable pd where pd.UserId = @UserId) 
	--) as prayersgroup
		inner join dbo.Prayer p on gp.PrayerId = p.PrayerId
		AND p.PrayerId NOT IN (SELECT pd.PrayerId FROM dbo.PrayerDisable pd where pd.UserId = @UserId)
WHERE gp.GroupId = @GroupId and gp.Status = 'active' order by p.PrayerId desc


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/





