﻿
/********************************************************************************************************************
Procedure Name:	Prayer_GetAllByUserId
Copyright �	2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Prayer_GetAllByUserId
(

	@UserId as varchar(500)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	u.UserId,
	u.PrayerId,
	u.[Sequence],
	u.IsFavourite,
	u.[Status],
	u.CreatedBy,
	u.CreatedOn,
	u.ModifiedBy,
	u.ModifiedOn,
	p.ChurchId,
	p.[Description],
	p.IsAnswer,
	p.MusicId,
	p.Title,
	p.[Type]
FROM 
	UserPrayer u inner join dbo.Prayer p on u.PrayerId = p.PrayerId
WHERE u.UserId = @UserId and p.IsAnswer = 'n' and p.Status = 'active' order by p.PrayerId desc


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/





