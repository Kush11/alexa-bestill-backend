﻿CREATE PROCEDURE [dbo].[Group_GetById]
	@GroupId int
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT
	gu.UserId,
	g.Name, 
	g.Status,
	g.Description,
	g.Organization,
	g.Location,
	g.CreatedBy,
	g.CreatedOn,
	g.ModifiedBy,
	g.ModifiedOn
FROM 
	GroupUser gu inner join [Group] g on gu.GroupId = g.GroupId
WHERE g.GroupId = @GroupId order by gu.GroupId desc


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

