﻿CREATE PROCEDURE [dbo].[Prayer_GetById]
	@Id uniqueidentifier
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	u.UserId,
	u.PrayerId,
	u.[Sequence],
	u.IsFavourite,
	u.[Status],
	u.CreatedBy,
	u.CreatedOn,
	u.ModifiedBy,
	u.ModifiedOn,
	p.ChurchId,
	p.[Description],
	p.IsAnswer,
	p.MusicId,
	p.Title,
	p.[Type]
FROM 
	UserPrayer u inner join dbo.Prayer p on u.PrayerId = p.PrayerId
WHERE p.PrayerId = @Id and p.IsAnswer = 'n' and p.Status = 'active'


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

