﻿
CREATE PROCEDURE dbo.UserPreference_Insert
(
	@UserId nvarchar(500),
	@PrayerTime int,
	@BackgroundMusic nvarchar(50),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION
IF NOT EXISTS (SELECT d.UserId FROM dbo.UserPreference d where d.UserId = @UserId)
	INSERT INTO UserPreference (UserId,PrayerTime,BackgroundMusic,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) VALUES (@UserId,@PrayerTime,@BackgroundMusic,@CreatedBy,GetDate(),@CreatedBy,GetDate())
ELSE
	UPDATE dbo.UserPreference set PrayerTime = @PrayerTime, BackgroundMusic = @BackgroundMusic where UserId = @UserId

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
