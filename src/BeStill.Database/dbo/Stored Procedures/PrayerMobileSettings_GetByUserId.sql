﻿CREATE PROCEDURE dbo.PrayerMobileSettings_GetByUserId
	(
	@UserId as int
)
As
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */

SELECT
	
	m.UserId,
    m.Frequency,
    m.[Date],
    m.[Time],
    m.CreatedBy,
    m.CreatedOn,
    m.ModifiedBy,
    m.ModifiedOn
FROM 
    PrayerMobileSettings m where m.UserId = @UserId

    /* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/