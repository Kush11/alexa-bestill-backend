﻿CREATE PROCEDURE [dbo].[User_UpdateLastPrayed]
(
	@UserId nvarchar(500)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

UPDATE dbo.[User] SET [LastPrayed] = GetDate() WHERE UserId = @UserId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/

