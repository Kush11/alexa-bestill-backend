﻿
/********************************************************************************************************************
Procedure Name:	UserDevice_GetByUserId
Copyright �	2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.UserDevice_GetByUserId
(
	@UserId as nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	u.UserId,
	u.DeviceId,
	u.[Status],
	u.CreatedBy,
	u.CreatedOn,
	u.ModifiedBy,
	u.ModifiedOn
FROM 
	UserDevice u
WHERE u.UserId = @UserId


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/





