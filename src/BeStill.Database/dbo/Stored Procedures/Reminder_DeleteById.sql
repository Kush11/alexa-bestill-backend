﻿
/********************************************************************************************************************
Procedure Name:		Reminder_DeleteById
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Reminder_DeleteById
(
	@ReminderId as uniqueidentifier
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

DELETE Reminder
WHERE ReminderId = @ReminderId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/




