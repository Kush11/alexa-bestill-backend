﻿
CREATE PROCEDURE dbo.UserDevice_Insert
(
	@UserId nvarchar(1000),
	@DeviceId nvarchar(1000),
	@Model nvarchar(500),
	@Name nvarchar(500),
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int,
	@_deviceId int

/* Beginning of procedure */
BEGIN TRANSACTION

IF NOT EXISTS (SELECT d.DeviceId FROM dbo.Device d where d.DeviceId = @DeviceId)
	insert into dbo.Device (DeviceId, Model, Name, [Status], CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values (@DeviceId, @Model, @Name,'Active',@CreatedBy,GetDate(),@CreatedBy,GetDate())

IF NOT EXISTS (SELECT u.UserId FROM dbo.[User] u where u.UserId = @UserId)
	insert into dbo.[User] (UserId, [Status], CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values (@UserId,'Active',@CreatedBy,GetDate(),@CreatedBy,GetDate())

select @_deviceId = Id from Device where DeviceId = @DeviceId
INSERT INTO UserDevice 
(
	UserId,
	DeviceId,
	[Status],
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UserId,
	@_deviceId,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)





/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/





