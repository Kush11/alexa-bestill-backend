﻿CREATE PROCEDURE dbo.RecommendedBible_GetAll

As
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */

SELECT
	
	m.Id as Id,
    m.Title as Title,
    m.Abbreviation as Abbreviation,
    m.SubTitle as SubTitle,
    m.[Description] as [Description],
    m.Link as Link,
    m.[Status] as [Status],
    m.CreatedBy as CreatedBy,
    m.CreatedOn as CreatedOn,
    m.ModifiedBy as ModifiedBy,
    m.ModifiedOn as ModifiedOn
FROM 
    RecommendedBible m 

    /* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/
