﻿CREATE PROCEDURE dbo.RecommendedBible_Insert
(
    @Title nvarchar(300),
    @Abbreviation nvarchar(300),
    @SubTitle nvarchar(300),
    @Description nvarchar(300),
    @Link nvarchar(500),
    @Status nvarchar(50),
    @CreatedBy nvarchar(300),
    @CreatedOn DateTime,
    @ModifiedBy nvarchar(300),
    @ModifiedOn DateTime
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO RecommendedBible
(
    Title,
    Abbreviation,
    SubTitle,
    [Description],
    Link,
    [Status],
    CreatedBy,
    CreatedOn,
    ModifiedBy,
    ModifiedOn
)

VALUES
(
    @Title,
    @Abbreviation,
    @SubTitle,
    @Description,
    @Link,
    @Status,
    @CreatedBy,
    GetDate(),
    @ModifiedBy,
    GetDate()
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/
