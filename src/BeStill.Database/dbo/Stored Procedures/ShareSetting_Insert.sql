﻿CREATE PROCEDURE dbo.ShareSetting_Insert
	 (
   @EnableSharingViaText nchar(10),
   @EnableSharingViaEmail nchar(10),
   @ChurchId int,
   @Email nvarchar(300),
   @Phone nvarchar(50),
   @Status nchar(10),
   @CreatedBy nvarchar(300),
   @CreatedOn DateTime,
   @ModifiedBy nvarchar(300),
   @ModifiedOn DateTime
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO MobileSettingsSharing
(
    EnableSharingViaText,
    EnableSharingViaEmail,
    ChurchId,
    Email,
    Phone,
    [Status],
    CreatedBy,
    CreatedOn,
    ModifiedBy,
    ModifiedOn
)

VALUES
(
   @EnableSharingViaText,
   @EnableSharingViaEmail,
   @ChurchId,
   @Email,
   @Phone,
   @Status,
   @CreatedBy,
   GetDate(),
   @ModifiedBy,
   GetDate()
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/


