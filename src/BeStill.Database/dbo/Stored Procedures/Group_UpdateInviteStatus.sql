﻿

/********************************************************************************************************************
Procedure Name:		Prayer_MarkAsAnsweredById
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Group_UpdateInviteStatus
(
	@GroupId int,
	@UserId as varchar(500),
	@Status varchar(30)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

UPDATE GroupInvite SET [Status] = @Status WHERE GroupId = @GroupId AND UserId = @UserId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/

