﻿
CREATE PROCEDURE dbo.User_GetUserByUserId
(
 @UserId as nvarchar(500)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT

	u.UserId,
	u.FirstName,
	u.LastName,
	u.Phone,
	u.Email,
	u.Dob,
	u.ChurchId,
	u.Status,
	u.CreatedBy,
	u.CreatedOn,
	u.ModifiedBy,
	u.ModifiedOn,
	u.LastPrayed
FROM dbo.[User] u where u.UserId = @UserId


/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/



