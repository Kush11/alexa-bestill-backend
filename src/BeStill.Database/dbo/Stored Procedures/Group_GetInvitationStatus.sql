﻿CREATE PROCEDURE [dbo].[Group_GetInvitationStatus]
	@GroupId int,
	@UserId as nvarchar(500)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */


SELECT * FROM GroupInvite WHERE GroupId = @GroupId AND UserId = @UserId AND Status = 'pending'

/* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

