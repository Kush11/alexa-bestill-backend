﻿CREATE PROCEDURE dbo.GroupMobileSettings_Insert
 (
    @DeviceId int,
    @Status nvarchar(10),
    @CreatedBy nvarchar(300),
    @CreatedOn DateTime,
    @ModifiedBy nvarchar(300),
    @ModifiedOn DateTime
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO GroupMobileSettings
(
    DeviceId,
    [Status],
    CreatedBy,
    CreatedOn,
    ModifiedBy,
    ModifiedOn
)

VALUES
(
    @DeviceId,  
    @Status,
    @CreatedBy,
    GetDate(),
    @ModifiedBy,
    GetDate()
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/

