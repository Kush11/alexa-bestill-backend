﻿
CREATE PROCEDURE dbo.UserPrayer_Insert
(
	@UserId nvarchar(500),
	@PrayerId int,
	@Sequence int,
	@IsFavourite bit,
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO UserPrayer 
(
	UserId,
	PrayerId,
	Sequence,
	IsFavourite,
	Status,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UserId,
	@PrayerId,
	@Sequence,
	@IsFavourite,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/




