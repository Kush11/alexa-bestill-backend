﻿CREATE PROCEDURE dbo.Device_Insert
(
	@DeviceId nvarchar(500),
	@Model nvarchar(500),
	@Name nvarchar(500),
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO Device 
(
	DeviceId,
	Model,
	Name,
	Status,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@DeviceId,
	@Model,
	@Name,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
