﻿CREATE PROCEDURE dbo.MobileSetting_Insert
 (
    @Appearance nvarchar(50),
    @DefaultSortBy nvarchar(50),
    @DefaultSnoozeDuration int,
    @ArchiveAutoDelete nchar(10),
    @IncludeAnsweredPrayerAutoDelete nchar(10),
    @Status nvarchar(10),
    @CreatedBy nvarchar(300),
    @CreatedOn DateTime,
    @ModifiedBy nvarchar(300),
    @ModifiedOn DateTime
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO MobileSettings
(
    Appearance,
    DefaultSortBy,
    DefaultSnoozeDuration,
    ArchiveAutoDelete,
    IncludeAnsweredPrayerAutoDelete,
    [Status],
    CreatedBy,
    CreatedOn,
    ModifiedBy,
    ModifiedOn
)

VALUES
(
    @Appearance,
    @DefaultSortBy,
    @DefaultSnoozeDuration,
    @ArchiveAutoDelete,
    @IncludeAnsweredPrayerAutoDelete,
    @Status,
    @CreatedBy,
    GetDate(),
    @ModifiedBy,
    GetDate()
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/


