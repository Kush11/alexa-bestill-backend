﻿CREATE PROCEDURE dbo.AlexaSettings_Insert
 (
   @AllowPrayerTimeNotification nvarchar(50),
   @SyncAlexa nvarchar(50)
 )

AS
Set NoCount ON
/* Declare local variables */

Declare
@error int

/* Beginning of procedure */

BEGIN TRANSACTION

INSERT INTO MobileSettingAlexa
(
    AllowPrayerTimeNotification,
    SyncAlexa
)

VALUES
(
    @AllowPrayerTimeNotification,
    @SyncAlexa
)

/* error-handling */
SELECT @error = @@error

IF(@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
    IF(@@trancount > 0) ROLLBACK TRANSACTION
    RETURN @error

/* End of procedure*/


