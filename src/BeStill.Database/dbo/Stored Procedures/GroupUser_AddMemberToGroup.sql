﻿

/********************************************************************************************************************
Procedure Name:		Prayer_MarkAsAnsweredById
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.GroupUser_AddMemberToGroup
(
	@GroupId int,
	@UserId as nvarchar(500),
	@IsAdmin bit,
	@IsModerator bit,
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

INSERT INTO GroupUser 
(
	UserId,
	GroupId,
	IsAdmin,
	IsModerator,
	Status,
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UserId,
	@GroupId,
	@IsAdmin,
	@IsModerator,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/

