﻿
CREATE PROCEDURE dbo.Group_UpdateById
(
	@GroupId int,
	@Name nvarchar(300),
	@Status nvarchar (10),
    @Description nvarchar (1000),
    @Organization nvarchar (500),
    @Location nvarchar (300),
    @ModifiedBy nvarchar(300)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

UPDATE [Group] SET Name = @Name, Status = @Status, Description = @Description, Organization = @Organization, Location = @Location, ModifiedBy = @ModifiedBy
WHERE GroupId = @GroupId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
