﻿
/********************************************************************************************************************
Procedure Name:		Prayer_DeteAllByUserId
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Prayer_DeleteAllByUserId
(
	@UserId as varchar(500)
)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION
DELETE FROM Prayer where PrayerId in (select prayerId from UserPrayer where UserId = @UserId)
DELETE UserPrayer WHERE UserId = @UserId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/





