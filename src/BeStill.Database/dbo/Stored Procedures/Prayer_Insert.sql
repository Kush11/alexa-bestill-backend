﻿
CREATE PROCEDURE dbo.Prayer_Insert
(
	@PrayerId UNIQUEIDENTIFIER OUTPUT,
	@UserId nvarchar(500),
	@Sequence int,
	@IsFavourite bit,
	@Type nvarchar(100),
	@Title nvarchar(100),
	@Description nvarchar(2000),
	@IsAnswer nchar(2),
	@MusicId int,
	@ChurchId int,
	@Status nvarchar(20),
	@CreatedBy nvarchar(600)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int,
	@UNI UNIQUEIDENTIFIER

/* Beginning of procedure */
BEGIN TRANSACTION
SET @UNI = NEWID()
INSERT INTO Prayer 
(
	PrayerId,
	[Type],
	Title,
	[Description],
	IsAnswer,
	MusicId,
	ChurchId,
	[Status],
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UNI,
	@Type,
	@Title,
	@Description,
	@IsAnswer,
	@MusicId,
	@ChurchId,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error, @PrayerId = @UNI
insert into dbo.UserPrayer (UserId,PrayerId,[Sequence],IsFavourite,[Status],CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) 
	values (@UserId, @PrayerId, @Sequence, @IsFavourite, @Status, @CreatedBy, GetDate(), @CreatedBy, GetDate())

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/
