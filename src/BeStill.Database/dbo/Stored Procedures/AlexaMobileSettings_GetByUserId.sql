﻿CREATE PROCEDURE dbo.AlexaMobileSettings_GetByUserId
(
	@UserId as int
)
As
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */

SELECT
	m.UserId,
	m.AllowPrayerTimeNotification,
    m.SyncAlexa
   
FROM 
    MobileSettingAlexa m where m.UserId = @UserId

    /* error-handling */
SELECT @error = @@error 
IF (@error <> 0) GOTO ERROR


RETURN 0

/* error-handling */
ERROR:
	RETURN @error

/* End of procedure*/