﻿
/********************************************************************************************************************
Procedure Name:	Reminder_Insert
Copyright � 2020 Eminent Technology 
*********************************************************************************************************************/

CREATE PROCEDURE dbo.Reminder_Insert
(
	@ReminderId uniqueidentifier OUTPUT,
	@Token nvarchar(max) = null,
	@UserId nvarchar(500),
	@Frequency int,
	@StartDate datetime,
	@EndDate datetime,
	@Title nvarchar(500),
	@Status nvarchar(20),
	@CreatedBy nvarchar(600),
	@PrayerId nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int,
	@UNI uniqueidentifier

/* Beginning of procedure */
BEGIN TRANSACTION
set @UNI = NEWID()
INSERT INTO Reminder 
(
	ReminderId,
	PrayerId,
	UserId,
	Frequency,
	Token,
	StartDate,
	EndDate,
	Title,
	[Status],
	CreatedBy,
	CreatedOn,
	ModifiedBy,
	ModifiedOn
)
VALUES
(
	@UNI,
	@PrayerId,
	@UserId,
	@Frequency,
	@Token,
	@StartDate,
	@EndDate,
	@Title,
	@Status,
	@CreatedBy,
	GetDate(),
	@CreatedBy,
	GetDate()
)

/* error-handling */
SELECT @error = @@error, @ReminderId = @UNI

IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/




