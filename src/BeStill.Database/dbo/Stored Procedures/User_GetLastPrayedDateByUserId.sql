﻿CREATE PROCEDURE [dbo].[User_GetLastPrayedDateByUserId]
(
	@UserId as nvarchar(500)

)
AS
SET NOCOUNT ON
/* Declare local variables */
DECLARE
	@error int

/* Beginning of procedure */
BEGIN TRANSACTION

SELECT LastPrayed from dbo.[User] where UserId = @UserId


/* error-handling */
SELECT @error = @@error
IF (@error <> 0) GOTO ERROR

COMMIT TRANSACTION
RETURN 0

/* error-handling */
ERROR:
	IF (@@trancount > 0) ROLLBACK TRANSACTION
	RETURN @error

/* End of procedure*/


