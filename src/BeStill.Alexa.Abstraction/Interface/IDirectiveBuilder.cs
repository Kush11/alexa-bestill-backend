﻿using Alexa.NET.Response;
using Alexa.NET.Response.Directive;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeStill.Alexa.Abstraction.Interface
{
    public interface IDirectiveBuilder
    {
        Task<RenderDocumentDirective> BuildTextDirective(string sentences, string bg = "");
        Task<RenderDocumentDirective> BuildDefaultDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildItemListDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildGridListDirective(string sentences, string actions, string title = "", string subtitle = "", string childwidth = "50vw", string paddingRight = "20px");
        Task<RenderDocumentDirective> BuildDataListDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildScrollingTextDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildScrollingSequenceDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildScrollingSequenceWithVoiceDirective(string sentences, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuilduietTimeQScrollingSequenceDirective(string sentences, int quietTime, string title = "", string subtitle = "");
        Task<RenderDocumentDirective> BuildAutoPagerDirective(string sentences, int quietTime, string title = "", string subtitle = "");
        Task<AudioPlayerPlayDirective> BuildAudioDirective(string audioUrl, string title, string subtitle, string bg = "");
    }
}
