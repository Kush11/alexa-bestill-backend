﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStill.Alexa.Abstraction
{
    public class Prayer
    {
        public int PrayerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
}
